# SPDX-FileCopyrightText: 2023 Tezos Commons
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }:

{ config, pkgs, lib, ... }:
let
  inherit (lib) mkEnableOption mkDefault mkOption types;
  cfg = config.services.agora.indexer;
  db = "tzidx";
in {
  options.services.agora.indexer = {
    enable = lib.mkEnableOption "Agora frontend and backend combined with some sane defaults";
    package = lib.mkOption {
      type = types.path;
      default = self.packages.${pkgs.system}.indexer;
    };
    port = lib.mkOption {
      type = types.port;
      default = 5000;
    };
    tezosNodes = lib.mkOption {
      type = types.listOf types.str;
      default = [
        "https://rpc.tzbeta.net"
        "https://mainnet.tezos.marigold.dev"
        "https://tcinfra.net/rpc/mainnet"
      ];
    };
    dbConnectionString = lib.mkOption {
      type = types.str;
      default = if cfg.configurePostgres then "postgresql://${db}@127.0.0.1/${db}" else null;
    };
    user = lib.mkOption {
      type = lib.types.str;
      default = "agora-indexer";
    };
    configurePostgres = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Whether to configure a postgresql database for agora";
    };
  };
  config = lib.mkIf cfg.enable {
    systemd.services.agora-indexer-api = rec {
      wantedBy = [ "multi-user.target" ];

      after = [ "network-online.target" ] ++ lib.optional cfg.configurePostgres "postgresql.service";
      requires = after;

      path = [ cfg.package ];
      environment = {
        TZ_INDEXER_LISTEN_PORT = builtins.toString cfg.port;
        TZ_INDEXER_DATABASE= cfg.dbConnectionString; # "postgresql://tzidx:qwerty@127.0.0.1/tzidx";
      };
      script = ''
        indexerApi
      '';
      startLimitBurst = mkDefault 5;
      startLimitIntervalSec = mkDefault 300;
      serviceConfig = {
        User = cfg.user;
        Group = cfg.user;
        Restart = mkDefault "on-failure";
        RestartSec = mkDefault 10;
      };
      unitConfig.ConditionPathExists = [ cfg.package ];
    };
    systemd.services.agora-indexer-sync = rec {
      path = [ cfg.package ];

      wantedBy = [ "multi-user.target" ];

      after = [ "network-online.target" ] ++ lib.optional cfg.configurePostgres "postgresql.service";
      requires = after;
      environment = {
        TZ_INDEXER_NODES = lib.concatStringsSep "," cfg.tezosNodes;
        TZ_INDEXER_DATABASE= cfg.dbConnectionString;
      };
      script = ''
        indexerSync
      '';
      startLimitBurst = mkDefault 5;
      startLimitIntervalSec = mkDefault 300;
      serviceConfig = {
        User = cfg.user;
        Group = cfg.user;
        Restart = mkDefault "on-failure";
        RestartSec = mkDefault 10;
      };
      unitConfig.ConditionPathExists = [ cfg.package ];
    };
    users.users.${cfg.user} = { isSystemUser = true; group = cfg.user; };
    users.groups.${cfg.user} = { };

    services.postgresql = lib.mkIf cfg.configurePostgres {
      enable = true;
      package = pkgs.postgresql_14;
      ensureUsers = [{
        name = db;
        ensureDBOwnership = true;
      }];
      ensureDatabases = [ db ];
      authentication = ''
        host ${db} ${db} localhost trust
      '';
    };
  };
}
