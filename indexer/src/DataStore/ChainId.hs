-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.ChainId
  ( insertChainId
  ) where

import Common
import DataStore.Common

import Database.Mapping
import Database.Models.Blocks
import Database.Models.ChainId
import qualified Node

insertChainId
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> Node.ChainId
  -> CapsT caps m Int64
insertChainId blockId chainId = withConnection $ \conn -> let
  chainIdNew = ChainId (DbId 1) chainId blockId -- Using hardcoded value for primary key so that it becomes noticable if some code ever try to insert a second row.
  in (liftIO $
    runInsert conn $ Insert chainIdTable [toFields chainIdNew] rCount Nothing)
