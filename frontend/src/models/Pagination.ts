// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export interface Pagination {
  total: number;
  limit: number;
  rest: number;
  lastId: number;
}
