// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Proposer } from "~/models/ProposalInfo";

export interface ProposalsListItem {
  id: number;
  discourseLink: string;
  firstPeriod: number;
  hash: string;
  title: string;
  shortDescription: string;
  longDescription: string;
  proposer: Proposer;
  timeCreated: string;
  votesCasted: number;
}

export type ProposalsList = ProposalsListItem[];
