-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Web.Instances where

import Data.Aeson (Value)
import Fmt (Buildable (..))
import Servant.Util (ForResponseLog (..))

-- TODO: replace with something meaningful after fixes in 'servant-util'.
instance Buildable (ForResponseLog Value) where
    build = show . unForResponseLog
