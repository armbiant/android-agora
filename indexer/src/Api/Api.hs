-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | The API that this indexer exports. This is a small subset of the Tzkt
-- indexer api, that the Agora backend uses at the time of writing this.
-- While the API slightly differs from the Tzkt indexer's API, we have tried
-- to define it so that any backend that uses this API can be switched to use the
-- Tzkt indexer without any change in behavior.
module Api.Api
  ( IndexerApi
  , IndexerEndpoints(..)
  ) where

import Data.Text
import GHC.Generics
import Servant.API (Capture, Get, JSON, QueryParam, (:>))
import Servant.API.Generic (AsApi, ToServant, (:-))

import Api.Types
import qualified Node

-- | Definition of a subset of Tzkt indexer api that we currently use for Agora.
data IndexerEndpoints route = IndexerEndpoints
  { -- | Get info about head block
    neHead_ :: route
      :- "head"
      :> Get '[JSON] Head
  , -- | Get ballots for a specific proposal irrespective of the period
    neBallotsForProposal_ :: route
      :- "operations"
      :> "ballots"
      :> QueryParam "proposal" Node.ProposalHash
      :> QueryParam "period" Node.PeriodIdx
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [Ballot]
  , -- | Get voters for a given period
    neVoters_ :: route
      :- "voting"
      :> "periods"
      :> Capture "periodId" Node.PeriodIdx
      :> "voters"
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [PeriodVoter]
  , -- | Get the total number of blocks yet
    neBlockCount_ :: route
      :- "blocks"
      :> "count"
      :> Get '[JSON] Node.Level
  , -- | Get info about a given block
    neLevel_ :: route
      :- "blocks"
      :> Capture "level" Node.Level
      :> Get '[JSON] Block
  , -- | Get info about proposal
    neGetProposal_ :: route
      :- "voting"
      :> "proposals"
      :> Capture "hash" Node.ProposalHash
      :> Get '[JSON] Proposal
  , -- | Get info about all proposals in a voting epoch
    neGetProposals_ :: route
      :- "voting"
      :> "proposals"
      :> QueryParam "epoch" Node.EpochIdx
      :> Get '[JSON] [Proposal]
  , -- | Get all proposal operations for the given period
    neProposalOperations_ :: route
      :- "operations"
      :> "proposals"
      :> QueryParam "proposal" Node.ProposalHash
      :> QueryParam "period" Node.PeriodIdx
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [ProposalOperation]
  , -- | Get info about the protocol
    neProtocols_ :: route
      :- "protocols"
      :> QueryParam "offset" Int
      :> QueryParam "limit" Int
      :> Get '[JSON] [Protocol]
  , -- | Get info about the voting epoch
    neEpoch_ :: route
      :- "voting"
      :> "epochs"
      :> Capture "index" Node.EpochIdx
      :> Get '[JSON] EpochInfo
  , -- | Get info about the voting epoch
    neEpochs_ :: route
      :- "voting"
      :> "epochs"
      :> Get '[JSON] [EpochInfo]
  , -- | Get info about the voting epoch
    neVersion :: route
      :- "version"
      :> Get '[JSON] Text
  } deriving Generic

type IndexerApi  =
  "v1" :> ToServant IndexerEndpoints AsApi
