-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Discourse.TopicMatchingSpec
  ( spec
  ) where

import Test.Hspec (Spec, describe, it, shouldBe)

import Agora.Discourse.API
import Agora.Discourse.Client
import Agora.Discourse.Types
import Agora.Indexer.Capability
import Agora.TestMode
import Agora.Types as AT

spec :: Spec
spec =
  describe "Topic matching behaviour" $ do
    it "matches the topic with the longest prefix match consistently" $ do
      let
        paren t = "(" <> t <> ")"
        title1 = let
          hash1 = AT.Hash "0000000000"
          hash2 = AT.Hash "PsCARTHAGa"
          in Title $ unwords
          [ paren (hashToText hash1)
          , paren (hashToText hash2)
          ]
        title2 = let
          hash1 = AT.Hash "0000000000"
          hash2 = AT.Hash "PsCARTHAGaz"
          in Title $ unwords
          [ paren (hashToText hash1)
          , paren (hashToText hash2)
          ]
        topicHead1 = TopicHead 0 title1
        topicHead2 = TopicHead 1 title2

        mockDiscourseRaw = DiscourseEndpointsIO
          { deGetCategoryTopicsIO = \_ _ -> pure $ CategoryTopics 0 [topicHead1, topicHead2]
          , deGetCategoriesIO = pure $ CategoryList $ one $ Category 0 testDiscourseCategory
          , deGetTopicIO = \(Id tid) -> pure $ case tid of
              0 -> MkTopic 0 title1 (one $ Post 0 0 "")
              1 -> MkTopic 0 title2 (one $ Post 0 0 "")
              _ -> error "Unexpected topic request"
          , dePostTopicIO = error "Not supported"
          , deGetPostIO = error "Not supported"
          }
      topic <- runWithMockRefs $ \(discourseRef -> discourseIOTVar) -> do
        liftIO $ atomically $ writeTVar discourseIOTVar mockDiscourseRaw
        idxStartCacheRefresh
        idxWaitForCacheRefresh 2
        getProposalTopic (AT.Hash "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb")
      case topic of
        ExistingTopic (tTitle -> t) -> t `shouldBe` title2
        _                           -> fail "Expected topic was not found.."

    it "skips making match if two topics match a proposal" $ do
      let
        paren t = "(" <> t <> ")"
        title1 = let
          hash1 = AT.Hash "0000000000"
          hash2 = AT.Hash "PsFLor" -- This should match two proposals, PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE and PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i from the test data.
          in Title $ unwords
          [ paren (hashToText hash1)
          , paren (hashToText hash2)
          ]
        title2 = let
          hash1 = AT.Hash "0000000000"
          hash2 = AT.Hash "PsFLorenaU"
          in Title $ unwords
          [ paren (hashToText hash1)
          , paren (hashToText hash2)
          ]
        topicHead1 = TopicHead 0 title1
        topicHead2 = TopicHead 1 title2
        topic1 = MkTopic 0 title1 (one $ Post 0 0 "")
        topic2 = MkTopic 0 title2 (one $ Post 0 0 "")

        topic1' = MkTopic 0 title1 (Post 0 0 "")
        topic2' = MkTopic 0 title2 (Post 0 0 "")
      categoryTopicsTvar <- liftIO $ newTVarIO [topicHead1]
      let
        mockDiscourseRaw = DiscourseEndpointsIO
          { deGetCategoryTopicsIO = \_ _ -> do
              topics <- readTVarIO  categoryTopicsTvar
              pure $ CategoryTopics 0 topics
          , deGetCategoriesIO = pure $ CategoryList $ one $ Category 0 testDiscourseCategory
          , deGetTopicIO = \(Id tid) -> pure $ case tid of
              0 -> topic1
              1 -> topic2
              _ -> error "Unexpected topic request"
          , dePostTopicIO = error "Not supported"
          , deGetPostIO = error "Not supported"
          }

      results <- runWithMockRefs $ \(discourseRef -> discourseIOTVar) -> do
        liftIO $ atomically $ writeTVar discourseIOTVar mockDiscourseRaw
        idxStartCacheRefresh
        idxWaitForCacheRefresh 2
        r1 <- getProposalTopic (AT.Hash "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE")
        r2 <- getProposalTopic (AT.Hash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i")

        liftIO $ atomically $ writeTVar categoryTopicsTvar [topicHead1, topicHead2] -- Make more proposal topics to appear
        idxWaitForCacheRefresh 2
        r3 <- getProposalTopic (AT.Hash "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE")
        r4 <- getProposalTopic (AT.Hash "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i")
        pure (r1, r2, r3, r4)
      results `shouldBe` (DoesNotExist, DoesNotExist, ExistingTopic topic1', ExistingTopic topic2')

