// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { createContext } from "react";

export const ThemeContext = createContext({
  currentTheme: "",
  toggleTheme: (): void => {},
});
