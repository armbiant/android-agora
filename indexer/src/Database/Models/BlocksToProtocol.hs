-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.BlocksToProtocol
  ( BlocksToProtocolPoly(..)
  , BlocksToProtocolId
  , BlocksToProtocol
  , blocksToProtocolsTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Database.Models.Protocols qualified as Protocols

type BlocksToProtocolId = DbId "voter"

data BlocksToProtocolPoly id blockId protocolId = BlocksToProtocol
  { id         :: id
  , blockId    :: blockId
  , protocolId :: protocolId
  }

$(makeAdaptorAndInstance "pDbBlocksToProtocol" ''BlocksToProtocolPoly)

type BlocksToProtocol'' rm = BlocksToProtocolPoly
  (ComputeFieldTypeAuto rm BlocksToProtocolId)
  (ComputeFieldType rm Blocks.BlockId)
  (ComputeFieldType rm Protocols.ProtocolId)

type BlocksToProtocol = BlocksToProtocol'' 'HaskellRead

blocksToProtocolsTable
  :: Table (BlocksToProtocol'' 'DbWrite) (BlocksToProtocol'' 'DbRead)
blocksToProtocolsTable =
  table "blocks_to_protocols" (pDbBlocksToProtocol
    (BlocksToProtocol
      (tableField "id")
      (tableField "block_id")
      (tableField "protocol_id")))
