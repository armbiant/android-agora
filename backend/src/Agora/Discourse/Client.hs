-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE TypeOperators #-}

module Agora.Discourse.Client
       ( DiscourseClient (..)
       , DiscourseProposal (..)
       , DiscourseError (..)
       , DiscourseDeps
       , MonadDiscourseClient (..)
       , withDiscourseClient

       -- * For tests
       , discourseClient
       , topicParser
       ) where

import Control.Concurrent.STM.TBChan (TBChan, newTBChan, readTBChan, tryWriteTBChan)
import Control.Monad.Combinators as MC (between, many)
import Control.Monad.Combinators.NonEmpty as MC (some)
import Control.Monad.Reader (withReaderT)
import Data.Char (isPrint)
import qualified Data.Map as Map
import qualified Data.Text as T
import Fmt (pretty, unlinesF, (+|), (|+))
import Loot.Log (Logging, logDebug, logError)
import Monad.Capabilities (CapImpl (..), CapsT, HasCap, HasCaps, HasNoCap, addCap, makeCap)
import Text.Megaparsec (Parsec, parse, satisfy)
import Text.Megaparsec.Char (alphaNumChar, space)
import qualified Text.Megaparsec.Char.Lexer as L
import qualified UnliftIO as UIO
import UnliftIO (MonadUnliftIO)

import Agora.CapUtil
import Agora.Config
import Agora.Discourse.API
import Agora.Discourse.Html
import Agora.Discourse.Types
import Agora.Metrics.Capability
import Agora.State.Capability
import Agora.Types

data DiscourseProposal
  = Unknown -- We did not get a chance to check if a topic exist for this proposal
  | DoesNotExist -- We checked and it does not exist
  | ExistingTopic TopicOnePost -- We checked and a topic exists.
  deriving stock (Eq, Show)

data DiscourseClient m = DiscourseClient
  { _postProposalStubAsync    :: ProposalHash -> m ()
  , _getProposalTopic         :: ProposalHash  -> m DiscourseProposal
  , _getDiscourseTopic        :: DiscourseTopicId -> m Topic
  , _getDiscoursePost         :: DiscoursePostId -> m Post
  , _runProposalTopicMatching :: m ()
  }

makeCap ''DiscourseClient

data DiscourseError
  = CategoryNotFound !Text
  | MultipleTopicMatch !ProposalHash
  deriving (Eq, Show, Generic)

instance Exception DiscourseError

type ProposalToTopicsMap = Map.Map ProposalHash DiscourseProposal

withDiscourseClient
  :: forall m caps a .
  ( HasNoCap DiscourseClient caps
  , HasAgoraConfig caps
  , HasCap Logging caps
  , HasCap MetricsCap caps
  , HasCap StateCap caps
  , MonadUnliftIO m
  )
  => DiscourseEndpointsIO
  -> CapsT (DiscourseClient ': caps) m a
  -> CapsT caps m a
withDiscourseClient dep@(DiscourseEndpointsIO {..}) action = do
  categoryName <- fromAgoraConfig $ sub #discourse . option #category

  CategoryList categories <- runHTTPIOWithRetry deGetCategoriesIO
  Category{..} <- find ((categoryName ==) . cName) categories
    `whenNothing` UIO.throwIO (CategoryNotFound categoryName)

  -- A global reference that trackes proposal/topics association.
  proposalsToTopicsTVar <- liftIO $ newTVarIO (mempty :: ProposalToTopicsMap)

  chan <- UIO.atomically $ newTBChan 100
  UIO.withAsync (workerPoster chan cId dep) $ \_ ->
    withReaderT (addCap $ discourseClient proposalsToTopicsTVar chan cId dep) action

proposalMatcher
  :: forall m caps.
  (MonadUnliftIO m, HasCaps DiscourseDeps caps)
  => DiscourseCategoryId
  -> TVar ProposalToTopicsMap
  -> DiscourseEndpointsIO
  -> CapsT caps m ()
proposalMatcher catId proposalToTopicsMapTVar DiscourseEndpointsIO {..} = do
  logDebug "Starting proposal/topic sync"
  proposalsToTopicsMap <- liftIO $ readTVarIO proposalToTopicsMapTVar
  proposalTopics <- sortOn (Down . T.length . snd) <$> collectTopicsFromDiscourse [] 0 -- Descending sort discourse topics on prefix length.

  -- Here we walk through sorted proposal prefixes from discourse
  -- and pick matching proposal hash keys from the map that have a
  -- corresponding value of Nothing. If there are more then one matches
  -- that means that a topic at discourse has more than one matching proposals.
  -- This can mean that a proposal is missing a topic on discourse (if it was there
  -- one of the proposal that matched would be claimed by it), so we just log it and
  -- wait for more topics to appear on discourse. We also set all values in the map to `DoesNotExist`
  -- so that any hashes that did not match with a proposal at discourse will right fully have this status.
  newMap <- foldM foldFn (Map.map (const DoesNotExist) proposalsToTopicsMap) proposalTopics
  liftIO $ atomically $ writeTVar proposalToTopicsMapTVar newMap
  logDebug "Proposal/Topics sync Done"
  where
    foldFn :: ProposalToTopicsMap -> (TopicHead, Text) -> CapsT caps m ProposalToTopicsMap
    foldFn resMap (th, prefix) = let
      matchedHashes = Map.keys $ Map.filterWithKey filterFn resMap
      filterFn pHash dps = case dps of
        ExistingTopic _ -> False
        _               -> T.isPrefixOf prefix $ hashToText pHash
      in case matchedHashes of
        [] -> do
          logDebug $ "Proposal topic: " +| prefix |+ ", from discourse did not match any known proposals..."
          pure resMap
        [matchedHash] -> do
          topic <- runHTTPIOWithRetry $ deGetTopicIO (thId th)
          logDebug $ "Proposal topic: " +| prefix |+ ", matched with " +| matchedHash |+ "..."
          pure $ Map.insert matchedHash (ExistingTopic $ convertTopic topic) resMap
        otherMatches -> do
          logDebug $ Fmt.pretty $ unlinesF @_ @Text
            [ "Proposal topic from discourse match more then one known proposal"
            , "target prefix:" +| prefix  |+ ""
            , "matched:" +| otherMatches |+ ""
            , "Will do nothing and wait for more topics to appear in discourse..."
            ]
          pure resMap

    extractProposalHashes :: TopicHead -> Maybe (TopicHead, Text)
    extractProposalHashes th = case parse topicParser "" (unTitle $ thTitle th) of
      Right x -> Just (th, T.pack x)
      Left _  -> Nothing
    -- Walk through all the discourse topics in the given category
    -- and collect the ones that matches with the proposal hash.
    collectTopicsFromDiscourse :: HasCaps DiscourseDeps caps => [(TopicHead, Text)] -> Int -> CapsT caps m [(TopicHead, Text)]
    collectTopicsFromDiscourse thIn page = do
      CategoryTopics{..} <- runHTTPIOWithRetry $ deGetCategoryTopicsIO catId (Just page)
      let pageTpcs = mapMaybe extractProposalHashes ctTopics
      if length ctTopics < ctPerPage || ctPerPage == 0
        then pure (thIn <> pageTpcs) else collectTopicsFromDiscourse (thIn <> pageTpcs) (page + 1)


topicParser :: Parsec Void Text String
topicParser = last <$> MC.some topicEntryParser
  where
    topicEntryParser :: Parsec Void Text String
    topicEntryParser = MC.many ch *> parseTopicHash <* MC.many ch

    parseTopicHash :: Parsec Void Text String
    parseTopicHash =
      between (symbol "(") (symbol ")") (MC.many alphaNumChar) <|>
      between (symbol "[") (symbol "]") (MC.many alphaNumChar)

    symbol = L.symbol @_ @Text space
    ch = satisfy (\c -> isPrint c && c /= '(' && c /= '[')

type DiscourseDeps = '[AgoraConfigCap, MetricsCap, Logging]

discourseClient
  :: forall m . MonadUnliftIO m
  => TVar ProposalToTopicsMap
  -> TBChan ProposalHash
  -> DiscourseCategoryId
  -> DiscourseEndpointsIO
  -> CapImpl DiscourseClient DiscourseDeps m
discourseClient proposalsToTopicsVar chan catId dep@(DiscourseEndpointsIO {..}) = CapImpl $ DiscourseClient
  { _postProposalStubAsync = \ph -> do
      success <- UIO.atomically $ tryWriteTBChan chan ph
      if success then
        logDebug $ "Task to create a stub topic for " +| shortenHash ph |+ " is added to the Discourse worker queue"
      else
        sendMetrics $ CriticalError $ "Task to create a stub topic for " +| shortenHash ph |+ " is NOT added to the Discourse worker queue"
  , _getProposalTopic = \ph -> do
      pttMap <- liftIO $ readTVarIO proposalsToTopicsVar
      case Map.lookup ph pttMap of
        Just t -> pure t
        Nothing -> do
          liftIO $ atomically $ writeTVar proposalsToTopicsVar (Map.insert ph Unknown pttMap)
          pure Unknown
  , _getDiscourseTopic = \tid -> do
        runHTTPIOWithRetry $ deGetTopicIO tid
  , _getDiscoursePost = \pid -> do
        runHTTPIOWithRetry $ deGetPostIO pid
  , _runProposalTopicMatching = proposalMatcher catId proposalsToTopicsVar dep
  }

-- TODO maybe more robust scheme is needed here
convertTopic :: Topic -> TopicOnePost
convertTopic (MkTopic tId tTitle (post :| _)) = MkTopic tId tTitle post

workerPoster
  :: ( MonadUnliftIO m
     , HasAgoraConfig caps
     , HasCap Logging caps
     , HasCap MetricsCap caps
     , HasCap StateCap caps
     )
  => TBChan ProposalHash
  -> DiscourseCategoryId
  -> DiscourseEndpointsIO
  -> CapsT caps m ()
workerPoster chan cId DiscourseEndpointsIO {..} = forever $ do
    ph <- UIO.atomically $ readTBChan chan
    Map.lookup ph . asProposalCreationFailures <$> getState >>= \case
      Just err -> logDebug $ "Not retrying proposal creation for proposal hash, " +| ph |+ " since it  has failed in past with error:" +| err |+ ""
      _ -> do
        let shorten = shortenHash ph
        let title = Title $ "(" <> shorten <> ")"
        let body = RawBody $ defaultDescription shorten
        let ct = CreateTopic title body cId
        UIO.catch @_ @SomeException (workerDo ct) (\e -> do
          let
            errorStr = toText (displayException e) <> " while trying to create :" <> show ct
          modifyAgoraState (\as -> as { asProposalCreationFailures = Map.insert ph errorStr (asProposalCreationFailures as) })
          logError $
            "Something went wrong in the Discourse worker: " +| errorStr |+ "")
    where
      workerDo ct = do
        apiUsername <- fromAgoraConfig $ sub #discourse . option #api_username
        apiKey <- fromAgoraConfig $ sub #discourse . option #api_key
        void $ runHTTPIOWithRetry $ dePostTopicIO (Just apiUsername) (Just apiKey) ct
