-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main where

import Data.Yaml (encodeFile)

import Agora.Web.Swagger (agoraApiSwagger, swaggerSpecFilePath)

main :: IO ()
main = do
  putStrLn $ "Writing Swagger spec to " <> swaggerSpecFilePath <> "..."
  encodeFile swaggerSpecFilePath agoraApiSwagger
  putText "done"

