// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/common/Button.scss";
import { LinkProps } from "react-navi/dist/types/Link";
import NaviLink from "./NaviLink";

interface ButtonProps extends LinkProps {
  className?: string;
  children?: React.ReactNode;
}

export const ButtonLink: FunctionComponent<ButtonProps> = ({
  className,
  children,
  ...linkProps
}): ReactElement => {
  return (
    <NaviLink className={cx(className, styles.button)} {...linkProps}>
      {children}
    </NaviLink>
  );
};
