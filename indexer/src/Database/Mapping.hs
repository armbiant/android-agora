-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -Wno-orphans  #-}

-- | Module that defines the mappings of various domain types to their database
-- field types.
module Database.Mapping
  ( module Opaleye
  , ComputeFieldType
  , ComputeFieldTypeAuto
  , DbId(..)
  , RowMode(..)
  , ToSql
  , makeAdaptorAndInstance
  ) where

import Opaleye
  (DefaultFromField(..), Field, FieldNullable, Field_, Nullability(NonNullable, Nullable), Nullable,
  Select, SqlBool, SqlDate, SqlFloat8, SqlInt4, SqlInt8, SqlJsonb, SqlText, SqlTimestamptz, Table,
  ToFields, Unpackspec, aggregate, avg, count, groupBy, ifThenElse, isNull, leftJoin, matchNullable,
  runSelect, selectTable, showSql, sqlString, sum, table, tableField, where_, (.&&), (.++), (.<),
  (.<=), (.==), (.===))

import Data.Aeson
import Data.Maybe
import Data.Profunctor
import Data.Profunctor.Product.Default (Default(..))
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Text
import Data.Time
import GHC.TypeLits
import System.Log.FastLogger

import Node hiding (Block)
import Node qualified

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

newtype DbId (a :: Symbol) = DbId { unDbId :: Int }
  deriving newtype (Num, Eq, Show, ToLogStr)

instance DefaultFromField SqlText (Hash a) where
  defaultFromField = Hash <$> defaultFromField

instance DefaultFromField SqlText Ballot where
  defaultFromField = textToBallot <$> defaultFromField

instance DefaultFromField SqlText ProposalStatus where
  defaultFromField = parseProposalStatus <$> defaultFromField

instance DefaultFromField SqlText PeriodStatus where
  defaultFromField = parsePeriodStatus <$> defaultFromField

instance DefaultFromField SqlText Node.ChainId where
  defaultFromField = Node.ChainId <$> defaultFromField

instance DefaultFromField SqlText VotingPeriodKind where
  defaultFromField = fromMaybe (error "paring voting period from database failed") . Node.parseVotingPeriodKind <$> defaultFromField

instance DefaultFromField SqlInt4 Level where
  defaultFromField = Level <$> defaultFromField

instance DefaultFromField SqlInt4 PeriodIdx where
  defaultFromField = PeriodIdx <$> defaultFromField

instance DefaultFromField SqlInt8 VotingPower where
  defaultFromField = VotingPower <$> defaultFromField

instance DefaultFromField SqlInt4 EpochIdx where
  defaultFromField = EpochIdx <$> defaultFromField

instance DefaultFromField SqlInt4 (Maybe Int) where
  defaultFromField = Just <$> defaultFromField

instance DefaultFromField SqlInt4 (DbId a) where
  defaultFromField = DbId <$> defaultFromField

instance DefaultFromField SqlInt4 Quorum where
  defaultFromField = Quorum <$> defaultFromField

instance DefaultFromField SqlJsonb RawConstants where
  defaultFromField = RawConstants <$> defaultFromField

instance DefaultFromField SqlJsonb Node.Block where
  defaultFromField = fromJson' <$> defaultFromField
    where
      fromJson' :: Value -> Node.Block
      fromJson' v = case fromJSON v of
        Data.Aeson.Success x -> x
        _ -> error "Decoding block data from cache failed"

data RowMode = HaskellRead | HaskellWrite | DbRead | DbWrite

instance Default ToFields Node.ChainId (Field_ 'NonNullable SqlText) where
  def = lmap unChainId def

instance Default ToFields VotingPeriodKind (Field_ 'NonNullable SqlText) where
  def = lmap votingPeriodKindToText def

instance Default ToFields PeriodIdx (Field_ 'NonNullable SqlInt4) where
  def = lmap unPeriodIdx def

instance Default ToFields VotingPower (Field_ 'NonNullable SqlInt8) where
  def = lmap unVotingPower def

instance Default ToFields EpochIdx (Field_ 'NonNullable SqlInt4) where
  def = lmap unEpochIdx def

instance Default ToFields (Hash a) (Field_ 'NonNullable SqlText) where
  def = lmap unHash def

instance Default ToFields Level (Field_ 'NonNullable SqlInt4) where
  def = lmap unLevel def

instance Default ToFields Ballot (Field_ 'NonNullable SqlText) where
  def = lmap ballotToText def

instance Default ToFields ProposalStatus (Field_ 'NonNullable SqlText) where
  def = lmap proposalStatusToText def

instance Default ToFields PeriodStatus (Field_ 'NonNullable SqlText) where
  def = lmap periodStatusToText def

instance Default ToFields (DbId a) (Field_ 'NonNullable SqlInt4) where
  def = lmap unDbId def

instance Default ToFields Quorum (Field_ 'Nullable SqlInt4) where
  def = lmap (Just . unQuorum) def

instance Default ToFields Quorum (Field_ 'NonNullable SqlInt4) where
  def = lmap unQuorum def

instance Default ToFields RawConstants (Field_ 'NonNullable SqlJsonb) where
  def = lmap unRawConstants def

instance Default ToFields Node.Block (Field_ 'NonNullable SqlJsonb) where
  def = lmap encode def

type family ComputeFieldTypeAuto rm ft where
  ComputeFieldTypeAuto 'HaskellRead a = a
  ComputeFieldTypeAuto 'HaskellWrite a = Maybe a
  ComputeFieldTypeAuto 'DbWrite a = Maybe (Field (ToSql a))
  ComputeFieldTypeAuto 'DbRead a = Field (ToSql a)

type family ToSql a where
  ToSql Int = SqlInt4
  ToSql Text = SqlText
  ToSql (Hash a) = SqlText
  ToSql (DbId a) = SqlInt4
  ToSql Ballot  = SqlText
  ToSql ProposalStatus = SqlText
  ToSql ChainId = SqlText
  ToSql PeriodStatus = SqlText
  ToSql PeriodIdx = SqlInt4
  ToSql EpochIdx = SqlInt4
  ToSql VotingPower = SqlInt8
  ToSql VotingPeriodKind = SqlText
  ToSql UTCTime = SqlTimestamptz
  ToSql Level = SqlInt4
  ToSql RawConstants = SqlJsonb
  ToSql Node.Block = SqlJsonb
  ToSql Quorum = SqlInt4

type family ComputeFieldType rm ft where
  ComputeFieldType 'HaskellRead a = a
  ComputeFieldType 'HaskellWrite a = a
  ComputeFieldType 'DbWrite (Maybe a) = FieldNullable (ToSql a)
  ComputeFieldType 'DbWrite a = Field (ToSql a)
  ComputeFieldType 'DbRead (Maybe a) = FieldNullable (ToSql a)
  ComputeFieldType 'DbRead a = Field (ToSql a)
