# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

let
  pkgs = import ../nix {};
  project = import ./. { inherit pkgs; _expose = true; };
in

project.shellFor {}
