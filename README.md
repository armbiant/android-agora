# Tezos Agora _(agora)_

[![pipeline status](https://gitlab.com/tezosagora/agora/badges/master/pipeline.svg)](https://gitlab.com/tezosagora/agora/commits/master)

Tezos governance explorer.

Tezos blockchain is a self-amendment system, where the amendment process is
driven by stakeholders. It has its own voting system allowing stakeholders to
propose and vote for protocol updates. The Tezos Agora web application is used
to monitor the voting process and its results, and to discuss existing
proposals in Tezos community.

## Licensing guidelines

We want to make sure that our projects come with correct licensing information
and that this information is machine-readable, thus we are following the
[REUSE Practices][reuse] – feel free to click the link and read about them,
but, basically, it all boils down to the following:

  * Add the following header at the very top (but below the shebang, if there
    is one) of each source file in the repository (yes, each and every source
    file – it is not as hard as it might sound):

    ```haskell
    -- SPDX-FileCopyrightText: 2022 Tezos Commons
    -- SPDX-License-Identifier: AGPL-3.0-or-later
    ```

    (This is an example for Haskell; adapt it as needed for other languages.)

    The license identifier should be the same as the one in the `LICENSE` file.

  * If you are copying any source files from some other project, and they do not
    contain a header with a copyright and a machine-readable license identifier,
    add it, but be extra careful and make sure that information you are recording
    is correct.

    If the license of the file is different from the one used in the project and
    you do not plan to relicense it, use the appropriate license identifier and
    make sure the license text exists in the `LICENSES` directory.

    If the file contains the entire license in its header, it is best to move the
    text to a separate file in the `LICENSES` directory and leave a reference.

  * If you are copying pieces of code from some other project, leave a note in the
    comments, stating where you copied it from, who is the copyright owner, and
    what license applies.

  * All the same rules apply to documentation that is stored in the repository.

These simple rules should cover most of situation you are likely to encounter.
In case of doubt, consult the [REUSE Practices][reuse] document.

[reuse]: https://reuse.software/spec/
