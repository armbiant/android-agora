# Agora development

Agora comprises two major parts: a frontend web application and backend that
provides API that the frontend application uses.


## Backend

The code for the backend is located in the [`backend`](backend) directory.

### Building

* Install [Stack]
* `stack build`
* The resulting executable will be called `agora`

[stack]: https://docs.haskellstack.org/

### Development

* `stack test` to run tests
* `stack build --file-watch` to automatically recompile modified code

(TODO (AG-166) Add more details.)

## Frontend

The code for the frontend is located in the [`frontend`](frontend) directory.

### Building

* Install [Node.js]
* Install project dependencies: `npm install`
* `npm run build`

[Node.js]: https://nodejs.org/

### Development

* `npm run test` to run tests
* `npm run tscompile:watch` to automatically recompile modified code

#### Run in development mode

* `npm run serve:dev` will launch a local web-server that you can access
  from your browser at <http://localhost:1234/>

In order to display some actual data, frontend needs to be connected to
a backend server. The dev server started by the command above will forward
all API requests to `localhost:8190`.

You can compile and run a backend server locally on your own, however
this might be tricky, as the backend needs a connection
to a Tzkt.io indexer.

Alternatively, if you have ssh access to a host running an Agora backend,
(for example, managed by your continuous delivery system) you can forward
the local port to this remote host using something like:

* `ssh -L 127.0.0.1:8190:<container ip>:8190 <host ip>`
* `<host ip>` is the IP address of the remote host running Agora
* `<container ip>` is the virtual address of the container running in Docker
  on the target host, you can find it with `docker container inspect`

### Lints

* `npm run tslint` to lint TypeScript
* `npm run stylelint` to lint Sass styles

or:

* `npm run check-all` to run all of the above in parallel
