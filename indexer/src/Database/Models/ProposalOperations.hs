-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.ProposalOperations
  ( ProposalOperationPoly(..)
  , ProposalOperationId
  , ProposalOperation
  , proposalOperationsTable
  ) where

import Database.Mapping
import Database.Models.Proposals qualified as Proposals
import Database.Models.PeriodVoters qualified as PeriodVoters
import Node qualified
import System.Log.FastLogger

type ProposalOperationId = DbId "proposalOperation"

data ProposalOperationPoly id proposalId periodIdx operationHash periodVoterId sourceBlockId = ProposalOperation
  { id            :: id
  , proposalId    :: proposalId
  , periodIdx     :: periodIdx
  , operationHash :: operationHash
  , periodVoterId :: periodVoterId
  , sourceBlockId :: sourceBlockId
  } deriving stock Show

instance
  ( Show id, Show proposalId, Show periodIdx
  , Show operationHash, Show proposer, Show sourceBlockId) =>
    ToLogStr (ProposalOperationPoly id proposalId
      periodIdx operationHash proposer sourceBlockId) where
    toLogStr = toLogStr . show

$(makeAdaptorAndInstance "pDbProposalOperation" ''ProposalOperationPoly)

type ProposalOperation'' rm = ProposalOperationPoly
  (ComputeFieldTypeAuto rm ProposalOperationId)
  (ComputeFieldType rm Proposals.ProposalId)
  (ComputeFieldType rm Node.PeriodIdx)
  (ComputeFieldType rm Node.OperationHash)
  (ComputeFieldType rm PeriodVoters.PeriodVoterId)
  (ComputeFieldType rm Int)

type ProposalOperation = ProposalOperation'' 'HaskellRead

proposalOperationsTable
  :: Table (ProposalOperation'' 'DbWrite) (ProposalOperation'' 'DbRead)
proposalOperationsTable =
  table "proposal_operations" (pDbProposalOperation
    (ProposalOperation
      (tableField "id")
      (tableField "proposal_id")
      (tableField "period_idx")
      (tableField "operation_hash")
      (tableField "period_voter_id")
      (tableField "source_block_id")))
