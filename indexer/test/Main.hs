-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main where

import Test.Hspec (hspec)

import qualified Spec

main :: IO ()
main = hspec Spec.spec
