// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/common/CommonLayout.scss";

interface LayoutProps {
  children: React.ReactNode;
  className?: string;
}

export const Layout: FunctionComponent<LayoutProps> = ({
  children,
  className,
}): ReactElement => {
  return <div className={cx(className, styles.layout)}>{children}</div>;
};

export const LayoutContent: FunctionComponent<LayoutProps> = ({
  children,
  className,
}): ReactElement => {
  return (
    <div className={cx(className, styles.layout__content)}>{children}</div>
  );
};
