// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, {
  FunctionComponent,
  ReactElement,
  useContext,
  useState,
} from "react";
import cx from "classnames";
import styles from "~/styles/components/common/Header.scss";
import { useTranslation } from "react-i18next";
import MenuIcon from "~/assets/svg/MenuIcon";
import CloseIcon from "~/assets/svg/CloseIcon";
import Logo from "~/assets/svg/Logo";
import LogoCaption from "~/assets/svg/LogoCaption";
import NaviLink from "./NaviLink";
import ThemeSwitcher from "~/components/controls/ThemeSwitcher";
import { ThemeContext } from "~/components/common/ThemeContext";

interface HeaderMenuTypes {
  isOpen: boolean;
  onClose: () => void;
}

const HeaderMenu: FunctionComponent<HeaderMenuTypes> = ({
  isOpen,
  onClose,
}): ReactElement => {
  const themeContext = useContext(ThemeContext);
  const body = document.getElementsByTagName("body").item(0);
  if (body) {
    body.setAttribute(
      "style",
      isOpen ? "position: fixed" : "position: initial"
    );
  }
  const { t } = useTranslation();
  return (
    <div
      className={cx(styles.header__menu, {
        [styles.header__menu_open]: isOpen,
      })}
    >
      <div className={styles.header__menu__closeButton} onClick={onClose}>
        <CloseIcon />
      </div>
      <div className={styles.header__logo}>
        <NaviLink href="/">
          <Logo />
          <LogoCaption />
        </NaviLink>
      </div>
      <div className={styles.header__menu__links}>
        <ThemeSwitcher toggleTheme={themeContext.toggleTheme} />
        <a href={t("tezosLinks.tezosDocsLink")}>{t("header.docsLink")}</a>
        <a href={t("tezosLinks.getStartedLink")}>
          {t("header.getStartedLink")}
        </a>
        <NaviLink href={t("tezosLinks.tezosGovernanceLink")}>
          {t("header.governanceLink")}
        </NaviLink>
        <a href={t("tezosLinks.tzipExplorerLink")}>
          {t("header.tzipExplorerLink")}
        </a>
      </div>
    </div>
  );
};

interface Props {
  className?: string;
}

const AgoraHeader: FunctionComponent<Props> = ({ className }): ReactElement => {
  const { t } = useTranslation();
  const [isMenuOpen, setMenuOpen] = useState(false);
  const themeContext = useContext(ThemeContext);
  return (
    <div className={cx(className, styles.header)}>
      <div className={styles.header__logo}>
        <NaviLink href="/">
          <Logo />
          <LogoCaption />
        </NaviLink>
      </div>
      <div className={styles.header__links}>
        <ThemeSwitcher toggleTheme={themeContext.toggleTheme} />
        <a href={t("tezosLinks.tezosDocsLink")}>{t("header.docsLink")}</a>
        <a href={t("tezosLinks.getStartedLink")}>
          {t("header.getStartedLink")}
        </a>
        <NaviLink href={t("tezosLinks.tezosGovernanceLink")}>
          {t("header.governanceLink")}
        </NaviLink>
        <a href={t("tezosLinks.tzipExplorerLink")}>
          {t("header.tzipExplorerLink")}
        </a>
      </div>
      <div
        className={styles.header__expandMenuButton}
        onClick={(): void => setMenuOpen(true)}
      >
        <MenuIcon />
      </div>
      <div>
        <HeaderMenu
          isOpen={isMenuOpen}
          onClose={(): void => setMenuOpen(false)}
        />
      </div>
    </div>
  );
};

export default AgoraHeader;
