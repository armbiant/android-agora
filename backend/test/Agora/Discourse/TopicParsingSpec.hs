-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Discourse.TopicParsingSpec
  ( spec
  ) where

import Test.Hspec (Spec, describe, it, shouldBe)

import Agora.Discourse.API
import Agora.Discourse.Client
import Agora.Discourse.Types
import Agora.Indexer.Capability
import Agora.TestMode
import Agora.Types as AT

spec :: Spec
spec =
  describe "Topic parsing behaviour" $
    it "Topic parser treats last entry in parenthesis as a proposal hash" $ do
      topic <- runWithMockRefs $ \(discourseRef -> discourseIOTVar) -> do
        liftIO $ atomically $ writeTVar discourseIOTVar mockDiscourseRaw
        idxStartCacheRefresh
        idxWaitForCacheRefresh 2
        getProposalTopic (AT.Hash "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb")
      case topic of
        ExistingTopic (tTitle -> t) -> t `shouldBe` topicTitle
        _                           -> fail "Expected topic was not found.."
  where
    paren t = "(" <> t <> ")"
    hash1 = AT.Hash "0000000000"
    hash2 = AT.Hash "PsCARTHAGa"
    topicTitle = Title $ unwords
      [ paren (hashToText hash1)
      , paren (hashToText hash2)
      ]

    mockDiscourseRaw = DiscourseEndpointsIO
      { deGetCategoryTopicsIO = \_ _ -> pure $ CategoryTopics 0 (one $ TopicHead 0 topicTitle)
      , deGetCategoriesIO = pure $ CategoryList $ one $ Category 0 testDiscourseCategory
      , deGetTopicIO = \_ -> pure $ MkTopic 0 topicTitle (one $ Post 0 0 "")
      , dePostTopicIO = error "Not supported"
      , deGetPostIO = error "Not supported"
      }
