-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
{-# LANGUAGE NumericUnderscores #-}

module Indexer.ServerPoolSpec
  ( spec
  ) where

import GHC.Stack
import GHC.TypeLits
import Data.Text.Encoding
import Data.Text qualified as T
import Data.Time.Clock.POSIX
import Data.Bifunctor
import Control.Concurrent (threadDelay)
import Control.Concurrent.STM.TVar
import Control.Exception
import Control.Monad.Reader
import qualified Data.Map.Strict as Map
import Data.Typeable
import Monad.Capabilities
import Network.HTTP.Types.Status
import Network.HTTP.Types.Version
import System.Log.FastLogger
import System.Random
import Servant.Client.Core.Request
import Test.Hspec (Spec, describe, it)
import UnliftIO.Async

import Logging
import Servant.Client
import ServerPool

-- We use the following types to control the response behavior from mock
-- servers. For examples, @LegacyBlockResponse@ sent to non archive nodes
-- will always return a 404 not found error. Requests for @ServerOne500@
-- always results in 500 internal error only at one of the servers, but
-- respond success when using other servers. In this manner, we are hope
-- to mimic real life behavior of nodes.
data BlockResponse = BlockResponse
data LegacyBlockResponse = LegacyBlockResponse
data ISE500Response (idx :: Nat) = ISE500Response

data ConnectionErr = ConnectionErr
  deriving Show

instance Exception ConnectionErr

mkErrorResponse :: HasCallStack => BaseUrl -> Status -> ClientError
mkErrorResponse baseUrl s = FailureResponse
  (bimap (const ()) (const (baseUrl, "")) defaultRequest) $
  Response s mempty http11 ""

randomFail :: BaseUrl -> a -> IO (Either ClientError a)
randomFail baseUrl act = do
  x <- randomRIO (0, 100 :: Int)
  if
    | x < 5 -> pure $ Left $ mkErrorResponse baseUrl status500
    | x < 10 -> pure $ Left $ ConnectionError (toException ConnectionErr)
    | x < 11 -> do
          threadDelay 1000_000_00 -- 100 secs
          error "Timeout expected, but didn't hit it"
    | otherwise -> pure $ Right act

mockServer
  :: forall a. Typeable a
  => Bool
  -> Int
  -> BaseUrl
  -> ClientM a
  -> IO (Either ClientError a)
mockServer isArchive idx baseUrl _ =
  case ( eqT @a @BlockResponse, eqT @a @LegacyBlockResponse
       , eqT @a @(ISE500Response 1), eqT @a @(ISE500Response 2)
       , eqT @a @(ISE500Response 3)) of
    (Just Refl, Nothing, Nothing, Nothing, Nothing) ->
      randomFail baseUrl BlockResponse
    (Nothing, Just Refl, Nothing, Nothing, Nothing) ->
      if isArchive -- We suppose archive nodes never return 404 here
        then randomFail baseUrl LegacyBlockResponse
        -- While non archive nodes always return 404 for legacy blocks
        else pure $ Left $ mkErrorResponse baseUrl status404
    (Nothing, Nothing, Just Refl, Nothing, Nothing)
      | idx == 1 -> pure $ Left $ mkErrorResponse baseUrl status500
      | otherwise -> randomFail baseUrl ISE500Response

    (Nothing, Nothing, Nothing, Just Refl, Nothing)
      | idx == 2 -> pure $ Left $ mkErrorResponse baseUrl status500
      | otherwise -> randomFail baseUrl ISE500Response

    (Nothing, Nothing, Nothing, Nothing, Just Refl)
      | idx == 3 -> pure $ Left $ mkErrorResponse baseUrl status500
      | otherwise -> randomFail baseUrl ISE500Response
    _ -> error "Implossible"

parseBaseUrlUnsafe :: String -> BaseUrl
parseBaseUrlUnsafe s = case parseBaseUrl s of
  Just x -> x
  Nothing -> error "Unexpected"

mkMockServerInfo
  :: (forall a. Typeable a => ClientM a -> IO (Either ClientError a))
  -> BaseUrl
  -> ServerInfo
mkMockServerInfo action burl = ServerInfo
  { sendReq = action
  , config = emptyServerConfig { minSecsBetweenReqs = 0.000_1 }
  , serverStatus = ServerStatus Nothing True 0 0 0 Nothing 0
  , baseUrl = burl
  }

sendRandomRequests
  :: HasCaps '[LoggingCap, ServerPoolCap] caps => CapsT caps IO ()
sendRandomRequests = do
  requests <- forM [(1::Int)..20] (\_ -> async sendRandomRequest)
  processResponses requests
  where
    processResponses [] = pure ()
    processResponses (request : rst) = do
      _ <- wait request
      x <- randomRIO (0, 1_000 :: Int)
      if x < 100
        then mapM_ cancel rst
        else processResponses rst

sendRandomRequest
  :: HasCaps '[LoggingCap, ServerPoolCap] caps => CapsT caps IO ()
sendRandomRequest = do
  randomRIO (1, 100 :: Int) >>= \case
    x
      | x < 5 -> void $ runRequest (pure LegacyBlockResponse)
      | x < 10 -> void $ runRequest (pure (ISE500Response @1))
      | x < 15 -> void $ runRequest (pure (ISE500Response @2))
      | x < 25 -> void $ runRequest (pure (ISE500Response @3))
      | otherwise -> void $ runRequest (pure BlockResponse)
  logDebug "REQUEST SUCCESS!!!"

spec :: Spec
spec =
  describe "ServerPool behavior" $ do
    it "will not enter an endless wait or throw an exception while serving requests" $ do
    -- Here we simulate one archive node and two non-archive nodes. These
    -- simulated archive nodes never return 404. But non archive nodes will
    -- always return 404 for requests targeting @LegacyBlockResponse@. For
    -- @BlockResponse@ all the nodes randomly throw @ConnectionError@ or
    -- Internal Server Error status (500) or simply wait for 100 seconds
    -- without returning anything (expecting the calling code to timeout the request).
    --
    -- Given this behavior, we expect all the requests to be served by one of
    -- the three servers.
      let
        serverInfos = let
          -- The second argument is the server index, which is used to
          -- decide if the ServerOne500, ServerTwo500 errors should be thrown
          -- on the requests to the server. So if the index is 1 for a server and
          -- the response is ServerOne500, then a 500 Internal server is thrown,
          -- but is responded with success for servers of all other indexes.
          mkt burl idx isarch = let
            baseUrl = parseBaseUrlUnsafe burl
            in (baseUrl, mkMockServerInfo (mockServer isarch idx baseUrl) baseUrl)
          in Map.fromList
            [ mkt "api.archive" 1 True
            , mkt "api.server1" 2 False
            , mkt "api.server2" 3 False
            ]
      serverInfoRef <- liftIO $ newTVarIO serverInfos
      requestIdRef <- liftIO $ newTVarIO 1_000
      let serverPoolRefs = ServerPoolRefs serverInfoRef requestIdRef
      withTimedFastLogger (getPOSIXTime >>= (\x -> pure $ encodeUtf8 $  T.pack $ show x)) (LogStdout defaultBufSize) $ \fl -> do
        runReaderT (replicateM_ 1_000 sendRandomRequests) $ buildCaps $
          AddCap (serverPoolCap serverPoolRefs) $
          AddCap (loggingCap (mkLogFn fl) LogDebug) $
          BaseCaps emptyCaps
      where
        mkLogFn :: TimedFastLogger -> LogStr -> IO ()
        mkLogFn logger msg = logger (\time -> toLogStr time <> ": " <> msg)

