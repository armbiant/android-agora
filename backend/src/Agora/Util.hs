-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-|
Dump for generic stuff which has nowhere else to go
-}
module Agora.Util
       ( NetworkAddress (..)
       , ConnString (..)
       , hoistClient
       , waitFor
       , hoistStreamingClient
       , HasId (..)
       , Amount (..)
       , buildFromJSON
       , ApiUsername (..)
       , ApiKey (..)
       , TagEnum (..)
       , buildTag
       , toJSONTag
       , isServantStatus
       , parseJSONTag
       , parseUTCTime
       , declareNamedSchemaTag
       , getClientEnv
       , suppressException
       , ordNubBy
       , prettyL
       , pretty
       , runClientM_
       ) where

import Data.Aeson
  (FromJSON(..), ToJSON(..), Value(..), encode, withText)
import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Types (Parser)
import Data.List (elemIndex, (!!))
import qualified Data.Map.Strict as M
import qualified Data.Swagger as S
import qualified Data.Swagger.Declare as S
import qualified Data.Swagger.Internal.Schema as S
import Data.Text.Lazy.Builder (fromText, toLazyText)
import Data.Time.Clock (UTCTime)
import Data.Time.Format (defaultTimeLocale, parseTimeOrError)
import Data.Time.Units (Second, toMicroseconds)
import Data.Typeable (typeRep)
import Fmt (Buildable(..), Builder, (+|), (|+))
import Lens.Micro.Platform ((?=))
import Loot.Log (MonadLogging)
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Network.HTTP.Types.Status (Status)
import Servant.API (FromHttpApiData(..), ToHttpApiData(..))
import Servant.API.Stream (SourceIO)
import qualified Servant.Client as SC (ClientM, mkClientEnv, runClientM)
import Servant.Client.Streaming
  (BaseUrl, ClientEnv, ClientError(..), ClientM, responseStatusCode, showBaseUrl, withClientM)
import Servant.Types.SourceT (foreach)
import qualified Text.ParserCombinators.ReadP as ReadP
import Text.Read (Read(..), read)
import qualified Text.Show
import UnliftIO (MonadUnliftIO)
import qualified UnliftIO as UIO
import qualified UnliftIO.Concurrent as UIO

import Agora.Util.JsonOptions (defaultOptions)

---------------------------------------------------------------------------
-- Network-related stuff
---------------------------------------------------------------------------

-- | Datatype which contains info about socket network address
data NetworkAddress = NetworkAddress
  { naHost :: !Text
  , naPort :: !Word16
  } deriving (Eq, Ord, Generic)

instance Buildable NetworkAddress where
  build NetworkAddress {..} = ""+|naHost|+":"+|naPort|+""

instance Show NetworkAddress where
  show = toString . pretty

instance Read NetworkAddress where
  readsPrec _ = ReadP.readP_to_S addrParser
    where
      addrParser = NetworkAddress
        <$> (parseHost <* ReadP.char ':')
        <*> parsePort
      parseHost = toText <$> ReadP.munch (/= ':')
      parsePort = ReadP.readS_to_P reads

instance IsString NetworkAddress where
  fromString = read

instance FromJSON NetworkAddress where
  parseJSON = withText "NetworkAddress" $ pure . read . toString

instance ToJSON NetworkAddress where
  toJSON = String . pretty

-- | Hoist a servant client by running it and, in case of an error,
-- wrapt it and throw.
-- Note: does not work with a streaming client. Use ''hoistStreamingClient''.
hoistClient
  :: (Exception e, MonadUnliftIO m)
  => (ClientError -> e)
  -> ClientEnv
  -> (forall x. {- NFData x => -} ClientM x -> m x)
-- FIXME: Switch to @runClientM@ from streaming. It requries NFData on all
-- data and we depend on some Tezos libs that do not have it.
hoistClient errWrapper env clientM = UIO.withRunInIO $ \_runIO ->
  withClientM clientM env $ \case
    Left e  -> UIO.throwIO $ errWrapper e
    Right x -> pure x

-- | Hoist a servant client by running it and, in case of an error,
-- wrapt it and throw. See also ''hoistClient''.
hoistStreamingClient
  :: (Exception e, MonadUnliftIO m)
  => (ClientError -> e)
  -> ClientEnv
  -> (forall x. (x -> m ()) -> ClientM (SourceIO x) -> m ())
hoistStreamingClient errWrapper env onItem clientM = UIO.withRunInIO $ \runIO ->
  withClientM clientM env $ \case
    Left e  -> UIO.throwIO $ errWrapper e
    Right xs -> foreach fail (runIO . onItem) xs

---------------------------------------------------------------------------
-- API-related stuff
---------------------------------------------------------------------------

-- | Class which represent the types values of which have unique IDs.
class HasId s where
    type IdT s :: Type
    type IdT s = s

    type TagT s :: Type

    getId :: s -> IdT s

    default getId :: (IdT s ~ s) => s -> IdT s
    getId = id

newtype Amount = Amount Word32
  deriving (Eq, Ord, Show, Generic, Num, Real, Integral, Enum, Buildable)

buildFromJSON :: ToJSON a => a -> Builder
buildFromJSON x = "" +| decodeUtf8 @Text (encode x) |+ ""

---------------------------------------------------------------------------
-- Discourse API-related stuff
---------------------------------------------------------------------------
newtype ApiUsername = ApiUsername Text
  deriving (Eq, Show, Generic, ToHttpApiData, FromHttpApiData)

newtype ApiKey = ApiKey Text
  deriving (Eq, Show, Generic, ToHttpApiData, FromHttpApiData)

---------------------------------------------------------------------------
-- DB-related stuff
---------------------------------------------------------------------------

-- | Newtype which denotes LIBPQ connection string.
-- Moved here to avoid import cycles between `Agora.Config` and `Agora.DB`.
-- Syntax: https://www.postgresql.org/docs/9.5/libpq-connect.html#LIBPQ-CONNSTRING
newtype ConnString = ConnString
  { unConnString :: ByteString
  } deriving (Show, Eq, Ord)

instance FromJSON ConnString where
  parseJSON = withText "ConnString" $ pure . ConnString . encodeUtf8

instance ToJSON ConnString where
  toJSON = String . decodeUtf8 . unConnString

instance Buildable ConnString where
  build (ConnString s) = ""+|decodeUtf8 @Text s|+""

---------------------------------------------------------------------------
-- Generic stuff
---------------------------------------------------------------------------

-- | Suppress an exception in passed action
-- and retry in @retryIn@ seconds until it succeeds.
suppressException
  :: forall e m a .
  ( MonadUnliftIO m
  , MonadLogging m
  , Exception e
  )
  => Second      -- ^ When action should be retried if something went wrong
  -> (e -> m ()) -- ^ Action in a case if something went wrong
  -> m a         -- ^ Action where Tezos client errors should be suspended
  -> m a
suppressException retryIn onFail action =
  action `UIO.catch` \e -> do
    onFail e
    UIO.threadDelay $ fromIntegral $ toMicroseconds retryIn
    suppressException retryIn onFail action

runClientM_
  :: ClientEnv
  -> SC.ClientM a
  -> IO a
runClientM_ env c = SC.runClientM c env >>= \case
    Right a  -> pure a
    Left err -> UIO.throwIO err

waitFor :: Second -> MonadUnliftIO m => m ()
waitFor sec = UIO.threadDelay (fromIntegral $ toMicroseconds sec)

-- | Fast `nubBy` for items with `Ord` key.
ordNubBy :: Ord b => (a -> b) -> [a] -> [a]
ordNubBy f = map snd . M.toList . M.fromList . map (\v -> (f v, v))

prettyL :: Buildable a => a -> LText
prettyL = toLazyText . build

pretty :: Buildable a => a -> Text
pretty = toStrict . prettyL

-- | Class for enums which have a defined text representation
class (Typeable a, Bounded a, Enum a) => TagEnum a where
  {-# MINIMAL toTag | enumVals #-}
  enumVals :: Proxy a -> [Text]
  enumVals _ = map toTag $ enumFromTo @a minBound maxBound

  enumName :: Proxy a -> Text
  enumName = show . typeRep

  enumDesc :: Proxy a -> Text
  enumDesc = enumName

  toTag :: a -> Text
  toTag a = enumVals (Proxy @a) !! fromEnum a

  fromTag :: Text -> Maybe a
  fromTag t = toEnum <$> elemIndex t (enumVals $ Proxy @a)

buildTag :: TagEnum a => a -> Builder
buildTag = fromText . toTag

toJSONTag :: TagEnum a => a -> Value
toJSONTag = String . toTag

parseJSONTag :: forall a . TagEnum a => Value -> Parser a
parseJSONTag = withText (toString $ enumName $ Proxy @a) $ \t ->
  maybe (fail $ "Invalid value: " ++ toString t) pure $
  fromTag t

parseUTCTime :: String -> UTCTime
parseUTCTime = parseTimeOrError False defaultTimeLocale "%Y-%m-%dT%H:%M:%SZ"

getClientEnv :: BaseUrl -> IO ClientEnv
getClientEnv burl = do
  manager <- newManager tlsManagerSettings
  pure $ SC.mkClientEnv manager (fromMaybe (error "") (Just burl))

declareNamedSchemaTag :: forall a proxy . TagEnum a => proxy a -> S.Declare (S.Definitions S.Schema) S.NamedSchema
declareNamedSchemaTag _ =
  return $ S.named (enumName $ Proxy @a) $ mempty `executingState` do
    S.description ?= enumDesc (Proxy @a)
    S.enum_ ?= map String (enumVals $ Proxy @a)

instance Buildable BaseUrl where
  build b = "" +| toText (showBaseUrl b) |+ ""

isServantStatus :: ClientError -> Status -> Maybe Bool
isServantStatus (FailureResponse _ r) status = Just $ responseStatusCode r == status
isServantStatus _ _ = Nothing

---------------------------------------------------------------------------
-- Derivations
---------------------------------------------------------------------------

deriveJSON defaultOptions ''ApiUsername
deriveJSON defaultOptions ''ApiKey
deriveJSON defaultOptions ''Amount
