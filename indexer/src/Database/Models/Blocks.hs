-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.Blocks
  ( BlockId
  , Block
  , BlockPoly(..)
  , blocksTable
  , blockToDbBlock
  ) where

import Data.Time
import System.Log.FastLogger

import Database.Mapping
import Node (BlockHash, ChainId, Level)
import Node qualified

type BlockId = DbId "block"

data BlockPoly id blockHash chainId level predecessorHash blockTime  = Block
  { id              :: id
  , hash            :: blockHash
  , chainId         :: chainId
  , level           :: level
  , predecessorHash :: predecessorHash
  , blockTime       :: blockTime
  }

instance
  (ToLogStr blockHash, ToLogStr level) =>
    ToLogStr (BlockPoly id blockHash
      chainId level predecessorHash blockTime) where
  toLogStr (Block _ hash _ level _ _) =
    "Block: (" <> toLogStr hash <> " at level " <> toLogStr level <> ")"

$(makeAdaptorAndInstance "pDbBlock" ''BlockPoly)

type Block'' rm = BlockPoly
  (ComputeFieldTypeAuto rm BlockId)
  (ComputeFieldType rm BlockHash)
  (ComputeFieldType rm ChainId)
  (ComputeFieldType rm Level)
  (ComputeFieldType rm BlockHash)
  (ComputeFieldType rm UTCTime)

blockToDbBlock
  :: Node.Block
  -> Block'' 'HaskellWrite
blockToDbBlock blk = Block
  Nothing
  blk.hash
  blk.chainId
  blk.header.level
  blk.header.predecessor
  blk.header.timestamp

type Block = Block'' 'HaskellRead

blocksTable
  :: Table (Block'' 'DbWrite) (Block'' 'DbRead)
blocksTable =
  table "blocks" (pDbBlock
    (Block
      (tableField "id")
      (tableField "hash")
      (tableField "chain_id")
      (tableField "level")
      (tableField "predecessor_hash")
      (tableField "blocktime")))
