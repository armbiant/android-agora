-- SPDX-FileCopyrightText: 2020-2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.BlockCache
  ( BlockCache
  , BlockCache''
  , BlockCache'(..)
  , blockCacheTable
  ) where

import Database.Mapping
import Node qualified

data BlockCache' level blockHash content = BlockCache
  { level            :: level
  , hash    :: blockHash
  , content     :: content
  } deriving stock Show

$(makeAdaptorAndInstance "pDbBlockCache" ''BlockCache')

type BlockCache'' rm = BlockCache'
  (ComputeFieldType rm Node.Level)
  (ComputeFieldType rm Node.BlockHash)
  (ComputeFieldType rm Node.Block)

type BlockCache = BlockCache'' 'HaskellRead

blockCacheTable
  :: Table (BlockCache'' 'DbWrite) (BlockCache'' 'DbRead)
blockCacheTable =
  table "block_cache" (pDbBlockCache
    (BlockCache
      (tableField "level")
      (tableField "hash")
      (tableField "content")))
