# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ pkgs ? import ./../nix {} }: with pkgs;

mkShell {
  buildInputs = with nodePackages; [
    nodejs
  ];
}
