# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
# SPDX-License-Identifier: AGPL-3.0-or-later

{
  nixConfig = {
    flake-registry = "https://github.com/serokell/flake-registry/raw/master/flake-registry.json";
  };

  inputs = {
    hackage.flake = false;
    stackage.flake = false;
    tezos-packaging.url = "github:serokell/tezos-packaging/octez-v20.0-1";
  };

  outputs = { self, nixpkgs, nix-npm-buildpackage, flake-utils, haskell-nix
    , serokell-nix, deploy-rs, hackage, stackage, tezos-packaging }:
    {
      overlay = final: prev: {
        agora = {
          frontend = final.callPackage ./frontend { };
          backend = final.callPackage ./backend { };
          inherit (final.callPackage ./docker.nix {
            agora = self.packages.${final.system};
          })
            backend-image frontend-image;
        };
      };

      nixosModules = {
        agora-backend =
          import ./nix/modules/services/backend.nix { inherit self; };
        agora-frontend =
          import ./nix/modules/services/frontend.nix { inherit self; };
        agora = import ./nix/modules/services/combined.nix { inherit self; };
      };

      deploy = let
        mkNode = hostname: {
          inherit hostname;
          user = "deploy";
          profiles = {
            agora-backend.path = deploy-rs.lib.x86_64-linux.activate.custom
              self.packages.x86_64-linux.agora-backend
              "sudo /run/current-system/sw/bin/systemctl restart agora";
            agora-frontend.path = deploy-rs.lib.x86_64-linux.activate.noop
              self.packages.x86_64-linux.agora-frontend;
            agora-indexer.path = deploy-rs.lib.x86_64-linux.activate.custom
              self.packages.x86_64-linux.indexer ''
                sudo /run/current-system/sw/bin/systemctl restart agora-indexer-api
                sudo /run/current-system/sw/bin/systemctl restart agora-indexer-sync
              '';
          };
        };
      in {
        sshOpts = [ "-p 17788" ];
        nodes = {
          staging = mkNode "agora.tezos.serokell.team";
          production = mkNode "www.tezosagora.org";
        };
      };

      nixosConfigurations.container = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          self.nixosModules.agora

          ({ config, pkgs, lib, ... }: {
            system.configurationRevision = lib.mkIf (self ? rev) self.rev;
            boot.isContainer = true;
            networking.useDHCP = false;
            networking.firewall.allowedTCPPorts = [ 80 ];
            networking.hostName = "agora";

            services.agora = {
              enable = true;
              frontend.fqdn = "agora";
              backend.config.discourse = {
                host = "https://forum.tezosagora.org";
                category = "None";
                api_username = "nouser";
                api_key = "";
              };
            };

            services.nginx = {
              enable = true;
              recommendedProxySettings = true;
              virtualHosts = {
                agora = {
                  default = true;
                  forceSSL = lib.mkForce false;
                  enableACME = lib.mkForce false;
                };
              };
            };
          })
        ];
      };
    } // flake-utils.lib.eachSystem [ "x86_64-linux" "x86_64-darwin" ] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system}.extend (nixpkgs.lib.composeManyExtensions [
          nix-npm-buildpackage.overlays.default
          haskell-nix.overlay
          serokell-nix.overlay
          (_: prev: prev // {
            inherit (tezos-packaging.packages.${system}) octez-client octez-node;
          })
        ]);

        # Import project derivations
        backend = import ./backend/release.nix { inherit pkgs; };
        frontend = import ./frontend/release.nix { inherit pkgs; };
        indexer = import ./indexer/release.nix { inherit pkgs; };
        docker =
          pkgs.callPackage ./docker.nix { agora = self.packages.${system}; };
      in {
        packages = {
          inherit (backend) agora-backend agora-backend-config;
          inherit (indexer) indexer;

          inherit (frontend) agora-frontend;

          inherit (docker) backend-image frontend-image;
        };
        legacyPackages = pkgs;
        checks = {
          inherit (backend)
            agora-backend-checks agora-backend-haddock agora-backend-hlint;
          inherit (indexer) indexer-checks;

          agora-backend-whitespace =
            pkgs.build.checkTrailingWhitespace ./backend;
          agora-frontend-whitespace =
            pkgs.build.checkTrailingWhitespace ./frontend;
          reuse = pkgs.runCommand "reuse-lint" { buildInputs = [ pkgs.reuse ]; } ''
             cd ${./.}
             reuse lint
             touch $out
          '';
        } // deploy-rs.lib.${system}.deployChecks self.deploy;
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.skopeo
            pkgs.bash
            deploy-rs.defaultPackage.${system}
          ];
          inputsFrom = [
            (backend.project.agora.project.shellFor {
              packages = ps: builtins.attrValues (pkgs.haskell-nix.haskellLib.selectLocalPackages ps);
              withHoogle = false;
              allToolDeps = false;
            })
            self.packages.${system}.agora-frontend
          ];
        };

      });
}
