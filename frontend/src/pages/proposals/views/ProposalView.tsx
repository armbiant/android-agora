// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, {
  FunctionComponent,
  ReactElement,
  useEffect,
  useState,
} from "react";
import { useTranslation } from "react-i18next";
import { LayoutContent } from "~/components/common/Layout";
import styles from "~/styles/pages/proposals/ProposalStagePage.scss";
import { Api } from "~/api/api";
import { ProposalPeriodInfo } from "~/models/Period";
import ProposalPieChart from "~/components/proposals/graphs/ProposalPieChart";
import ProposalsList from "~/components/proposals/ProposalsList";
import ParticipationTracker from "~/components/proposals/ParticipationTracker";
import ProposalVotesTable from "~/components/proposals/table/ProposalVotesTable";
import RecentVotes from "~/components/proposals/RecentVotes";
import { useSelector } from "react-redux";
import {
  ProposalVotesListItem,
  ProposalVotesList,
} from "~/models/ProposalVotesList";
import { ProposalsList as ProposalsListType } from "~/models/ProposalsList";
import { RootStoreType } from "~/store";

interface ProposalViewProps {
  period: ProposalPeriodInfo;
}

const ProposalView: FunctionComponent<ProposalViewProps> = ({
  period,
}): ReactElement => {
  const { t } = useTranslation();
  const [
    proposalVotesFull,
    setProposalVotesFull,
  ] = useState<null | ProposalVotesList>(null);
  const proposalVotes: ProposalVotesListItem[] = useSelector(
    ({ periodStore }: RootStoreType): ProposalVotesListItem[] => {
      return periodStore.proposalVotes ? periodStore.proposalVotes.data : [];
    }
  );

  const proposals: ProposalsListType | null = useSelector(
    ({ periodStore }: RootStoreType): ProposalsListType | null => {
      if (!periodStore.proposalsLoading && periodStore.proposals)
        return periodStore.proposals;
      return null;
    }
  );

  useEffect((): void => {
    Api.agoraApi
      .getProposalVotes(period.period.id, undefined, 20)
      .then((r): void => setProposalVotesFull(r));
  }, [period.period.id]);

  let handleShowAllProposalVotes = (): void => {
    if (proposalVotesFull) {
      if (proposalVotesFull.pagination.rest > 0) {
        Api.agoraApi
          .getProposalVotes(
            period.period.id,
            proposalVotesFull.pagination.lastId,
            proposalVotesFull.pagination.rest
          )
          .then((r): void =>
            setProposalVotesFull({
              pagination: r.pagination,
              results: [...proposalVotesFull.results, ...r.results],
            })
          );
      }
    }
  };

  var ShowAllButton = <span />;

  if (proposalVotesFull && proposalVotesFull.pagination.rest > 0) {
    var ShowAllButton = (
      <button
        className={styles.bakers__showAllButton}
        onClick={handleShowAllProposalVotes}
      >
        {t("common.showAll")}
      </button>
    );
  }

  return (
    <>
      <LayoutContent className={styles.period__primaryInfo}>
        <div>
          <div className={styles.left}>
            <ProposalPieChart className={styles.proposal__info__chart} />
          </div>
          <div className={styles.right}>
            <div className={styles.right__top}>
              <RecentVotes votes={proposalVotes} />
            </div>
            <div className={styles.right__bottom}>
              <ParticipationTracker
                className={styles.proposal__info__votersInfo}
                voteStats={period.voteStats}
              />
            </div>
          </div>
        </div>
      </LayoutContent>
      <LayoutContent className={styles.period__secondaryInfo}>
        {proposals ? (
          <ProposalsList
            className={styles.proposal__info__proposalList}
            proposals={proposals}
            period={period}
            votesAvailable={period.voteStats.votesAvailable}
          />
        ) : null}
      </LayoutContent>
      {!!proposalVotesFull ? (
        <LayoutContent className={styles.period__secondaryInfo}>
          <ProposalVotesTable
            data={proposalVotesFull.results}
            className={styles.bakers__table}
          />
          {ShowAllButton}
        </LayoutContent>
      ) : null}
    </>
  );
};

export default ProposalView;
