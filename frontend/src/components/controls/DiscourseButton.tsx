// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/controls/DiscourseButton.scss";
import { useTranslation } from "react-i18next";

interface DiscourseButtonTypes {
  className?: string;
  href: string;
}

const DiscourseButton: FunctionComponent<DiscourseButtonTypes> = ({
  className,
  href,
}): ReactElement => {
  const { t } = useTranslation();

  return (
    <>
      <a
        className={cx(className, styles.button, styles.button_desktop)}
        href={href}
      >
        {t("common.discourseButton")}
      </a>
      <a
        className={cx(className, styles.button, styles.button_mobile)}
        href={href}
      >
        {t("common.discourseButtonMobile")}
      </a>
    </>
  );
};

export default DiscourseButton;
