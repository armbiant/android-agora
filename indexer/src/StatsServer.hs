-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | An http web server to serve node communication statistics.
module StatsServer
  ( serveStats
  ) where

import Control.Concurrent.STM.TVar
import Data.Map
import Data.Time.Clock (NominalDiffTime)
import GHC.Generics
import Network.Wai.Handler.Warp (run)
import Servant (Proxy(..), ToServant)
import Servant.API (Get, JSON, (:>))
import Servant.API.Generic (AsApi, (:-))
import Deriving.Aeson
import Deriving.Aeson.Stock
import Servant.Client
import Servant.Server (Application, Handler, serve)
import Servant.Server.Generic (AsServerT, genericServerT)

import Common
import ServerPool

data ServerStatistics = ServerStatistics
  { requests :: Integer
  , success :: Integer
  , averageResponseTime :: Maybe NominalDiffTime
  , timeouts :: Integer
  } deriving (Show, Generic)
    deriving (FromJSON, ToJSON) via (Snake ServerStatistics)

data StatsEndpoints route = StatsEndpoints
  { neGetStats_ :: route
      :- "stats"
      :> Get '[JSON] (Map BaseUrl ServerStatistics)
  } deriving Generic

type StatsApi  =
  ToServant StatsEndpoints AsApi

api :: Proxy StatsApi
api = Proxy

type StatsApiHandlers m = ToServant StatsEndpoints (AsServerT m)

statsHandlers :: TVar (Map BaseUrl ServerInfo) -> StatsApiHandlers Handler
statsHandlers serverInfosRef = genericServerT StatsEndpoints
  { neGetStats_ = handleStats serverInfosRef
  }

serverInfoToStatistics :: ServerInfo -> ServerStatistics
serverInfoToStatistics ServerInfo { serverStatus = ServerStatus {..}} =
  ServerStatistics
    { requests = requestsCount
    , success = successCount
    , averageResponseTime = averageResponseTime
    , timeouts = timeoutsCount
    }

handleStats
  :: TVar ServerInfos
  -> Handler (Map BaseUrl ServerStatistics)
handleStats serverInfosRef = liftIO $ do
   serverInfos <- readTVarIO serverInfosRef
   pure $ serverInfoToStatistics <$> serverInfos

apiApp :: TVar (Map BaseUrl ServerInfo) -> Application
apiApp serverInfosRef = serve api (statsHandlers serverInfosRef)

serveStats :: Int -> TVar (Map BaseUrl ServerInfo) -> IO ()
serveStats port serverInfosRef = do
  run port (apiApp serverInfosRef)
