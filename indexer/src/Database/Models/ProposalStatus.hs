-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.ProposalStatus
  ( ProposalStatus
  , ProposalStatusPoly(..)
  , ProposalStatusId
  , proposalStatusTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Database.Models.Proposals qualified as Proposals
import Node qualified

type ProposalStatusId = DbId "proposalStatus"

data ProposalStatusPoly id proposalId status sourceBlockId = ProposalStatus
  { id         :: id
  , proposalId   :: proposalId
  , status :: status
  , sourceBlockId :: sourceBlockId
  }

$(makeAdaptorAndInstance "pDbProposalStatus" ''ProposalStatusPoly)

type ProposalStatus'' rm = ProposalStatusPoly
  (ComputeFieldTypeAuto rm ProposalStatusId)
  (ComputeFieldType rm Proposals.ProposalId)
  (ComputeFieldType rm Node.ProposalStatus)
  (ComputeFieldType rm Blocks.BlockId)

type ProposalStatus = ProposalStatus'' 'HaskellRead

proposalStatusTable
  :: Table (ProposalStatus'' 'DbWrite) (ProposalStatus'' 'DbRead)
proposalStatusTable =
  table "proposal_status" (pDbProposalStatus
    (ProposalStatus
      (tableField "id")
      (tableField "proposal_id")
      (tableField "status")
      (tableField "source_block_id")))
