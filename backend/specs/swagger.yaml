# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

definitions:
  Amount:
    description: Number of rest entries
    maximum: 4294967295
    minimum: 0
    type: integer
  HistoricalConstants:
    properties:
      periodsPerEpoch:
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        type: integer
    required:
    - periodsPerEpoch
    type: object
  Constants:
    properties:
      blocksPerCycle:
        format: int64
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        type: integer
      blocksPerVoting:
        format: int64
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        type: integer
      timeBetweenBlocks:
        format: int64
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        type: integer
    type: object
  Baker:
    properties:
      logoUrl:
        type: string
      name:
        type: string
      pkh:
        $ref: '#/definitions/Hash'
      profileUrl:
        type: string
      votingPower:
        $ref: '#/definitions/Mutez'
    required:
    - pkh
    - votingPower
    - name
    type: object
  Ballot:
    properties:
      author:
        $ref: '#/definitions/Baker'
      decision:
        $ref: '#/definitions/Decision'
      id:
        $ref: '#/definitions/BallotId'
      operation:
        $ref: '#/definitions/Hash'
      timestamp:
        $ref: '#/definitions/UTCTime'
    required:
    - id
    - author
    - decision
    - operation
    - timestamp
    type: object
  BallotId:
    description: Ballot id
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  Ballots:
    properties:
      nay:
        $ref: '#/definitions/Votes'
      pass:
        $ref: '#/definitions/Votes'
      quorum:
        format: float
        type: number
      supermajority:
        format: float
        type: number
      yay:
        $ref: '#/definitions/Votes'
    required:
    - yay
    - nay
    - pass
    - quorum
    - supermajority
    type: object
  Cycle:
    description: Cycle number
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  Decision:
    description: Ballot decision
    enum:
    - yay
    - nay
    - pass
  Hash:
    description: Base58 hash value
    format: byte
    type: string
  Level:
    description: Block level
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  Limit:
    description: Requested number of entries to return
    maximum: 4294967295
    minimum: 0
    type: integer
  Mutez:
    description: An amount in Mutez
    maximum: 18446744073709551615
    minimum: 0
    type: integer
  PaginatedList:
    properties:
      pagination:
        $ref: '#/definitions/PaginationData'
      results:
        items:
          $ref: '#/definitions/Ballot'
        type: array
    required:
    - pagination
    - results
    type: object
  PaginationData:
    properties:
      lastId:
        format: int64
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        type: integer
      limit:
        $ref: '#/definitions/Limit'
      rest:
        $ref: '#/definitions/Amount'
    required:
    - rest
    type: object
  Period:
    properties:
      historicalConstants:
        $ref: '#/definitions/HistoricalConstants'
      constants:
        $ref: '#/definitions/Constants'
      curLevel:
        $ref: '#/definitions/Level'
      cycle:
        $ref: '#/definitions/Cycle'
      endLevel:
        $ref: '#/definitions/Level'
      endTime:
        $ref: '#/definitions/UTCTime'
      id:
        $ref: '#/definitions/PeriodId'
      startLevel:
        $ref: '#/definitions/Level'
      startTime:
        $ref: '#/definitions/UTCTime'
      totalCycles:
        format: int32
        maximum: 4294967295
        minimum: 0
        type: integer
    required:
    - id
    - startLevel
    - curLevel
    - endLevel
    - startTime
    - endTime
    - cycle
    - totalCycles
    - constants
    - historicalConstants
    type: object
  PeriodId:
    description: Period number
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  PeriodInfo:
    maxProperties: 1
    minProperties: 1
    properties:
      adoptionInfo:
        properties:
          discourseLink:
            type: string
          period:
            $ref: '#/definitions/Period'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          proposal:
            $ref: '#/definitions/Proposal'
          totalPeriods:
            format: int32
            maximum: 4294967295
            minimum: 0
            type: integer
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - discourseLink
        type: object
      explorationInfo:
        properties:
          advanced:
            type: boolean
          ballots:
            $ref: '#/definitions/Ballots'
          discourseLink:
            type: string
          period:
            $ref: '#/definitions/Period'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          proposal:
            $ref: '#/definitions/Proposal'
          totalPeriods:
            format: int32
            maximum: 4294967295
            minimum: 0
            type: integer
          voteStats:
            $ref: '#/definitions/VoteStats'
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - voteStats
        - ballots
        - discourseLink
        type: object
      promotionInfo:
        properties:
          advanced:
            type: boolean
          ballots:
            $ref: '#/definitions/Ballots'
          discourseLink:
            type: string
          period:
            $ref: '#/definitions/Period'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          proposal:
            $ref: '#/definitions/Proposal'
          totalPeriods:
            format: int32
            maximum: 4294967295
            minimum: 0
            type: integer
          voteStats:
            $ref: '#/definitions/VoteStats'
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - voteStats
        - ballots
        - discourseLink
        type: object
      proposalInfo:
        properties:
          discourseLink:
            type: string
          period:
            $ref: '#/definitions/Period'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          totalPeriods:
            format: int32
            maximum: 4294967295
            minimum: 0
            type: integer
          voteStats:
            $ref: '#/definitions/VoteStats'
          winner:
            $ref: '#/definitions/Proposal'
        required:
        - period
        - totalPeriods
        - periodTimes
        - voteStats
        - discourseLink
        type: object
      testingInfo:
        properties:
          advanced:
            type: boolean
          discourseLink:
            type: string
          period:
            $ref: '#/definitions/Period'
          periodTimes:
            items:
              $ref: '#/definitions/PeriodItemInfo'
            type: array
          proposal:
            $ref: '#/definitions/Proposal'
          totalPeriods:
            format: int32
            maximum: 4294967295
            minimum: 0
            type: integer
        required:
        - period
        - totalPeriods
        - periodTimes
        - proposal
        - discourseLink
        type: object
    type: object
  PeriodItemInfo:
    properties:
      endTime:
        $ref: '#/definitions/UTCTime'
      periodType:
        $ref: '#/definitions/PeriodType'
      startTime:
        $ref: '#/definitions/UTCTime'
    required:
    - startTime
    - endTime
    - periodType
    type: object
  PeriodType:
    description: Period type
    enum:
    - proposal
    - testing_vote
    - testing
    - promotion_vote
    - adoption
  Proposal:
    properties:
      discourseLink:
        type: string
      firstPeriod:
        $ref: '#/definitions/PeriodId'
      hash:
        $ref: '#/definitions/Hash'
      longDescription:
        type: string
      minQuorum:
        $ref: '#/definitions/Quorum'
      period:
        $ref: '#/definitions/PeriodId'
      proposalFile:
        type: string
      proposer:
        $ref: '#/definitions/Baker'
      shortDescription:
        type: string
      timeCreated:
        $ref: '#/definitions/UTCTime'
      title:
        type: string
      votersNum:
        $ref: '#/definitions/Voters'
      votesCasted:
        $ref: '#/definitions/Votes'
    required:
    - period
    - firstPeriod
    - hash
    - timeCreated
    - proposer
    - votesCasted
    - votersNum
    - minQuorum
    type: object
  ProposalVote:
    properties:
      author:
        $ref: '#/definitions/Baker'
      id:
        $ref: '#/definitions/ProposalVoteId'
      operation:
        $ref: '#/definitions/Hash'
      proposal:
        $ref: '#/definitions/Hash'
      proposalTitle:
        type: string
      timestamp:
        $ref: '#/definitions/UTCTime'
    required:
    - id
    - proposal
    - author
    - operation
    - timestamp
    type: object
  ProposalVoteId:
    description: Proposal vote id
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  Quorum:
    description: A wrapper for Int32 used to represent Quorum values
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  UTCTime:
    example: 2016-07-22T00:00:00Z
    format: yyyy-mm-ddThh:MM:ssZ
    type: string
  VoteStats:
    properties:
      numVoters:
        $ref: '#/definitions/Voters'
      numVotersTotal:
        $ref: '#/definitions/Voters'
      votesAvailable:
        $ref: '#/definitions/Votes'
      votesCast:
        $ref: '#/definitions/Votes'
    required:
    - votesCast
    - votesAvailable
    - numVoters
    - numVotersTotal
    type: object
  Voters:
    description: Number of voters
    maximum: 2147483647
    minimum: -2147483648
    type: integer
  Votes:
    description: Number of votes
    maximum: 2147483647
    minimum: -2147483648
    type: integer
externalDocs:
  description: Find out more about Swagger
  url: http://swagger.io
info:
  contact:
    email: hi@serokell.io
    name: Serokell OÜ
    url: https://serokell.io
  title: Tezos Agora API
  version: 1.0.0
paths:
  /api/v1/ballots/{period_id}:
    get:
      parameters:
      - format: int64
        in: path
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: period_id
        required: true
        type: integer
      - format: int64
        in: query
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: lastId
        required: false
        type: integer
      - format: int32
        in: query
        maximum: 4294967295
        minimum: 0
        name: limit
        required: false
        type: integer
      - collectionFormat: null
        in: query
        items:
          enum:
          - yay
          - nay
          - pass
          type: string
        name: decisions
        required: false
        type: array
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PaginatedList'
        '400':
          description: Invalid `decisions` or `limit` or `lastId` or `period_id`
      summary: Ballots for given voting period.
  /api/v1/non_voters/{period_id}:
    get:
      parameters:
      - format: int64
        in: path
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: period_id
        required: true
        type: integer
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            items:
              $ref: '#/definitions/Baker'
            type: array
        '400':
          description: Invalid `period_id`
      summary: Bakers who didn't cast their vote so far.
  /api/v1/period:
    get:
      parameters:
      - format: int64
        in: query
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: id
        required: false
        type: integer
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PeriodInfo'
        '400':
          description: Invalid `id`
      summary: Info about given voting period
  /api/v1/proposal/{proposal_id}:
    get:
      parameters:
      - in: path
        name: proposal_id
        required: true
        type: string
      - format: int64
        in: query
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: period_id
        required: false
        type: integer
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/Proposal'
        '400':
          description: Invalid `period_id` or `proposal_id`
      summary: Info about specific proposal
  /api/v1/proposal/{proposal_id}/{period_id}/votes:
    get:
      parameters:
      - in: path
        name: proposal_id
        required: true
        type: string
      - format: int64
        in: path
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: period_id
        required: true
        type: integer
      - format: int64
        in: query
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: lastId
        required: false
        type: integer
      - format: int32
        in: query
        maximum: 4294967295
        minimum: 0
        name: limit
        required: false
        type: integer
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PaginatedList'
        '400':
          description: Invalid `limit` or `lastId` or `period_id` or `proposal_id`
      summary: Proposal votes issued for a given proposal
  /api/v1/proposal_votes/{period_id}:
    get:
      parameters:
      - format: int64
        in: path
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: period_id
        required: true
        type: integer
      - format: int64
        in: query
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: lastId
        required: false
        type: integer
      - format: int32
        in: query
        maximum: 4294967295
        minimum: 0
        name: limit
        required: false
        type: integer
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            $ref: '#/definitions/PaginatedList'
        '400':
          description: Invalid `limit` or `lastId` or `period_id`
      summary: Proposal votes for given proposal period.
  /api/v1/proposals/{period_id}:
    get:
      parameters:
      - format: int64
        in: path
        maximum: 9223372036854775807
        minimum: -9223372036854775808
        name: period_id
        required: true
        type: integer
      produces:
      - application/json;charset=utf-8
      responses:
        '200':
          description: ''
          schema:
            items:
              $ref: '#/definitions/Proposal'
            type: array
        '400':
          description: Invalid `period_id`
      summary: Proposals for given proposal period.
schemes:
- http
- https
swagger: '2.0'
