// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement, SVGProps } from "react";
import styles from "~/styles/components/common/Svg.scss";

const CheckIcon: FunctionComponent<SVGProps<SVGSVGElement>> = (
  props
): ReactElement => (
  <svg
    {...props}
    viewBox="-45 -100 448 512"
    className={styles.proposal_status_icon_svg}
  >
    <path d="M 124.78077,314.76205 376.08856,76.629734 c 8.53354,-8.086145 8.53354,-21.197659 0,-29.283804 L 345.18453,18.062127 c -8.53354,-8.0861449 -22.37049,-8.0874391 -30.90539,0 L 109.32807,212.2668 13.641255,121.59662 c -8.5335395,-8.08614 -22.3704929,-8.08614 -30.905398,0 l -30.904032,29.28381 c -8.533539,8.08614 -8.533539,21.19765 0,29.2838 L 93.875371,314.76075 c 8.534909,8.08874 22.370489,8.08874 30.905399,0.001 z"></path>
  </svg>
);

export default CheckIcon;
