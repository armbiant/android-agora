// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import * as React from "react";
import * as renderer from "react-test-renderer";
import "../testUtils/setupTest";
import Header from "../../src/components/common/Header";
import { StaticRouter } from "react-router";

describe("<Header />", (): void => {
  it("renders", (): void => {
    const tree = renderer
      .create(
        <StaticRouter>
          <Header />
        </StaticRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
