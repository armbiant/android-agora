-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -F -pgmF autoexporter #-}
{-# OPTIONS_GHC -Wno-dodgy-exports #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}
