#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2024 Tezos Commons
# SPDX-FileCopyrightText: 2021-2022 Oxhead Alpha
# SPDX-FileCopyrightText: 2020-2021 Tocqueville Group
# SPDX-License-Identifier: MIT

"""
A script that runs over Tezos amendment process.
"""

import argparse, json, os, shutil, subprocess, sys, tempfile, time
import urllib.request
import re
import random

from dataclasses import dataclass

known_protocols = {
    "paris": "PtParisBxoLz5gzMmn3d9WBQNoPSZakgnkMC2VNuQ3KXfUtUQeZ",
    "oxford": "ProxfordYmVfjWnRcgjWH36fW6PArwqykTFzotUxRs6gmTcZDuH",
}


def extract_protocol_hash(proto):
    return known_protocols.get(proto, proto)


parser = argparse.ArgumentParser()
parser.add_argument(
    "--node-config", required=False, help="Path to the node config file"
)
parser.add_argument(
    "--genesis-secret-key",
    required=False,
    help="Secret key for the genesis address, should match the public key in the node config file",
)
parser.add_argument(
    "--protocol-parameters",
    required=False,
    help="Path to the protocols parameters JSON",
)
parser.add_argument(
    "--baker-secret-key",
    required=False,
    help="Baker secret key, should match the bootstrap account public key in the protocol parameters file",
)
parser.add_argument(
    "--base-protocol",
    required=True,
    help=f"""Initial chain protocol. Could be either full protocol hash or one of the following protocol aliases:
    {", ".join(known_protocols.keys())}
    """,
)
parser.add_argument(
    "--new-protocol",
    required=False,
    help="Protocol that will participate in voting. If not specified, it will be the same as 'base-protocol'",
)

parser.add_argument(
    "--continue-after-activation",
    required=False,
    help="Specify the number of blocks to run after activation. Defaults to 0",
)

parser.add_argument(
    "--vote-exploration",
    required=False,
    help='What the voter will vote for the proposal in the exploration period, "yay" or "nay"; default no vote',
)

parser.add_argument(
    "--vote-promotion",
    required=False,
    help='What the voter will vote for the proposal in the promotion period, "yay" or "nay"; default no vote',
)

parser.add_argument(
    "--rpc-port",
    required=True,
    help="Node will bind RPC to 127.0.0.1:<port>",
)

parsed_args = parser.parse_args()

tmp_root = tempfile.mkdtemp()
node_data_dir = f"{tmp_root}/node-dir-voting"
node_net_port = random.randrange(16384, 65535)
client_data_dir = f"{tmp_root}/client-dir-voting"
default_node_config = {
    "p2p": {
        # We want to allow identities with small difficulties
        # since such identities generate much faster
        "expected-proof-of-work": 1
    },
    "network": {
        "genesis": {
            "timestamp": "2021-05-21T15:00:00Z",
            "block": "BMFCHw1mv3A71KpTuGD3MoFnkHk9wvTYjUzuR9QqiUumKGFG6pM",
            "protocol": "ProtoGenesisGenesisGenesisGenesisGenesisGenesk612im",
        },
        "genesis_parameters": {
            "values": {
                "genesis_pubkey": "edpkvP4vq1PjEmfgfsiWpnQmojx4GYhW5hPHPfomWtmjdUULxRDjRt"
            }
        },
        "chain_name": "TEZOS_LOCALNET_2021-05-21T15:00:00Z",
        "old_chain_name": "TEZOS_LOCALNET_2021-05-21T15:00:00Z",
        "incompatible_chain_name": "INCOMPATIBLE",
        "sandboxed_chain_name": "SANDBOXED_TEZOS",
        "default_bootstrap_peers": [],
    },
}

default_genesis_secret_key = (
    "unencrypted:edsk3efmvuZ9dbhjRCEvfH47Ad3LmrZZgCadfYT6wmTgnN2E6XaEYh"
)

default_oxford_protocol_parameters = {
    "preserved_cycles": 3,
    "blocks_per_cycle": 8,
    "blocks_per_commitment": 4,
    "nonce_revelation_threshold": 4,
    "blocks_per_stake_snapshot": 8,
    "cycles_per_voting_period": 1,
    "hard_gas_limit_per_operation": "1040000",
    "hard_gas_limit_per_block": "2600000",
    "proof_of_work_threshold": "-1",
    "minimal_stake": "6000000000",
    "minimal_frozen_stake": "600000000",
    "vdf_difficulty": "10000000000",
    "origination_size": 257,
    "issuance_weights": {
        "base_total_issued_per_minute": "85007812",
        "baking_reward_fixed_portion_weight": 5120,
        "baking_reward_bonus_weight": 5120,
        "attesting_reward_weight": 10240,
        "liquidity_baking_subsidy_weight": 1280,
        "seed_nonce_revelation_tip_weight": 1,
        "vdf_revelation_tip_weight": 1,
    },
    "cost_per_byte": "250",
    "hard_storage_limit_per_operation": "60000",
    "quorum_min": 2000,
    "quorum_max": 7000,
    "min_proposal_quorum": 500,
    "liquidity_baking_toggle_ema_threshold": 1000000000,
    "max_operations_time_to_live": 240,
    "minimal_block_delay": "1",
    "delay_increment_per_round": "5",
    "consensus_committee_size": 7000,
    "consensus_threshold": 4667,
    "minimal_participation_ratio": {"numerator": 2, "denominator": 3},
    "limit_of_delegation_over_baking": 9,
    "percentage_of_frozen_deposits_slashed_per_double_baking": 5,
    "percentage_of_frozen_deposits_slashed_per_double_attestation": 50,
    "cache_script_size": 100000000,
    "cache_stake_distribution_cycles": 8,
    "cache_sampler_state_cycles": 8,
    "dal_parametric": {
        "feature_enable": False,
        "number_of_slots": 256,
        "attestation_lag": 4,
        "attestation_threshold": 50,
        "blocks_per_epoch": 8,
        "redundancy_factor": 16,
        "page_size": 4096,
        "slot_size": 1048576,
        "number_of_shards": 2048,
    },
    "smart_rollup_arith_pvm_enable": False,
    "smart_rollup_origination_size": 6314,
    "smart_rollup_challenge_window_in_blocks": 40,
    "smart_rollup_stake_amount": "10000000000",
    "smart_rollup_commitment_period_in_blocks": 20,
    "smart_rollup_max_lookahead_in_blocks": 30000,
    "smart_rollup_max_active_outbox_levels": 20160,
    "smart_rollup_max_outbox_messages_per_level": 100,
    "smart_rollup_number_of_sections_in_dissection": 32,
    "smart_rollup_timeout_period_in_blocks": 500,
    "smart_rollup_max_number_of_cemented_commitments": 5,
    "smart_rollup_max_number_of_parallel_games": 32,
    "smart_rollup_reveal_activation_level": {
        "raw_data": {"Blake2B": 0},
        "metadata": 0,
        "dal_page": 2147483646,
        "dal_parameters": 2147483646,
    },
    "smart_rollup_private_enable": True,
    "smart_rollup_riscv_pvm_enable": False,
    "zk_rollup_enable": False,
    "zk_rollup_origination_size": 4000,
    "zk_rollup_min_pending_to_process": 10,
    "zk_rollup_max_ticket_payload_size": 2048,
    "global_limit_of_staking_over_baking": 5,
    "edge_of_staking_over_delegation": 2,
    "adaptive_issuance_launch_ema_threshold": 100000000,
    "adaptive_rewards_params": {
        "issuance_ratio_min": {"numerator": "1", "denominator": "2000"},
        "issuance_ratio_max": {"numerator": "1", "denominator": "20"},
        "max_bonus": "50000000000000",
        "growth_rate": {"numerator": "1", "denominator": "100"},
        "center_dz": {"numerator": "1", "denominator": "2"},
        "radius_dz": {"numerator": "1", "denominator": "50"},
    },
    "adaptive_issuance_activation_vote_enable": False,
    "autostaking_enable": True,
    "bootstrap_accounts": [
        ["edpkubXzL1rs3dQAGEdTyevfxLw3pBCTF53CdWKdJJYiBFwC1xZSct", "40000000000000"]
    ],
}

default_paris_protocol_parameters = {
  "consensus_rights_delay": 2,
  "blocks_preservation_cycles": 1,
  "delegate_parameters_activation_delay": 5,
  "blocks_per_cycle": 8,
  "blocks_per_commitment": 4,
  "nonce_revelation_threshold": 4,
  "cycles_per_voting_period": 1,
  "hard_gas_limit_per_operation": "1040000",
  "hard_gas_limit_per_block": "1733333",
  "proof_of_work_threshold": "-1",
  "minimal_stake": "6000000000",
  "minimal_frozen_stake": "600000000",
  "vdf_difficulty": "10000000000",
  "origination_size": 257,
  "issuance_weights": {
    "base_total_issued_per_minute": "80007812",
    "baking_reward_fixed_portion_weight": 5120,
    "baking_reward_bonus_weight": 5120,
    "attesting_reward_weight": 10240,
    "seed_nonce_revelation_tip_weight": 1,
    "vdf_revelation_tip_weight": 1
  },
  "cost_per_byte": "250",
  "hard_storage_limit_per_operation": "60000",
  "quorum_min": 2000,
  "quorum_max": 7000,
  "min_proposal_quorum": 500,
  "liquidity_baking_subsidy": "5000000",
  "liquidity_baking_toggle_ema_threshold": 1000000000,
  "max_operations_time_to_live": 360,
  "minimal_block_delay": "1",
  "delay_increment_per_round": "5",
  "consensus_committee_size": 7000,
  "consensus_threshold": 4667,
  "minimal_participation_ratio": {
    "numerator": 2,
    "denominator": 3
  },
  "limit_of_delegation_over_baking": 9,
  "percentage_of_frozen_deposits_slashed_per_double_baking": 500,
  "percentage_of_frozen_deposits_slashed_per_double_attestation": 5000,
  "max_slashing_per_block": 10000,
  "max_slashing_threshold": 2334,
  "cache_script_size": 100000000,
  "cache_stake_distribution_cycles": 8,
  "cache_sampler_state_cycles": 8,
  "dal_parametric": {
    "feature_enable": False,
    "incentives_enable": False,
    "number_of_slots": 32,
    "attestation_lag": 8,
    "attestation_threshold": 66,
    "redundancy_factor": 8,
    "page_size": 3967,
    "slot_size": 126944,
    "number_of_shards": 512,
    # "blocks_per_epoch": 8
  },
  "smart_rollup_arith_pvm_enable": False,
  "smart_rollup_origination_size": 6314,
  "smart_rollup_challenge_window_in_blocks": 241920,
  "smart_rollup_stake_amount": "10000000000",
  "smart_rollup_commitment_period_in_blocks": 180,
  "smart_rollup_max_lookahead_in_blocks": 518400,
  "smart_rollup_max_active_outbox_levels": 241920,
  "smart_rollup_max_outbox_messages_per_level": 100,
  "smart_rollup_number_of_sections_in_dissection": 32,
  "smart_rollup_timeout_period_in_blocks": 120960,
  "smart_rollup_max_number_of_cemented_commitments": 5,
  "smart_rollup_max_number_of_parallel_games": 32,
  "smart_rollup_reveal_activation_level": {
    "raw_data": {
      "Blake2B": 0
    },
    "metadata": 0,
    "dal_page": 8193,
    "dal_parameters": 8193,
    "dal_attested_slots_validity_lag": 241920
  },
  "smart_rollup_private_enable": True,
  "smart_rollup_riscv_pvm_enable": False,
  "zk_rollup_enable": False,
  "zk_rollup_origination_size": 4000,
  "zk_rollup_min_pending_to_process": 10,
  "zk_rollup_max_ticket_payload_size": 2048,
  "global_limit_of_staking_over_baking": 5,
  "edge_of_staking_over_delegation": 2,
  "adaptive_issuance_launch_ema_threshold": 0,
  "adaptive_rewards_params": {
    "issuance_ratio_final_min": {
      "numerator": "1",
      "denominator": "400"
    },
    "issuance_ratio_final_max": {
      "numerator": "1",
      "denominator": "10"
    },
    "issuance_ratio_initial_min": {
      "numerator": "9",
      "denominator": "200"
    },
    "issuance_ratio_initial_max": {
      "numerator": "11",
      "denominator": "200"
    },
    "initial_period": 10,
    "transition_period": 50,
    "max_bonus": "50000000000000",
    "growth_rate": {
      "numerator": "1",
      "denominator": "25"
    },
    "center_dz": {
      "numerator": "1",
      "denominator": "2"
    },
    "radius_dz": {
      "numerator": "1",
      "denominator": "50"
    }
  },
  "adaptive_issuance_activation_vote_enable": True,
  "autostaking_enable": True,
  "adaptive_issuance_force_activation": False,
  "ns_enable": True,
  "direct_ticket_spending_enable": False,
#   "blocks_per_stake_snapshot": 8,
  "bootstrap_accounts": [
    [
      "edpkubXzL1rs3dQAGEdTyevfxLw3pBCTF53CdWKdJJYiBFwC1xZSct",
      "40000000000000"
    ]
  ]
}

default_baker_secret_key = (
    "unencrypted:edsk47wWUSCHRDC1Hxtg7yxXzocwDjBJJRqTKnoP7htAifSpzwkg8K"
)


@dataclass
class TestingScenarioEnv:
    node_data_dir: str
    client_base_dir: str
    node_rpc_addr: str
    node_config: dict
    parameters: dict
    base_protocol: str
    new_protocol: str
    genesis_key: str
    baker_key: str
    continue_after_activation: int
    voter_vote: dict


class TestingScenario:
    def __init__(self, env: TestingScenarioEnv):
        self.env = env

    """
    Call 'octez-client' with the given 'call_args', if 'in_background' is True runs 'octez-client'
    in background and bakes blocks until the invocation finishes.
    """

    def __call_tezos_client(self, call_args, in_background=False):
        with open("client.log", "w") as log:
            proc = subprocess.Popen(
                [
                    "octez-client",
                    "--base-dir",
                    self.env.client_base_dir,
                    "--endpoint",
                    f"http://{self.env.node_rpc_addr}",
                ]
                + call_args,
                stdout=log,
                stderr=log,
            )
            if not in_background:
                return_code = proc.wait()
                if return_code != 0:
                    print(
                        "'octez-client' call failed, check 'client.log' to check what went wrong"
                    )
                    sys.exit(return_code)
            else:
                while proc.poll() is None:
                    self.__bake_block()
                return_code = proc.poll()
                if return_code != 0:
                    print(
                        "'octez-client' call failed, check 'client.log' to check what went wrong"
                    )
                    sys.exit(return_code)
        with open("client.log", "r") as log:
            return log.read()

    """
    Imports given 'secret_key' and associates it with the given 'alias'.
    """

    def __import_key(self, alias, secret_key):
        self.__call_tezos_client(
            [
                "import",
                "secret",
                "key",
                alias,
                secret_key,
            ]
        )

    """
    Bakes single block.
    """

    def __bake_block(self):
        minimal_delay = int(
            self.__call_rpc("/chains/main/blocks/head/context/constants")[
                "minimal_block_delay"
            ]
        )
        # Wait a bit more than block delay to avoid rounding issues
        time.sleep(minimal_delay + 1)
        self.__call_tezos_client(["bake", "for", "baker", "--minimal-timestamp"])
        # Node needs some time to handle newly baked block, so we're waiting
        # some short period of time to be sure that all subsequent RPC requests
        # will see a new block.
        time.sleep(0.1)

    """
    Calls given 'rpc_endpoint' and returns JSON result.
    """

    def __call_rpc(self, rpc_endpoint):
        return json.load(
            urllib.request.urlopen(f"http://{self.env.node_rpc_addr}" + rpc_endpoint)
        )

    """
    Starts node using the config from the 'env'.
    """

    def __start_node(self):
        data_dir = self.env.node_data_dir
        os.makedirs(data_dir)
        with open(f"{data_dir}/config.json", "w") as f:
            json.dump(self.env.node_config, f)
        with open("node.log", "w") as log:
            log.flush()
            print("Generating node identity and starting the node")
            subprocess.run(
                ["octez-node", "identity", "generate", "--data-dir", data_dir, "2"],
                stdout=log,
                stderr=log,
            )
            node_proc = subprocess.Popen(
                [
                    "octez-node",
                    "run",
                    "--data-dir",
                    data_dir,
                    "--rpc-addr",
                    self.env.node_rpc_addr,
                    "--synchronisation-threshold",
                    "0",
                    "--no-bootstrap-peers",
                    # Here we avoid using default octez-node port
                    f"--net-addr=:{node_net_port}",
                ],
                stdout=log,
                stderr=log,
            )
            return node_proc

    """
    Activates protocol using parameters from the 'env'.
    """

    def __activate_protocol(self):
        base_dir = self.env.client_base_dir
        os.makedirs(base_dir)
        parameters = f"{base_dir}/parameters.json"
        with open(parameters, "w") as f:
            json.dump(self.env.parameters, f)
        for _ in range(0, 120):  # wait at most 2 minutes for the node to start
            try:
                print("Waiting for node to start...")
                self.__call_rpc("/version")
                break
            except urllib.error.URLError:
                time.sleep(1)
        else:
            raise Exception("Node didn't start in time")
        self.__import_key("genesis", self.env.genesis_key)
        self.__import_key("baker", self.env.baker_key)

        self.__call_tezos_client(
            [
                "--block",
                "genesis",
                "activate",
                "protocol",
                self.env.base_protocol,
                "with",
                "fitness",
                "0",
                "and",
                "key",
                "genesis",
                "and",
                "parameters",
                parameters,
            ]
        )
        self.__bake_block()

    """
    Goes through single amendment period at a time.
    Votes as baker and voter in 'exploration' and 'promotion' periods.
    Bakes blocks until the beginning of the new amendment period.
    """

    def __handle_current_voting_period(self):
        voting_period_info = self.__call_rpc(
            "/chains/main/blocks/head/votes/current_period"
        )
        voting_period_kind = voting_period_info["voting_period"]["kind"]
        # Make sure that the proposal in the latest final head (HEAD~2)
        for _ in range(2):
            self.__bake_block()
        print(f"Current voting period is {voting_period_kind}")
        if voting_period_kind == "proposal":
            pass
        elif voting_period_kind == "exploration" or voting_period_kind == "promotion":
            proposal = self.__call_rpc(
                "/chains/main/blocks/head/votes/current_proposal"
            )
            self.__call_tezos_client(
                ["submit", "ballot", "for", "baker", proposal, "yay"]
            )
            vote = self.env.voter_vote[voting_period_kind]
            if vote is not None:
                print(f"Voting as voter for {proposal}: {vote}")
                self.__call_tezos_client(
                    ["submit", "ballot", "for", "voting_scenario_voter", proposal, vote]
                )
        # Bake blocks till the end of the current voting period
        for _ in range(voting_period_info["remaining"] + 1):
            self.__bake_block()
        return voting_period_kind

    """
    Runs the testing scenario.
    """

    def run_scenario(self):
        proc = self.__start_node()
        try:
            self.__activate_protocol()
            # Make sure that the latest final head has 'base-protocol'
            for _ in range(3):
                self.__bake_block()
            user_node_config = dict(self.env.node_config)
            # Nice dict merging interface is present starting from Python 3.9 :(
            user_node_config["p2p"] = {
                **user_node_config.get("p2p", {}),
                **{"bootstrap-peers": ["127.0.0.1:9735"]},
            }
            user_node_config = {
                **user_node_config,
                **{
                    "shell": {
                        "chain_validator": {
                            "synchronisation_threshold": 1,
                            # We're baking blocks with timestamps in the past, so we're increasing the latency
                            # to increase chances that node will consider itself synced with the others
                            "latency": 1200,
                        }
                    }
                },
            }
            config_path = os.path.join(
                os.path.abspath(os.getcwd()), "voting-config.json"
            )
            with open(config_path, "w") as f:
                json.dump(user_node_config, f, indent=2)
            print(f"You can start local octez-node using the config from {config_path}")
            self.__call_tezos_client(
                ["gen", "keys", "voting_scenario_voter", "--force"]
            )
            addrs = self.__call_tezos_client(["list", "known", "addresses"])
            voter_addr = re.search("\\bvoting_scenario_voter: ([^ ]+)", addrs).group(1)
            print(f"Transferring some money to {voter_addr}")
            self.__call_tezos_client(
                [
                    "transfer",
                    "80000",
                    "from",
                    "baker",
                    "to",
                    voter_addr,
                    "--burn-cap",
                    "10",
                ],
                in_background=True,
            )
            print(f"Registering {voter_addr} as delegate...")
            self.__call_tezos_client(
                ["register", "key", "voting_scenario_voter", "as", "delegate"],
                in_background=True,
            )
            # wait for at most 30 blocks for registration, which is about 2.5 minutes
            for _ in range(0, 30):
                try:
                    self.__call_rpc(
                        f"/chains/main/blocks/head/context/contracts/{voter_addr}/delegate"
                    )
                    break
                except urllib.error.URLError:
                    self.__bake_block()
            else:
                raise Exception("Did not register as delegate in time")
            print(
                f"Waiting till the end of the current cycle for {voter_addr} to obtain voting rights"
            )

            current_cycle = self.__call_rpc(
                "/chains/main/blocks/head/helpers/current_level"
            )["cycle"]
            while (
                self.__call_rpc("/chains/main/blocks/head/helpers/current_level")[
                    "cycle"
                ]
                == current_cycle
            ):
                self.__bake_block()
            print(f"Submitting a new proposal: '{self.env.new_protocol}'")
            self.__call_tezos_client(
                ["submit", "proposals", "for", "baker", self.env.new_protocol]
            )
            # Make sure that the proposal in the latest final head (HEAD~2)
            for _ in range(2):
                self.__bake_block()
            print("Going through the voting cycle")
            for _ in range(5):
                self.__handle_current_voting_period()
            print("Voting cycle is over")
            if self.env.continue_after_activation is not None:
                print(
                    f"Chain is still running for {self.env.continue_after_activation} blocks, terminate script to stop it"
                )
                for _ in range(0, self.env.continue_after_activation):
                    self.__bake_block()
        except Exception as _:
            proc.terminate()
        finally:
            proc.terminate()


def get_node_config(args):
    if args.node_config is None:
        return default_node_config
    else:
        with open(args.node_config, "r") as f:
            return json.load(f.read())


def get_genesis_key(args):
    if args.genesis_secret_key is None:
        return default_genesis_secret_key
    else:
        return args.genesis_secret_key


def get_parameters(args):
    if args.protocol_parameters is None:
        base_protocol = extract_protocol_hash(parsed_args.base_protocol)
        match base_protocol:
            case "ProxfordYmVfjWnRcgjWH36fW6PArwqykTFzotUxRs6gmTcZDuH":
                return default_oxford_protocol_parameters
            case "PtParisBxoLz5gzMmn3d9WBQNoPSZakgnkMC2VNuQ3KXfUtUQeZ":
                return default_paris_protocol_parameters
            case _:
                raise f"No default protocol parameters for {base_protocol}"
    else:
        with open(args.protocol_parameters, "r") as f:
            return json.load(f.read())


def get_baker_key(args):
    if args.baker_secret_key is None:
        return default_baker_secret_key
    else:
        return args.baker_secret_key


def get_new_protocol(args):
    if args.new_protocol is None:
        return args.base_protocol
    else:
        return args.new_protocol


def get_continue_after_activation(args):
    if args.continue_after_activation is None:
        return None
    else:
        return int(parsed_args.continue_after_activation)


if __name__ == "__main__":
    scenario = TestingScenario(
        env=TestingScenarioEnv(
            node_data_dir=node_data_dir,
            client_base_dir=client_data_dir,
            node_rpc_addr=f"127.0.0.1:{parsed_args.rpc_port}",
            node_config=get_node_config(parsed_args),
            parameters=get_parameters(parsed_args),
            base_protocol=extract_protocol_hash(parsed_args.base_protocol),
            new_protocol=extract_protocol_hash(get_new_protocol(parsed_args)),
            genesis_key=get_genesis_key(parsed_args),
            baker_key=get_baker_key(parsed_args),
            continue_after_activation=get_continue_after_activation(parsed_args),
            voter_vote={
                "exploration": parsed_args.vote_exploration,
                "promotion": parsed_args.vote_promotion,
            },
        )
    )
    try:
        scenario.run_scenario()
    finally:
        shutil.rmtree(tmp_root)
