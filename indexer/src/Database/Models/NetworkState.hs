-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.NetworkState
  ( NetworkState
  , NetworkStateWrite
  , NetworkStatePoly(..)
  , NetworkStateId
  , networkStateTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Node qualified
import System.Log.FastLogger

type NetworkStateId = DbId "networkState"

data NetworkStatePoly id periodIdx periodKind epoch quorum sourceBlockId = NetworkState
  { id            :: id
  , periodIdx     :: periodIdx
  , periodKind    :: periodKind
  , epoch         :: epoch
  , quorum        :: quorum
  , sourceBlockId :: sourceBlockId
  } deriving stock Show

instance
  ( ToLogStr periodIdx, ToLogStr epoch
  , ToLogStr periodKind, ToLogStr sourceBlockId) =>
    ToLogStr
      (NetworkStatePoly id periodIdx periodKind
        epoch quorum sourceBlockId) where
    toLogStr (NetworkState _ periodIdx periodKind  epoch _ _)
      = "NetworkState: (Period idx " <> toLogStr periodIdx <>
          " of type: " <> toLogStr periodKind <> " at epoch " <>
            toLogStr epoch <> ")"

type NetworkState'' rm = NetworkStatePoly
  (ComputeFieldTypeAuto rm NetworkStateId)
  (ComputeFieldType rm Node.PeriodIdx)
  (ComputeFieldType rm Node.VotingPeriodKind)
  (ComputeFieldType rm Node.EpochIdx)
  (ComputeFieldType rm (Maybe Node.Quorum))
  (ComputeFieldType rm Blocks.BlockId)

$(makeAdaptorAndInstance "pDbNetworkState" ''NetworkStatePoly)

type NetworkState = NetworkState'' 'HaskellRead
type NetworkStateWrite = NetworkState'' 'HaskellWrite

networkStateTable
  :: Table (NetworkState'' 'DbWrite) (NetworkState'' 'DbRead)
networkStateTable =
  table "network_state" (pDbNetworkState
    (NetworkState
      (tableField "id")
      (tableField "period_idx")
      (tableField "period_kind")
      (tableField "epoch")
      (tableField "min_quorum")
      (tableField "source_block_id")))
