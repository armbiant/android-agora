-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.PeriodStatus
  ( PeriodStatusPoly(..)
  , PeriodStatusId
  , PeriodStatus
  , periodStatusTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Node qualified
import System.Log.FastLogger

type PeriodStatusId = DbId "periodStatus"

data PeriodStatusPoly id periodIdx status sourceBlockId = PeriodStatus
  { id         :: id
  , periodIdx   :: periodIdx
  , status :: status
  , sourceBlockId :: sourceBlockId
  } deriving stock Show

instance (Show id, Show periodIdx, Show status, Show sourceBlockId) =>
  ToLogStr (PeriodStatusPoly id periodIdx status sourceBlockId) where
  toLogStr = toLogStr . show

$(makeAdaptorAndInstance "pDbPeriodStatus" ''PeriodStatusPoly)

type PeriodStatus'' rm = PeriodStatusPoly
  (ComputeFieldTypeAuto rm PeriodStatusId)
  (ComputeFieldType rm Node.PeriodIdx)
  (ComputeFieldType rm Node.PeriodStatus)
  (ComputeFieldType rm Blocks.BlockId)

type PeriodStatus = PeriodStatus'' 'HaskellRead

periodStatusTable
  :: Table (PeriodStatus'' 'DbWrite) (PeriodStatus'' 'DbRead)
periodStatusTable =
  table "period_status" (pDbPeriodStatus
    (PeriodStatus
      (tableField "id")
      (tableField "period_idx")
      (tableField "status")
      (tableField "source_block_id")))
