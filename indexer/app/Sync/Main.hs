-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main where

import Sync

-- | The entrypoint of the sync component. This is separated from the
-- api component since they are logically separate concerns. Having
-- this separation also allows to check logs and monitor resources for
-- them separately.
main :: IO ()
main = runSync
