# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ pkgs }: with pkgs;

let
  # Keep in mind that parent or global .gitignore are not respected
  source = ./.;

  project = pkgs.callPackage ./. { };
in
{
  agora-frontend = project;
}
