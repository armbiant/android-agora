-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Proposal
  ( getProposal
  , getProposalOperationForProposal
  , getProposalOperationsForPeriod
  , getProposalOperationsForProposal
  , getProposalPeriodForEpoch
  , getProposalStatus
  , getProposalsForPeriod
  , getProposalsForPeriodWithUpvotes
  , getProposalsWithStatusesFor
  , getRecentProposalWithHash
  , insertProposal
  , insertProposalOperation
  , insertProposalStatus
  ) where

import Data.Text (Text)

import Common
import Database.Models.Blocks
import Database.Models.NetworkState
import Database.Models.PeriodVoters
import Database.Models.ProposalOperations
import Database.Models.Proposals
import qualified Database.Models.Proposals as Proposals
import Database.Models.ProposalStatus
import qualified Database.Models.ProposalStatus as ProposalStatus
import Database.Models.Voters
import DataStore.Common
import qualified Node

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getProposalsForPeriod
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m [Proposals.Proposal]
getProposalsForPeriod periodId = let
  query = do
    pt <- selectTable Proposals.proposalsTable
    where_ (pt.periodIdx .== toFields periodId)
    pure pt
  in withConnection $ \conn -> liftIO (runSelect conn query)

getProposalsWithStatusesFor
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m [(Proposal, Node.ProposalStatus)]
getProposalsWithStatusesFor periodIdx = do
  let
    -- Use DISTINCT ON along with ORDER BY to group rows by proposal id
    -- and sort rows in each group by source block time, resulting in a
    -- set of proposal rows with latest proposal status for each proposal.
    query = fst <$> distinctOnBy (Proposals.id . fst . fst) (desc snd) (do
      proposal <- selectTable Proposals.proposalsTable
      proposalStatus <- selectTable proposalStatusTable
      blocks <- selectTable blocksTable
      where_ (proposal.id .== proposalStatus.proposalId)
      where_ (proposal.periodIdx .== toFields periodIdx)
      where_ (blocks.id .== proposalStatus.sourceBlockId)
      pure ((proposal, proposalStatus.status), blocks.blockTime))
  withConnection $ \conn -> liftIO (runSelect conn query)

getProposalsForPeriodWithUpvotes
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m [(Proposals.ProposalId, Node.VotingPower)]
getProposalsForPeriodWithUpvotes periodId = let
  query = aggregate (p2 (groupBy, sumInt8)) $ do
    pt <- selectTable Proposals.proposalsTable
    po <- selectTable proposalOperationsTable
    periodVoter <- selectTable periodVotersTable
    where_ (pt.periodIdx .== toFields periodId)
    where_ (po.proposalId .== pt.id)
    where_ (periodVoter.id .== po.periodVoterId)
    -- Grouping on proposal.id should be equalent to grouping on
    -- proposal hash, since we are filtering by periodIdx, and the
    -- table have a unique constraint for (proposalHash, periodIdx).
    pure (pt.id, periodVoter.votingPower)
  in fmap (\(a, b) -> (a, Node.VotingPower $ scientificToInt64 b))
      <$> withConnection (\conn -> liftIO (runSelect conn query))

getProposalPeriodForEpoch
  :: (MonadIO m, HasCap DbCap caps)
  => Node.EpochIdx
  -> CapsT caps m Node.PeriodIdx
getProposalPeriodForEpoch epochIdx = let
  query = do
    pt <- selectTable networkStateTable
    where_ (pt.epoch .== toFields epochIdx .&& (pt.periodKind .== toFields Node.Proposal))
    pure pt.periodIdx
  in withConnection $ \conn -> listToOne $ runSelect conn query


getRecentProposalWithHash
  :: (MonadIO m, HasCap DbCap caps)
  => Node.ProposalHash
  -> CapsT caps m (Maybe Proposal)
getRecentProposalWithHash proposalHash = let
  query = fmap fst $ limit 1 $ orderBy (desc (blockTime . snd)) $ do
    pt <- selectTable proposalsTable
    blk <- selectTable blocksTable
    where_ (pt.proposalHash .== toFields proposalHash .&& (pt.sourceBlockId .== blk.id))
    pure (pt, blk)
  in withConnection $ \conn -> liftIO $ listToMaybe <$> runSelect conn query

getProposalOperationsForPeriod
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> CapsT caps m
      [( ProposalOperation, Node.PkHash, UTCTime, Node.Level
       , Node.ProposalHash, Node.VotingPower)
      ]
getProposalOperationsForPeriod periodIdx mOffset mLimit = let
  query = do
    po <- selectTable proposalOperationsTable
    prop <- selectTable proposalsTable
    bl <- selectTable blocksTable
    voter <- selectTable votersTable
    periodVoter <- selectTable periodVotersTable

    where_ (periodVoter.id .== po.periodVoterId)
    where_ (voter.id .== periodVoter.voterId)

    where_ (prop.periodIdx .== toFields periodIdx .&& prop.id .== po.proposalId)
    where_ (bl.id .== po.sourceBlockId)
    pure (po, voter.voterAddress, bl.blockTime, bl.level, prop.proposalHash, periodVoter.votingPower)
  in withConnection $ \conn -> liftIO
      (runSelect conn (applyOffsetLimit mOffset mLimit (desc getProposalOperationId) query))
    where
      getProposalOperationId (a, _, _, _, _, _) = a.id

getProposalOperationsForProposal
  :: (MonadIO m, HasCap DbCap caps)
  => Node.ProposalHash
  -> Maybe Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> CapsT caps m [
      ( ProposalOperation, Node.PkHash, UTCTime
      , Node.Level, Node.ProposalHash, Node.VotingPower)
      ]
getProposalOperationsForProposal pHash mPeriodIdx mOffset mLimit = let
  query = do
    po <- selectTable proposalOperationsTable
    prop <- selectTable proposalsTable
    bl <- selectTable blocksTable
    voter <- selectTable votersTable
    periodVoter <- selectTable periodVotersTable

    where_ (periodVoter.id .== po.periodVoterId)
    where_ (voter.id .== periodVoter.voterId)

    case mPeriodIdx of
      Just periodIdx -> where_ (po.periodIdx .== toFields periodIdx)
      Nothing -> pass

    where_ (prop.proposalHash .== toFields pHash .&& prop.id .== po.proposalId)
    where_ (bl.id .== po.sourceBlockId)
    pure (po, voter.voterAddress, bl.blockTime, bl.level, prop.proposalHash, periodVoter.votingPower)
  in withConnection $ \conn -> liftIO
    (runSelect conn (applyOffsetLimit mOffset mLimit (desc getProposalOperationId) query))
    where
      getProposalOperationId (a, _, _, _, _, _) = a.id

getProposalOperationForProposal
  :: (MonadIO m, HasCap DbCap caps)
  => Proposal
  -> CapsT caps m (ProposalOperation, Voter)
getProposalOperationForProposal proposal = let
  query = limit 1 $ do
    po <- selectTable proposalOperationsTable
    pv <- selectTable periodVotersTable
    voter <- selectTable votersTable
    where_ (po.proposalId .== toFields (proposal.id) .&& pv.id .== po.periodVoterId .&& voter.id .== pv.voterId)
    pure (po, voter)
  in withConnection $ \conn -> listToOne $ runSelect conn query

getProposalStatus
  :: (MonadIO m, HasCap DbCap caps)
  => Proposals.ProposalId
  -> CapsT caps m Node.ProposalStatus
getProposalStatus proposalId = let
  query = fst <$> limit 1 (orderBy (desc snd) $ do
    ps <- selectTable ProposalStatus.proposalStatusTable
    blocks <- selectTable blocksTable
    where_ (ps.proposalId .== toFields proposalId)
    where_ (blocks.id .== ps.sourceBlockId)
    pure (ps.status, blocks.blockTime))
  in withConnection $ \conn -> listToOne $ runSelect conn query

getProposal
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> Node.ProposalHash
  -> CapsT caps m (Maybe Proposals.Proposal)
getProposal periodId proposalHash = let
  query = do
    pt <- selectTable Proposals.proposalsTable
    where_ (pt.periodIdx .== toFields periodId .&& pt.proposalHash .== toFields proposalHash)
    pure pt
  in withConnection $ \conn -> liftIO $ listToMaybe <$> runSelect conn query

insertProposal
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> Node.PeriodIdx
  -> Node.ProposalHash
  -> CapsT caps m Proposals.Proposal
insertProposal sourceBlockId periodId proposalHash = withConnection $ \conn -> listToOne $
  runInsert conn $
    Insert Proposals.proposalsTable
      [toFields (Proposals.Proposal (Nothing :: Maybe Int)
        proposalHash periodId sourceBlockId)] (rReturning Prelude.id) Nothing

insertProposalStatus
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> Node.ProposalStatus
  -> Proposals.ProposalId
  -> CapsT caps m ProposalStatusId
insertProposalStatus sourceBlockId status proposalId
  = withConnection $ \conn -> listToOne $ runInsert conn $ do
      Insert proposalStatusTable
        [toFields (ProposalStatus (Nothing :: Maybe Int)
          proposalId status sourceBlockId)
        ] (rReturning ProposalStatus.id) Nothing

insertProposalOperation
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> Node.PeriodIdx
  -> Proposals.ProposalId
  -> Node.OperationHash
  -> PeriodVoter
  -> CapsT caps m ProposalOperation
insertProposalOperation sourceBlockId periodIdx proposalId operationHash source
  = withConnection $ \conn -> listToOne $
      runInsert conn $
        Insert proposalOperationsTable
          [toFields (ProposalOperation
            (Nothing :: Maybe Int) proposalId periodIdx
              operationHash source.id sourceBlockId)
          ] (rReturning Prelude.id) Nothing
