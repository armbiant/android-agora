# Revision history for agora-indexer

## Unreleased

## 0.1.1.1

* Cache head block fetch request.

## 0.1.1.0

* Fix minor bug during block reverting which caused more blocks to be reverted than necessary.

## 0.1.0.10

* Fix show stopper bug in block caching.
* Make node access parameters configurable.
* Add node access statistics.

## 0.1.0.9

* Cache block data so that re-indexing can be done quickly.

## 0.1.0.8

* Fix voting power computation from rolls for legacy periods.

## 0.1.0.7

* Fix issue where there is a bug in the api component in the computing of won
  proposal in an epoch, when the epoch is still going on.

## 0.1.0.0

* First version. Released on an unsuspecting world.
