# SPDX-FileCopyrightText: 2023 Tezos Commons
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from ubuntu:22.04
ENV DEBIAN_FRONTEND=noninteractive
RUN adduser --disabled-password --disabled-login tzidx
ENV LANG=en_US.UTF-8
RUN chmod 777 /tmp
RUN apt-get update
RUN apt-get install -y locales && \
    sed -i -e "s/# $LANG.*/$LANG UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=$LANG
RUN bash -c 'apt-get install -y lsb-release gnupg wget zlib1g-dev libgmp-dev'
RUN \
    apt-get update -y && \
    apt-get install -y --no-install-recommends \
        curl \
        libnuma-dev \
        zlib1g-dev \
        libgmp-dev \
        libgmp10 \
        git \
        wget \
        lsb-release \
        software-properties-common \
        gnupg2 \
        apt-transport-https \
        gcc \
        autoconf \
        automake \
        pkg-config \
        build-essential


RUN bash -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN bash -c 'wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -'
RUN apt-get update
RUN bash -c "apt-get -y install 'postgresql-14' libpq-dev"
RUN bash -c "chmod 777 /var/run/postgresql"
RUN bash -c "apt-get install -y hpack"

# Init db directory
USER tzidx
RUN bash -c "mkdir /home/tzidx/PGDATA"
ENV PGDATA=/home/tzidx/PGDATA
ENV PATH=${PATH}:/usr/lib/postgresql/14/bin
RUN bash -c "initdb"

RUN bash -c "mkdir -p /home/tzidx/.local/bin"
RUN bash -c "curl -o /home/tzidx/.local/bin/ghcup https://downloads.haskell.org/~ghcup/x86_64-linux-ghcup"
RUN bash -c "chmod +x /home/tzidx/.local/bin/ghcup"

# Add ghcup to PATH and setup Haskell build infra
ENV PATH=${PATH}:/home/tzidx/.local/bin
ENV PATH=${PATH}:/home/tzidx/.ghcup/bin
ENV PATH=${PATH}:/home/tzidx/.cabal/bin

RUN bash -c "ghcup upgrade"
RUN bash -c "ghcup install cabal 3.6.2.0"
RUN bash -c "ghcup set cabal 3.6.2.0"

# Install GHC
RUN bash -c "ghcup install ghc 9.2.5"
RUN bash -c "ghcup set ghc 9.2.5"

RUN bash -c "cabal update"
ARG repobranch=master
WORKDIR /home/tzidx
RUN bash -c "git clone https://gitlab.com/tezosagora/agora.git -b $repobranch"

# Setup octez bins and fetch zcash params for node.
RUN bash -c "mkdir /home/tzidx/octez-bin"
WORKDIR /home/tzidx/octez-bin
RUN bash -c 'wget https://github.com/serokell/tezos-packaging/releases/download/v19.0-1/octez-node'
RUN bash -c 'wget https://github.com/serokell/tezos-packaging/releases/download/v19.0-1/octez-client'
RUN bash -c 'wget https://raw.githubusercontent.com/zcash/zcash/713fc761dd9cf4c9087c37b078bdeab98697bad2/zcutil/fetch-params.sh'
ENV PATH=${PATH}:/home/tzidx/octez-bin
RUN bash -c 'chmod +x *'
RUN bash -c './fetch-params.sh'

# build tests
WORKDIR /home/tzidx/agora/indexer
RUN bash -c "hpack"
RUN bash -c "cabal build --enable-tests"

# Copy the test runner script.
COPY run-test.sh run-test.sh
USER root
RUN bash -c "chmod +x run-test.sh"

USER tzidx
WORKDIR /home/tzidx/agora/indexer
ENV PATH=${PATH}:/usr/lib/postgresql/14/bin
ENV PGDATA=/home/tzidx/PGDATA
CMD ["./run-test.sh"]
