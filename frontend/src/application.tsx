// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { ReactElement, Suspense, useEffect, useState } from "react";
import "~/styles/main.scss";
import { withRedux } from "~/store";
import { Router, View } from "react-navi";
import agoraRouter from "~/router/agoraRouter";
import { ThemeContext } from "~/components/common/ThemeContext";

function Application(): ReactElement {
  let initTheme = "light";

  let savedPref = null;
  // If there is some saved preference, then use it to set initial theme.
  try {
    let s = localStorage.getItem("color-scheme");
    if (s) {
      savedPref = JSON.parse(s);
    }
    // If there is nothing in the localstorage key, we get a null from JSON.parse here.
  } catch {
    console.log(
      "Saved theme preference from localstorage could not be parsed!"
    );
  }

  if (savedPref) {
    initTheme = savedPref;
  } else {
    // else try to get browser theme preference.
    if (window.matchMedia) {
      if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
        initTheme = "dark";
      }
    }
  }

  const [theme, setTheme] = useState(initTheme);

  const toggleTheme = (): void => {
    setTheme((cur: string): string => (cur === "light" ? "dark" : "light"));
  };

  useEffect((): void => {
    document.body.setAttribute("data-theme", theme);
    localStorage.setItem("color-scheme", JSON.stringify(theme));
  }, [theme]);

  return (
    <ThemeContext.Provider
      value={{ currentTheme: theme, toggleTheme: toggleTheme }}
    >
      <div className="app">
        <Router routes={agoraRouter()}>
          <Suspense fallback={null}>
            <View />
          </Suspense>
        </Router>
      </div>
    </ThemeContext.Provider>
  );
}

export default withRedux(Application);
