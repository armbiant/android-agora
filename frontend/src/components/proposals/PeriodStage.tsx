// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { Fragment, FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/proposals/PeriodStage.scss";
import { PeriodType, PeriodTimeInfo } from "~/models/Period";
import AngleIcon from "~/assets/svg/AngleIcon";
import { useTranslation } from "react-i18next";
import { DateTime } from "luxon";
import NaviLink from "../common/NaviLink";
import { Constants, Period } from "~/models/Period";

type BoundaryType = "Estimated" | "Known";

interface PeriodBoundary {
  boundaryDate: DateTime;
  boundaryType: BoundaryType;
}

function mkPeriodBoundary(dt: string, pt: BoundaryType): PeriodBoundary {
  return {
    boundaryDate: DateTime.fromISO(dt),
    boundaryType: pt,
  };
}

function clonePeriodBoundary(pd: PeriodBoundary): PeriodBoundary {
  return {
    boundaryDate: pd.boundaryDate.set({}),
    boundaryType: pd.boundaryType,
  };
}

interface PeriodStageIndicatorTypes {
  caption: string;
  isCurrent: boolean;
  link?: string;
  startEndTimes?: [PeriodBoundary, PeriodBoundary];
}

const ProposalStageIndicator: FunctionComponent<PeriodStageIndicatorTypes> = ({
  caption,
  isCurrent,
  link,
  startEndTimes,
}): ReactElement => {
  const { t } = useTranslation();
  const className = cx(styles.proposalStage__indicator, {
    [styles.proposalStage__indicator_selected]: isCurrent,
    [styles.proposalStage__indicator_disabled]: !link && !isCurrent,
  });

  let projectedDate = startEndTimes ? (
    <div
      title={
        startEndTimes[0].boundaryType == "Estimated" ||
        startEndTimes[1].boundaryType == "Estimated"
          ? "* Estimated"
          : ""
      }
      className={cx(styles.proposalStage__projection)}
    >
      {t("periods.boundaryDate", {
        from: {
          date: startEndTimes[0].boundaryDate,
          format: startEndTimes[0].boundaryType == "Known" ? "d MMM" : "d MMM*",
        },
        to: {
          date: startEndTimes[1].boundaryDate,
          format: startEndTimes[1].boundaryType == "Known" ? "d MMM" : "d MMM*",
        },
      })}
    </div>
  ) : (
    <span />
  );

  return link ? (
    <div>
      <NaviLink className={className} href={link}>
        {caption}
      </NaviLink>
      {projectedDate}
    </div>
  ) : (
    <div>
      <div className={className}>{caption}</div>
      {projectedDate}
    </div>
  );
};

interface PeriodStageTypes {
  className?: string;
  stage: PeriodType;
  periodTimes: PeriodTimeInfo;
  hideSelected?: boolean;
  period: Period;
}

const stageAliases: object = {
  proposal: "proposal",
  exploration: "testing_vote",
  testing: "testing",
  promotion: "promotion_vote",
  adoption: "adoption",
};

// Extract start/end times for all periods in this
// cycle.
function findPeriodDurationsInCycle(
  constants: Constants,
  periodTimes: PeriodTimeInfo,
  currentPeriod: number,
  periodCycleOffset: number,
  stages: string[]
): Map<PeriodType, [PeriodBoundary, PeriodBoundary]> {
  const proposalIdx = currentPeriod - periodCycleOffset;
  let result = new Map();
  let timeBetweenBlocks = constants.timeBetweenBlocks
    ? constants.timeBetweenBlocks
    : 30;
  let blocksPerPeriod = constants.blocksPerVoting
    ? constants.blocksPerVoting
    : 40960;

  let lastPeriodEnd: PeriodBoundary | null = null;
  for (var idx = 0; idx < stages.length; idx++) {
    var pname = stages[idx];
    let currentPeriodIdx = proposalIdx + idx;

    // We infer the last period on the network by counting
    // the number of available period time entries.
    let lastPeriodOnNetwork = periodTimes.length - 1;
    // if we are in a past/current period, then start/end times
    // are available in 'periodTimes'.
    if (currentPeriodIdx in periodTimes) {
      let currentPeriodInfo = periodTimes[currentPeriodIdx];
      // check that the current period is indeed the expected type,
      // because period types can be skipped if a proposal is rejected
      // half way. In that case, do not try to fill in the projected
      // times, because those periods never happened, and return the current
      // result immediately, because this voting epoch has ended.
      if (currentPeriodInfo.periodType == stageAliases[pname]) {
        let thisStartTime = mkPeriodBoundary(
          currentPeriodInfo.startTime,
          "Known"
        );
        let thisEndTime =
          // Make the period boundary value, keeping the type to "Estimated" even for the
          // current period which the indexer provides an estimated end date for.
          mkPeriodBoundary(
            currentPeriodInfo.endTime,
            currentPeriodIdx == lastPeriodOnNetwork ? "Estimated" : "Known"
          );
        result.set(pname, [thisStartTime, thisEndTime]);

        // Track the end time of the period so that we can compute
        // time frames of the periods to come, starting from the end time
        // of the last available period.
        lastPeriodEnd = thisEndTime;
      } else return result; //Return result because this voting epoch is over.
    } else {
      if (lastPeriodEnd !== null) {
        let thisStartTime = clonePeriodBoundary(lastPeriodEnd);
        let thisEndTime = {
          boundaryDate: lastPeriodEnd.boundaryDate.plus({
            seconds: timeBetweenBlocks * blocksPerPeriod,
          }),
          boundaryType: "Estimated" as BoundaryType,
        };
        lastPeriodEnd = clonePeriodBoundary(thisEndTime);
        result.set(pname, [thisStartTime, thisEndTime]);
      }
    }
  }
  return result;
}

export const PeriodStage: FunctionComponent<PeriodStageTypes> = ({
  className,
  stage,
  periodTimes,
  hideSelected,
  period,
}): ReactElement => {
  const periodId = period.id;
  const stages = Object.keys(stageAliases);

  const idx = (stage: PeriodType): number =>
    stages.findIndex((s): boolean => s == stage);
  const current = idx(stage);
  const startEndTimes = findPeriodDurationsInCycle(
    period.constants,
    periodTimes,
    periodId,
    current,
    stages
  );

  const getLink = (
    s1: PeriodType,
    s2: string,
    i: number
  ): string | undefined => {
    const id = periodId - current + i;

    if (
      (!hideSelected && stage === s1) ||
      !periodTimes[id] ||
      periodTimes[id].periodType != s2
    )
      return;
    return `/period/${id}`;
  };

  const displayAdoptionPeriod = (): ReactElement => {
    if (period.historicalConstants.periodsPerEpoch == 5)
      return (
        <Fragment>
          <AngleIcon />
          <ProposalStageIndicator
            caption="Adoption"
            link={getLink("adoption", "adoption", 4)}
            isCurrent={!hideSelected && stage === "adoption"}
            startEndTimes={startEndTimes.get("adoption")}
          />
        </Fragment>
      );
    else return <div></div>;
  };

  return (
    <div className={className}>
      <div className={styles.proposalStage}>
        <ProposalStageIndicator
          caption="Proposal"
          link={getLink("proposal", "proposal", 0)}
          isCurrent={!hideSelected && stage === "proposal"}
          startEndTimes={startEndTimes.get("proposal")}
        />
        <AngleIcon />
        <ProposalStageIndicator
          caption="Exploration"
          link={getLink("exploration", "testing_vote", 1)}
          isCurrent={!hideSelected && stage === "exploration"}
          startEndTimes={startEndTimes.get("exploration")}
        />
        <AngleIcon />
        <ProposalStageIndicator
          caption="Cooldown" // Testing period has been renamed to 'cooldown' period with Florence protocol after period 46.
          link={getLink("testing", "testing", 2)}
          isCurrent={!hideSelected && stage === "testing"}
          startEndTimes={startEndTimes.get("testing")}
        />
        <AngleIcon />
        <ProposalStageIndicator
          caption="Promotion"
          link={getLink("promotion", "promotion_vote", 3)}
          isCurrent={!hideSelected && stage === "promotion"}
          startEndTimes={startEndTimes.get("promotion")}
        />
        {displayAdoptionPeriod()}
      </div>
    </div>
  );
};

export const PeriodStageShort: FunctionComponent<PeriodStageTypes> = ({
  className,
  stage,
}): ReactElement => {
  const { t } = useTranslation();

  return (
    <div className={className}>
      <div className={styles.proposalStage}>
        <ProposalStageIndicator
          caption={t(`periodType.${stage}`)}
          isCurrent={true}
        />
      </div>
    </div>
  );
};
