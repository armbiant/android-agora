-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Ballot
  ( BallotsQueryResult(..)
  , getBallotsForProposal
  , getBallotsForProposalWithVotingPower
  , insertBallotOperation
  ) where

import Data.Text (Text)

import Common
import Database.Models.BallotOperations
import qualified Database.Models.BallotOperations as BallotOperations
import Database.Models.Blocks
import Database.Models.NetworkState
import Database.Models.PeriodVoters
import Database.Models.Proposals
import Database.Models.Voters
import DataStore.Common
import qualified Node

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

type BallotsQueryResultRaw
  = ( Ballot, Node.PkHash, Node.VotingPower, UTCTime
    , Node.OperationHash, Node.PeriodIdx, Node.EpochIdx
    , Node.ProposalHash)

data BallotsQueryResult = BallotsQueryResult
  { ballot           :: Ballot
  , voterPkHash      :: Node.PkHash
  , voterVotingPower :: Node.VotingPower
  , timestamp        :: UTCTime
  , operationHash    :: Node.OperationHash
  , periodIdx        :: Node.PeriodIdx
  , epochIdx         :: Node.EpochIdx
  , proposalHash     :: Node.ProposalHash
  }

fromBallotsQuryResultRaw
  :: BallotsQueryResultRaw
  -> BallotsQueryResult
fromBallotsQuryResultRaw
  (bl, voterAddress, votingPower, timestamp, operationHash, periodIdx, epochIdx, proposalHash) =
    BallotsQueryResult bl voterAddress votingPower timestamp
      operationHash periodIdx epochIdx proposalHash

getBallotsQuery
  :: Select ( BallotDbRead, Field SqlText, Field SqlInt8
            , Field SqlTimestamptz, Field SqlText, Field SqlInt4
            , Field SqlInt4, Field SqlText)
getBallotsQuery = do
    bl <- selectTable ballotsTable
    ns <- selectTable networkStateTable
    v <- selectTable votersTable
    prp <- selectTable proposalsTable
    pv <- selectTable periodVotersTable
    blk <- selectTable blocksTable
    where_ (pv.id .== bl.periodVoterId)
    where_ (v.id .== pv.voterId)
    where_ (ns.periodIdx .== bl.periodIdx)
    where_ (blk.id .== bl.sourceBlockId)
    where_ (prp.id .== bl.proposalId)
    pure ( bl, v.voterAddress, pv.votingPower
         , blk.blockTime, bl.operationHash, bl.periodIdx
         , ns.epoch, prp.proposalHash)

getBallotsForProposal
  :: (MonadIO m, HasCap DbCap caps)
  => Maybe Node.PeriodIdx
  -> Maybe Node.ProposalHash
  -> Maybe Int
  -> Maybe Int
  -> CapsT caps m [BallotsQueryResult]
getBallotsForProposal mPeriodIdx mProposalHash mOffset mLimit = let
  query = do
    row@(_, _, _, _, _, periodIndex, _, pHash) <- getBallotsQuery
    case mPeriodIdx of
      Just periodIdx -> where_ (periodIndex .== toFields periodIdx)
      Nothing -> pass
    case mProposalHash of
      Just proposalHash -> where_ (pHash .== toFields proposalHash)
      Nothing -> pass
    pure row
  in withConnection $ \conn -> liftIO (fmap fromBallotsQuryResultRaw
    <$> runSelect conn (applyOffsetLimit mOffset mLimit (desc getBallotId) query))
    where
      getBallotId (a, _, _, _, _, _, _, _) = a.id

getBallotsForProposalWithVotingPower
  :: (MonadIO m, HasCap DbCap caps)
  => ProposalId
  -> Node.PeriodIdx
  -> CapsT caps m [(Ballot, Node.VotingPower)]
getBallotsForProposalWithVotingPower proposalId periodIdx = let
  query = do
    bl <- selectTable ballotsTable
    pv <- selectTable periodVotersTable
    where_ (bl.proposalId .== toFields proposalId .&& bl.periodIdx .== toFields periodIdx)
    where_ (pv.id .== bl.periodVoterId)
    pure (bl, pv.votingPower)
  in withConnection $ \conn -> liftIO (runSelect conn query)

insertBallotOperation
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> ProposalId
  -> Node.PeriodIdx
  -> Node.OperationHash
  -> PeriodVoterId
  -> Node.Ballot
  -> CapsT caps m BallotId
insertBallotOperation sourceBlockId proposalId periodIdx operationHash periodVoterId ballot
  = withConnection $ \conn -> listToOne $
      runInsert conn $
        Insert ballotsTable
          [toFields (Ballot (Nothing :: Maybe Int)
            proposalId periodIdx operationHash periodVoterId ballot sourceBlockId)
          ] (rReturning BallotOperations.id) Nothing
