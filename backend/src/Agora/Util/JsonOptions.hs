-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Util.JsonOptions
  ( defaultOptions
  , snakeCaseOptions
  ) where

import Data.Aeson (Options(..), SumEncoding(..))
import Data.Aeson.Casing (aesonPrefix, camelCase, snakeCase)
import Data.Char (isLower, toLower)
import Data.List (findIndex)

defaultOptions :: Options
defaultOptions = mkOptions camelCase

snakeCaseOptions :: Options
snakeCaseOptions = mkOptions snakeCase

mkOptions :: (String -> String) -> Options
mkOptions prefixModifier = (aesonPrefix prefixModifier)
  { sumEncoding = ObjectWithSingleField
  , constructorTagModifier = headToLower . stripConstructorPrefix
  }

stripConstructorPrefix :: String -> String
stripConstructorPrefix t =
    maybe t (flip drop t . decrementSafe) $ findIndex isLower t
  where
    decrementSafe 0 = 0
    decrementSafe i = i - 1

headToLower :: String -> String
headToLower []     = error "Can not use headToLower on empty String"
headToLower (x:xs) = toLower x : xs
