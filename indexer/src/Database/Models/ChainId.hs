-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.ChainId
  ( ChainIdPoly(..)
  , ChainId
  , chainIdTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Node qualified

type ChainIdRowId = DbId "chain_id"

data ChainIdPoly rowId chainId sourceBlockId = ChainId
  { id              :: rowId
  , chainId         :: chainId
  , sourceBlockId   :: sourceBlockId
  }

$(makeAdaptorAndInstance "pDbChainId" ''ChainIdPoly)

type ChainId'' rm = ChainIdPoly
  (ComputeFieldType rm ChainIdRowId)
  (ComputeFieldType rm Node.ChainId)
  (ComputeFieldType rm Blocks.BlockId)

type ChainId = ChainId'' 'HaskellRead

chainIdTable
  :: Table (ChainId'' 'DbWrite) (ChainId'' 'DbRead)
chainIdTable =
  table "chain_id" (pDbChainId
    (ChainId
      (tableField "id")
      (tableField "chain_id")
      (tableField "source_block_id")))
