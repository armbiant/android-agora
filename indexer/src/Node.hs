-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

-- | Module that abstracts access to a Tezos node. Here we try to abstract
-- Tezos protocol differences, possibility of missing data, http connection
-- hiccups etc.
module Node
  ( Ballot(..)
  , BallotOperationData(..)
  , Block(..)
  , BlockHash
  , BlockSpec(..)
  , ChainId(..)
  , Constants(..)
  , EpochIdx(..)
  , Hash(..)
  , Header(..)
  , Level(..)
  , Metadata(..)
  , MinimalBlockDelay(..)
  , NodeCap(..)
  , NotFoundError(..)
  , NumberType
  , NumericString(..)
  , Operation(..)
  , HeadBlockCache
  , OperationData(..)
  , OperationHash
  , PeriodIdx(..)
  , PeriodStatus(..)
  , PkHash
  , ProposalHash
  , ProposalOperationData(..)
  , ProposalStatus(..)
  , ProtocolHash
  , Quorum(..)
  , RawBlock(..)
  , RawConstants(..)
  , RawMetadata(..)
  , RawMetadataCurrent(..)
  , RawOperation(..)
  , RawOperationContent(..)
  , RawVoter(..)
  , Voter(..)
  , VotingPeriod(..)
  , VotingPeriodInfo(..)
  , VotingPeriodKind(..)
  , VotingPower(..)
  , ballotToText
  , getBlock
  , getConstantsForBlock
  , getQuorumForBlock
  , getVotersForBlock
  , nodeCap
  , parsePeriodStatus
  , parseProposalStatus
  , parseVotingPeriodKind
  , periodStatusToText
  , proposalStatusToText
  , rawBlockToBlock
  , rawConstantsToConstants
  , rawVoterToVoter
  , textToBallot
  , votingPeriodKindToText
  , waitFor
  ) where

import Control.Applicative
import Control.Monad.Catch
import Control.Monad.IO.Unlift
import Control.Concurrent.STM.TMVar
import Control.Concurrent.STM (atomically)
import Data.Aeson
import Data.Aeson qualified as Aeson
import Data.List qualified as DL
import Data.Maybe
import Data.String
import Data.Text hiding (concat, concatMap)
import Data.Time.Clock
import Deriving.Aeson
import Deriving.Aeson.Stock
import GHC.Int
import GHC.TypeLits
import Monad.Capabilities
import Servant
  (Capture, FromHttpApiData(..), Get, JSON, Proxy(..), ToHttpApiData(..), (:<|>)(..), (:>))
import Servant.Client
import System.Log.FastLogger
import Text.Read

import Common
import Logging
import ServerPool

instance IsString (Hash a) where
  fromString x = Hash (pack x)

data ProposalStatus
  = Active
  | Accepted
  | Rejected
  | Skipped
  deriving (Eq, Show, Ord, Enum, Bounded, Generic)

proposalStatusToText
  :: ProposalStatus
  -> Text
proposalStatusToText = \case
  Active   -> "active"
  Accepted -> "accepted"
  Rejected -> "rejected"
  Skipped  -> "skipped"

parseProposalStatus
  :: Text
  -> ProposalStatus
parseProposalStatus = \case
  "active"   -> Active
  "accepted" -> Accepted
  "rejected" -> Rejected
  "skipped"  -> Skipped
  _          -> error "Unkown proposal status"

newtype ChainId = ChainId { unChainId :: Text }
  deriving stock (Generic, Show)
  deriving newtype (FromJSON, ToJSON)

-- To implement special JSON parsing for block delay for legacy and recent
-- constants structure.
newtype MinimalBlockDelay = MinimalBlockDelay
  { unMinimalBlockDelay :: NumberType}
  deriving stock (Eq, Show)
  deriving newtype ToJSON

instance FromJSON MinimalBlockDelay where
  parseJSON v =
    (MinimalBlockDelay <$> parseJSON v) <|>
    (parseStringNum <$> parseJSON v) <|>
    (parseListOfStringNums <$> parseJSON v)
    where
      parseListOfStringNums :: [Text] -> MinimalBlockDelay
      parseListOfStringNums [] = error "Minimal block delay list empty"
      parseListOfStringNums (a: _) = parseStringNum a

      parseStringNum :: Text -> MinimalBlockDelay
      parseStringNum a = MinimalBlockDelay $ read $ unpack a

-- To implement special JSON parsing for some json fields in Constants
-- structure.
newtype NumericString = NumericString { unNumericString :: Int64 }
  deriving stock (Eq, Show)
  deriving newtype ToJSON

instance FromJSON NumericString where
  parseJSON v =
    (NumericString <$> parseJSON v) <|>
    (NumericString . read . unpack <$> parseJSON v)

data Constants = Constants
  { blocksPerCycle        :: Maybe NumberType
  , blocksPerVotingPeriod :: Maybe NumberType
  , cyclesPerVotingPeriod :: Maybe NumberType
  , timeBetweenBlocks     :: Maybe MinimalBlockDelay
  , minimalBlockDelay     :: Maybe MinimalBlockDelay
  , tokensPerRoll         :: Maybe NumericString
  , minimalStake          :: Maybe NumericString
  } deriving stock (Generic, Eq, Show)
    deriving (FromJSON, ToJSON) via (Snake Constants)

newtype Hash (t :: Symbol) = Hash { unHash :: Text }
  deriving stock (Generic, Show, Eq)
  deriving newtype (FromJSON, ToJSON, ToLogStr, Ord)

type BlockHash = Hash "Block"
type PkHash = Hash "PkHash"
type ProposalHash = Hash "Proposal"
type ProtocolHash = Hash "Protocol"
type OperationHash = Hash "Operation"

newtype RawConstants = RawConstants { unRawConstants :: Aeson.Value }
  deriving stock (Show, Eq, Generic)
  deriving newtype (FromJSON, ToJSON)

rawConstantsToConstants :: RawConstants -> Constants
rawConstantsToConstants (RawConstants value) = case fromJSON value of
  Aeson.Success x -> x
  Aeson.Error x   -> error ("Error decoding constants:" <> show value <> " Error: " <> x)

data RawVoter = RawVoter
  { pkh          :: PkHash
  , rolls        :: Maybe Int64
  , voting_power :: Maybe VotingPower
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla RawVoter)

instance Ord RawVoter where
  compare rv1 rv2 = compare rv1.pkh rv2.pkh

instance Eq RawVoter where
  rv1 == rv2 = rv1.pkh == rv2.pkh

data Voter = Voter
  { pkHash      :: PkHash
  , votingPower :: VotingPower
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla Voter)

rawVoterToVoter :: Constants -> RawVoter -> Voter
rawVoterToVoter constants rv =
    let
      oneRoll = fromIntegralSafe $ unNumericString $
        fromMaybe
          (fromMaybe (error "One constants for roll size not available")
            constants.minimalStake)
          constants.tokensPerRoll
    in rawVoterToVoter' oneRoll rv

rawVoterToVoter' :: Int64 -> RawVoter -> Voter
rawVoterToVoter' oneRoll rv = Voter rv.pkh $
  case rv.rolls of
    Just r -> VotingPower $ oneRoll * r
    Nothing -> case rv.voting_power of
      Just vp -> vp
      Nothing -> error "No voting power available for voter"

rawVoterToVoterUnsafe :: RawVoter -> Voter
rawVoterToVoterUnsafe rv = Voter rv.pkh $
  case rv.voting_power of
    Just vp -> vp
    Nothing -> error "No voting power available for voter"

data Ballot
  = Yay
  | Nay
  | Pass
  deriving stock (Eq, Generic, Ord, Show, Bounded, Enum)

ballotToText :: Ballot -> Text
ballotToText = \case
  Yay  -> "yay"
  Nay  -> "nay"
  Pass -> "pass"

textToBallot :: Text -> Ballot
textToBallot = \case
  "yay"  -> Yay
  "nay"  -> Nay
  "pass" -> Pass
  _      -> error "Unknown ballot"

instance ToJSON Ballot where
  toJSON b = toJSON $ ballotToText b

instance FromJSON Ballot where
  parseJSON v = textToBallot <$> parseJSON v

newtype VotingPower = VotingPower { unVotingPower :: Int64 }
  deriving stock (Generic, Show)
  deriving newtype (Num, Enum, Ord, Eq, ToJSON, ToLogStr)

-- This instance is required because voting power comes as a
-- string in Node response.
instance FromJSON VotingPower where
  parseJSON v
    =   (VotingPower <$> parseJSON v)
    <|> (VotingPower . fromIntegral . read @Integer <$> parseJSON v)

newtype PeriodIdx = PeriodIdx { unPeriodIdx :: NumberType }
  deriving stock (Generic, Show)
  deriving newtype (Num, Enum, Integral, Real, Ord, Eq, FromJSON, ToJSON, ToLogStr)

instance FromIntegral PeriodIdx Int

newtype Quorum = Quorum { unQuorum :: NumberType }
  deriving stock (Generic, Show)
  deriving newtype (Num, Enum, Ord, Eq, FromJSON, ToJSON, ToLogStr)

newtype EpochIdx = EpochIdx { unEpochIdx :: NumberType }
  deriving stock (Generic, Show)
  deriving newtype (Num, Enum, Ord, Eq, FromJSON, ToJSON, ToLogStr)

newtype Level = Level { unLevel :: NumberType }
  deriving stock (Generic, Show)
  deriving newtype (Num, Ord, Enum, Eq, FromJSON, ToJSON, ToLogStr)

type NumberType = Int32

data PeriodStatus
  = ActivePeriod
  | NoProposals
  | NoQuorum
  | NoSupermajority
  | NoSingleWinner
  | Success
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

periodStatusToText :: PeriodStatus -> Text
periodStatusToText = \case
  ActivePeriod    -> "active"
  NoProposals     -> "no_proposals"
  NoQuorum        -> "no_quorum"
  NoSupermajority -> "no_supermajority"
  NoSingleWinner  -> "no_singlewinner"
  Node.Success    -> "success"

parsePeriodStatus :: Text -> PeriodStatus
parsePeriodStatus = \case
  "active"           -> ActivePeriod
  "no_proposals"     -> NoProposals
  "no_quorum"        -> NoQuorum
  "no_supermajority" -> NoSupermajority
  "no_singlewinner"  -> NoSingleWinner
  "success"          -> Node.Success
  s                  -> error $ "Unknown period status: " <> unpack s

data VotingPeriodKind
  = Proposal
  | Exploration
  | Testing
  | Promotion
  | Adoption
  deriving stock (Show, Eq, Generic, Enum)

parseVotingPeriodKind :: Text -> Maybe VotingPeriodKind
parseVotingPeriodKind "proposal"    = Just Proposal
parseVotingPeriodKind "testing_vote" = Just Exploration
parseVotingPeriodKind "exploration" = Just Exploration
parseVotingPeriodKind "testing"     = Just Testing
parseVotingPeriodKind "cooldown"    = Just Testing
parseVotingPeriodKind "promotion"   = Just Promotion
parseVotingPeriodKind "promotion_vote"   = Just Promotion
parseVotingPeriodKind "adoption"    = Just Adoption
parseVotingPeriodKind s = error $ "Unknown voting period kind: " <> unpack s

instance ToJSON VotingPeriodKind where
  toJSON = toJSON . votingPeriodKindToText

votingPeriodKindToText :: VotingPeriodKind -> Text
votingPeriodKindToText = \case
  Proposal    -> "proposal"
  Exploration -> "exploration"
  Testing     -> "testing"
  Promotion   -> "promotion"
  Adoption    -> "adoption"

instance ToLogStr VotingPeriodKind where
  toLogStr = toLogStr . votingPeriodKindToText

instance FromJSON VotingPeriodKind where
  parseJSON v = do
    r <- parseJSON v
    case parseVotingPeriodKind r of
      Just x  -> pure x
      Nothing -> fail "Parsing of voting period kind failed"

data VotingPeriodInfo = VotingPeriodInfo
  { index :: NumberType
  , kind  :: VotingPeriodKind
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla VotingPeriodInfo)

-- A friendlier representation of the Metadata than what the node provides.
-- Only includes the attributes that we are interested in.
data Metadata = Metadata
  { period     :: PeriodIdx
  , periodKind :: VotingPeriodKind
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla Metadata)

-- Represent the metadata as provided by the node, we take into account
-- both the current and legacy variants.
data RawMetadata
  = RMDCurrent RawMetadataCurrent
  | RMDLegacy RawMetadataLegacy
  deriving stock Show

instance FromJSON RawMetadata where
  parseJSON v =
    (RMDCurrent <$> parseJSON v) <|> (RMDLegacy <$> parseJSON v)

instance ToJSON RawMetadata where
  toJSON (RMDCurrent v) = toJSON v
  toJSON (RMDLegacy v)  = toJSON v

data VotingPeriod = VotingPeriod
  { voting_period :: VotingPeriodInfo
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla VotingPeriod)

data RawMetadataCurrent = RawMetadataCurrent
  { voting_period_info :: VotingPeriod
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla RawMetadataCurrent)

data LevelInfoLegacy = LevelInfoLegacy
  { voting_period :: NumberType
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla LevelInfoLegacy)

data RawMetadataLegacy = RawMetadataLegacy
  { level              :: LevelInfoLegacy
  , voting_period_kind :: VotingPeriodKind
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla RawMetadataLegacy)

data Header = Header
  { level       :: Level
  , predecessor :: BlockHash
  , timestamp   :: UTCTime
  , proto       :: NumberType
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla Header)

data RawBlock = RawBlock
  { hash       :: BlockHash
  , chain_id   :: ChainId
  , header     :: Header
  , protocol   :: ProtocolHash
  , metadata   :: Maybe RawMetadata
  , operations :: [[RawOperation]]
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla RawBlock)

-- A friendlier representation of the Block than what the node provides.
data Block = Block
  { hash       :: BlockHash
  , chainId    :: ChainId
  , protocol   :: ProtocolHash
  , header     :: Header
  , metadata   :: Metadata
  , operations :: [Operation]
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla Block)

instance  ToLogStr Block where
  toLogStr (Block hash _ _ header _ _) =
    "Block: (" <>
    toLogStr hash <>
    ") at level " <>
    (toLogStr header.level) <>
    " with predecessor " <>
    (toLogStr header.predecessor)

instance  ToLogStr Header where
  toLogStr header =
    "Block header: level " <>
    (toLogStr header.level) <>
    " with predecessor " <>
    (toLogStr header.predecessor)

data RawOperation = RawOperationRaw
  { contents :: [RawOperationContent]
  , hash     :: OperationHash
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla RawOperation)

data RawOperationContent = RawOperationContent
  { kind      :: Text
  , proposals :: Maybe [ProposalHash]
  , proposal  :: Maybe ProposalHash
  , ballot    :: Maybe Ballot
  , source    :: Maybe PkHash
  , period    :: Maybe PeriodIdx
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla RawOperationContent)

data ProposalOperationData = ProposalOperationData
  { proposals :: [ProposalHash]
  , source    :: PkHash
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla ProposalOperationData)

data BallotOperationData = BallotOperationData
  { proposal :: ProposalHash
  , ballot   :: Ballot
  , source   :: PkHash
  , period   :: PeriodIdx
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla BallotOperationData)

instance ToLogStr BallotOperationData where
  toLogStr = toLogStr . show

-- A friendlier representation of Operation than what the node provides.
data Operation = Operation
  { hash          :: OperationHash
  , operationData :: OperationData
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON) via (Vanilla Operation)

data OperationData
  = ProposalOperation ProposalOperationData
  | BallotOperation BallotOperationData
  deriving stock (Generic, Show)
  deriving (FromJSON, ToJSON) via (Vanilla OperationData)

rawMetadataToMetadata :: RawMetadata -> Metadata
rawMetadataToMetadata = \case
  RMDLegacy l -> Metadata
    { period = PeriodIdx l.level.voting_period
    , periodKind = l.voting_period_kind
    }
  RMDCurrent c -> Metadata
    { period = PeriodIdx c.voting_period_info.voting_period.index
    , periodKind = c.voting_period_info.voting_period.kind
    }

rawBlockToBlock :: RawBlock -> Block
rawBlockToBlock b = Block
  { hash = b.hash
  , chainId = b.chain_id
  , protocol = b.protocol
  , header = b.header
  , operations = concat $ fmap rawOperationsToOperations b.operations
  , metadata = rawMetadataToMetadata $
      fromMaybe (error "Unexpected lack of metadata for block") b.metadata
  }

rawOperationsToOperations :: [RawOperation] -> [Operation]
rawOperationsToOperations ropl = concatMap ropToOp ropl
  where

    ropToOp :: RawOperation -> [Operation]
    ropToOp rop = mapMaybe (rocToOp rop.hash) (contents rop)

    rocToOp :: OperationHash -> RawOperationContent -> Maybe Operation
    rocToOp hash roc =
      case roc.kind of
        "proposals" -> Just $ Operation hash $ ProposalOperation
          (ProposalOperationData
            (fromMaybe (error "Proposals list not found in proposal operation") roc.proposals)
            (fromMaybe (error "Source address not found in proposal operation") roc.source))
        "ballot" -> Just $ Operation hash $ BallotOperation (BallotOperationData
          (fromMaybe (error "Proposal field not found for ballot operation") roc.proposal)
          (fromMaybe (error "Ballot field not found for ballot operation") roc.ballot)
          (fromMaybe (error "Source field not found for ballot operation") roc.source)
          (fromMaybe (error "Period field not found for ballot operation") roc.period)
          )
        _           -> Nothing

type API =
  "chains" :> "main" :> "blocks" :> Capture "blockspec" BlockSpec
    :> Get '[JSON] RawBlock :<|>
  "chains" :> "main" :> "blocks" :> Capture "blockspec" BlockSpec
    :> "header" :> Get '[JSON] Header :<|>
  "chains" :> "main" :> "blocks" :> Capture "blockhash" Text
    :> "metadata" :> Get '[JSON] RawMetadata :<|>
  "chains" :> "main" :> "blocks" :> Capture "blockhash" Text
    :> "votes" :> "listings" :> Get '[JSON] [RawVoter] :<|>
  "chains" :> "main" :> "blocks" :> Capture "blockhash" Text
    :> "context" :> "constants" :> Get '[JSON] RawConstants :<|>
  "chains" :> "main" :> "blocks" :> Capture "blockhash" Text
    :> "context" :> "delegates" :> Capture "delegate" Text
    :> "staking_balance" :> Get '[JSON] VotingPower :<|>
  "chains" :> "main" :> "blocks" :> Capture "blockhash" Text
    :> "votes" :> "current_quorum" :> Get '[JSON] Quorum


api :: Proxy API
api = Proxy

data BlockSpec
  = LevelSpec Level
  | HashSpec BlockHash
  | NodeHead
  deriving Show

instance ToLogStr BlockSpec where
  toLogStr x = toLogStr $ show x

instance ToHttpApiData BlockSpec where
  toUrlPiece bs = case bs of
    LevelSpec (Level l) -> pack $ show l
    HashSpec (Hash t)   -> t
    NodeHead            -> "head"

instance FromHttpApiData BlockSpec where
  parseUrlPiece bs = case bs of
    "head" -> Right NodeHead
    x -> case readMaybe (unpack bs) of
      Just l -> Right $ LevelSpec $ Level l
      Nothing -> Right $ HashSpec $ Hash x

instance FromHttpApiData (Hash a) where
  parseUrlPiece = Right . Hash

instance FromHttpApiData Level where
  parseUrlPiece = Right . Level . read . unpack

instance FromHttpApiData PeriodIdx where
  parseUrlPiece = Right . PeriodIdx . read . unpack

instance FromHttpApiData EpochIdx where
  parseUrlPiece = Right . EpochIdx . read . unpack

getBlock' :: BlockSpec -> ClientM RawBlock
getBlockHeader' :: BlockSpec -> ClientM Header
getMetadataForBlock' :: Text -> ClientM RawMetadata
getVotersForBlock' :: Text -> ClientM [RawVoter]
getConstantsForBlock' :: Text -> ClientM RawConstants
getVoterStakingBalance' :: Text -> Text -> ClientM VotingPower
getQuorumForBlock' :: Text -> ClientM Quorum
getBlock' :<|> getBlockHeader' :<|> getMetadataForBlock' :<|> getVotersForBlock'
  :<|> getConstantsForBlock' :<|> getVoterStakingBalance'
  :<|> getQuorumForBlock'
  = client api

-- We have a bunch of nodes from config, and for each request to a node that
-- fails, we try another one with the same request. If all nodes fail, then
-- throw the throwRE from the last node that was tried.
--
-- We can have a mix of nodes where some are running in full or rolling mode and some running in
-- archive mode. Some info is not present in full/rolling nodes, and this logic
-- automatically handles fetching the info from what ever node that has the
-- info. So we need at least one archive node to bootstrap from genesis, and only need a rolling
-- or full node for indexing recent periods..

data NodeCap m = NodeCap
  { _getBlock             :: BlockSpec -> m Block
  , _getMetadataForBlock  :: BlockHash -> m RawMetadata
  , _getHeadBlockHeader   :: m Header
  , _getVotersForBlock    :: BlockHash -> m [Voter]
  , _getQuorumForBlock    :: BlockHash -> m (Maybe Quorum)
  , _getConstantsForBlock :: BlockHash -> m RawConstants
  , _getStakingBalanceForVoter :: BlockHash -> PkHash -> m VotingPower
  }
$(makeCap ''NodeCap)

minBlockInterval :: Int32
minBlockInterval = 15

type HeadBlockCache = TMVar (Maybe Node.Header)

nodeCap
  :: (MonadMask m, MonadThrow m, MonadCatch m, MonadIO m)
  => HeadBlockCache
  -> CapImpl NodeCap '[NodeCap, ServerPoolCap, LoggingCap] m
nodeCap headBlockCache = CapImpl $ NodeCap
  { _getBlock = \bs -> do
      let
        notFoundHandler (e :: NotFoundError) = do
          case bs of
            LevelSpec level -> do
              logDebug "Getting head block header..."
              waitPeriod <- handle
                (\(er :: SomeException) -> do
                  logDebug $ toLogStr $ "There was an error in fetching the network head block:" <> showt er
                  logDebug "Using a default wait time of 20 secs..."
                  pure 20) $ do
                nodeHeadHeader <- getHeadBlockHeader
                logDebug $ toLogStr $ "Block " <> showt level <> " not found in node, head level is:" <> (showt nodeHeadHeader.level)
                pure $ max 0 $ min 20 (unLevel $ level - nodeHeadHeader.level) * minBlockInterval
                -- We use 15 sec min block time and the current node head block level
                -- to approximately compute how long we should wait for a block at tje
                -- requested level to appear. But we put a max limit on this wait time
                -- so that some node that is lagging too much won't delay block fetch
                -- from other, non-lagging nodes (by returning a very old level because
                -- the `getHeadBlock` request happened to be retrivied from the lagging node).
              logDebug $ toLogStr $ "waiting for "
                <> showt waitPeriod <> " seconds..."
              waitFor (fromIntegral waitPeriod)
              logDebug $ toLogStr $ "And retrying for block: " <> showt level
              getBlock bs
            _ -> throwM e -- Without target level we wouldn't know how long to wait...
      handle notFoundHandler $ do
        logDebug $ toLogStr $ "Fetching block " <> showt bs <> " from nodes"
        rb <- runRequest $ getBlock' bs
        case rb.metadata of
          Just _ -> pure $ rawBlockToBlock rb
          Nothing -> do
            md <- getMetadataForBlock rb.hash
            pure $ let
              RawBlock {..} = rb
              in rawBlockToBlock $ RawBlock {metadata = Just md, ..}
  , _getMetadataForBlock = \hash -> runRequest $ getMetadataForBlock' (unHash hash)
  , _getHeadBlockHeader = do
      liftIO (atomically $ readTMVar headBlockCache) >>= \case
        Nothing ->
          bracketOnError
            (liftIO $ atomically $ takeTMVar headBlockCache)
            (\v -> liftIO $ atomically $ putTMVar headBlockCache v)
            (\_ -> do
              headBlockHeader <- runRequest (getBlockHeader' NodeHead)
              logDebug $ "Populating head block header cache for the first time with: " <> toLogStr headBlockHeader
              liftIO $ atomically $ putTMVar headBlockCache (Just headBlockHeader)
              pure headBlockHeader)
        Just cachedHead -> do
          ct <- liftIO getCurrentTime
          if (diffUTCTime ct cachedHead.timestamp) > fromIntegral minBlockInterval
            then do
              bracketOnError
                (liftIO $ atomically $ takeTMVar headBlockCache)
                (\v -> liftIO $ atomically $ putTMVar headBlockCache v)
                (\_ -> do
                  headBlock <- runRequest (getBlockHeader' NodeHead)
                  if cachedHead.level < headBlock.level -- Never move cache head backward
                    then do
                      logDebug $ "Refreshing head block cache with: " <> toLogStr headBlock
                      liftIO $ atomically $ putTMVar headBlockCache (Just headBlock)
                      pure headBlock
                    else do
                      logDebug "Skipped refrsh of head block cache because cached head was more recent..."
                      liftIO $ atomically $ putTMVar headBlockCache (Just cachedHead)
                      pure cachedHead)
            else pure cachedHead
  , _getVotersForBlock = \hash -> let
      notFoundHandler (_ :: NotFoundError) = pure [] -- Voters is just not available for some blocks after genesis
      in do
        rawVoters <- handle notFoundHandler (runRequest $ getVotersForBlock' (unHash hash))
        case rawVoters of
          [] -> pure []
          (rv : _) -> case rv.voting_power of
            Just _ -> pure $ rawVoterToVoterUnsafe <$> rawVoters -- We can assume all the voters in the list have voting_power attribute, hence ok to call
                -- the unsafe function that require voting_power attribute.
            _ -> case rv.rolls of
              Just r -> do
                -- We have only rolls attribute for this voter.
                -- So try to infer tokens per roll value from the staking
                -- balance response for this voter. Using protocol constants
                -- for this turned out to be unrealiable. This is because
                -- sometimes voting snapshot is taken before protocol
                -- activation, so the older roll size might endup getting used.
                tokensPerRoll <- inferTokensPerRoll hash rv.pkh r
                logDebug $ "Inferring token rolls to be:" <> toLogStr tokensPerRoll
                pure $ fmap (rawVoterToVoter' tokensPerRoll) rawVoters
              Nothing -> error "Voter does not have neither rolls or voting power attributes"
  , _getQuorumForBlock = \hash -> let
      notFoundHandler (_ :: NotFoundError) = pure Nothing -- Quorum is just not available for some blocks after genesis
      in handle notFoundHandler
          (runRequest (Just <$> getQuorumForBlock' (unHash hash)))
  , _getConstantsForBlock =
      \hash -> runRequest $ getConstantsForBlock' (unHash hash)
  , _getStakingBalanceForVoter =
      \hash voterAddress -> runRequest $
        getVoterStakingBalance' (unHash hash) (unHash voterAddress)
  }

inferTokensPerRoll
  :: (MonadIO m, HasCap NodeCap caps)
  => BlockHash
  -> PkHash
  -> Int64
  -> CapsT caps m Int64
inferTokensPerRoll bHash vAddress rolls = do
  sb <- getStakingBalanceForVoter bHash vAddress
  case DL.find (checkRollsizeMatch (unVotingPower sb)) [1..] of
    Just x -> pure $ toRollSize x
    Nothing -> error "Cannot figure voting power"
  where
    checkRollsizeMatch :: Int64 -> Int64 -> Bool
    checkRollsizeMatch vp p = div vp (toRollSize p) == rolls
    toRollSize x = x * 1000 * 1000000
