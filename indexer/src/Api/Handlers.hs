-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | The module that implements various api endpoint handlers.
module Api.Handlers
  ( getBallots
  , getBallotsForPeriod
  , getBlockCount
  , getEpoch
  , getEpochs
  , getHead
  , getLevel
  , getProposal
  , getProposalOperations
  , getProposalsForEpoch
  , getProtocols
  , getVersion
  , getVotersForPeriod
  ) where

import Control.Applicative ((<|>))
import Control.Monad.Catch
import qualified Data.List as DL
import Data.Maybe
import Data.Text
import Data.Time
import Servant

import Caps
import Common
import Data.Version (showVersion)
import Database.Mapping
import qualified Database.Models.BallotOperations as BallotOperations
import qualified Database.Models.Blocks as Blocks
import qualified Database.Models.NetworkState as NetworkState
import qualified Database.Models.ProposalOperations as ProposalOperations
import qualified Database.Models.Proposals as Proposals
import qualified Database.Models.Protocols as Protocols
import qualified Database.Models.Voters as Voters
import qualified DataStore as Ds

import qualified Api.Types as AT
import qualified Node
import Paths_agora_indexer (version)

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

-- | Handler for the endpoint that responds with the cabal project
-- version.
getVersion :: ApiM Text
getVersion = pure $ pack $ showVersion version

-- | Handler for the endpoint that responds with the the block at
-- the specified level.
getLevel
  :: Node.Level
  -> ApiM AT.Block
getLevel level =
  Ds.getBlock (Node.LevelSpec level) >>= \case
    Just block -> pure $ AT.Block block.level block.blockTime
    Nothing    -> throwM err404

-- | Handler for the endpoint that responds with the last processed
-- block.
getHead :: ApiM AT.Head
getHead =
  Ds.getLastCommitedBlock >>= \case
    Nothing -> throwM err404
    Just headBlock -> do
      protocol <- Ds.getProtocolForBlock headBlock
      pure $ AT.Head
        { level = headBlock.level
        , hash = headBlock.hash
        , protocol = protocol.protocolHash
        , timestamp = headBlock.blockTime
        }

-- | Get ballots for a proposal in the provided period/proposal hash combinations.
getBallots
  :: Maybe Node.ProposalHash
  -> Maybe Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> ApiM [AT.Ballot]
getBallots mProposalHash mPeriodId mOffset mLimit = do
  case mProposalHash of
    Nothing -> case mPeriodId of
      Just periodIdx -> getBallotsForPeriod periodIdx mOffset mLimit
      Nothing -> throwRE "Unsupported request: period index or proposal hash required"
    Just proposalHash -> case mPeriodId of
      Nothing -> fmap convertBallots <$> Ds.getBallotsForProposal Nothing (Just proposalHash) mOffset mLimit
      Just periodIdx -> fmap convertBallots <$> Ds.getBallotsForProposal (Just periodIdx) (Just proposalHash) Nothing Nothing

-- | Makes a Ballot value from a bunch of data fetched from the database.
convertBallots
  :: Ds.BallotsQueryResult
  -> AT.Ballot
convertBallots bqr =
  AT.Ballot
    (AT.Delegate bqr.voterPkHash Nothing Nothing)
    bqr.ballot.ballot
    bqr.voterVotingPower
    (fromIntegralSafe $ unDbId bqr.ballot.id)
    bqr.operationHash
    bqr.timestamp
    (AT.SimpleProposal Nothing bqr.proposalHash)
    (AT.SimplePeriodInfo bqr.periodIdx bqr.epochIdx)

-- | Gets all ballots for a period.
getBallotsForPeriod
  :: Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> ApiM [AT.Ballot]
getBallotsForPeriod periodIndex mOffset mLimit = do
    fmap convertBallots <$> Ds.getBallotsForProposal (Just periodIndex) Nothing mOffset mLimit

-- | Get all voters for a period.
getVotersForPeriod
  :: Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> ApiM [AT.PeriodVoter]
getVotersForPeriod periodIdx offset limit = do
  fmap convertPeriodVoter <$> Ds.getVotersForPeriodWithVotingPower periodIdx offset limit
  where
    convertPeriodVoter :: (Voters.Voter, Node.VotingPower) -> AT.PeriodVoter
    convertPeriodVoter (voter, vp) = AT.PeriodVoter
      { delegate = AT.Delegate voter.voterAddress Nothing Nothing
      , votingPower = vp
      }

-- | Get the total blocks that the indexer have indexed.
getBlockCount
  :: ApiM Node.Level
getBlockCount = Ds.getBlockCount

-- | Accept the db counterpart of a proposal, and after doing additional
-- queries, makes a fullfledged proposal object for the response.
convertProposal
  :: Proposals.Proposal
  -> ApiM AT.Proposal
convertProposal proposal = do
  (_, proposer) <- Ds.getProposalOperationForProposal proposal
  epoch <- Ds.getEpochForPeriod proposal.periodIdx
  votingPower <- Ds.getVotingPowerForProposal proposal.id
  lastActivePeriod <-
    Ds.getLastActiveBlockForProposalInEpoch proposal.proposalHash epoch >>= \case
      Just lastActiveBlock -> Ds.getPeriodForBlock lastActiveBlock
      Nothing ->
        -- This means that the proposal is still active, so return the last
        -- available period. (successful proposals switch to accepted status at
        -- the end of epoch)
        Ds.getNetworkState >>= \case
          Just ns -> pure ns.periodIdx
          Nothing -> throwRE "Network state unavailable unexpectedly!"
  status <- Ds.getProposalStatus proposal.id
  pure $ AT.Proposal
    { epoch = epoch
    , votingPower = votingPower
    , hash = proposal.proposalHash
    , firstPeriod = proposal.periodIdx
    , lastPeriod = lastActivePeriod
    , initiator = AT.Delegate proposer.voterAddress Nothing Nothing
    , status = convertProposalStatus status
    }

-- | The handler that fetches the most recent proposal with the provided hash.
getProposal
  :: Node.ProposalHash
  -> ApiM AT.Proposal
getProposal proposalHash = do
  Ds.getRecentProposalWithHash proposalHash >>= \case
    Nothing -> throwM err404
    Just proposal -> convertProposal proposal

convertProposalStatus
  :: Node.ProposalStatus
  -> AT.ProposalStatus
convertProposalStatus = \case
  Node.Active   -> AT.Active
  Node.Accepted -> AT.Accepted
  Node.Rejected -> AT.Rejected
  Node.Skipped  -> AT.Skipped

-- | Get all proposals for an Epoch.
getProposalsForEpoch
  :: Maybe Node.EpochIdx
  -> ApiM [AT.Proposal]
getProposalsForEpoch = \case
  Just epochIdx -> do
    periodIdx <- Ds.getProposalPeriodForEpoch epochIdx
    proposals <- Ds.getProposalsForPeriod periodIdx
    mapM convertProposal proposals
  Nothing -> throwRE "Unsupported request: Epoch index is required"

-- | Gets proposal operations for given proposal hash/period combinations.
getProposalOperations
  :: Maybe Node.ProposalHash
  -> Maybe Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> ApiM [AT.ProposalOperation]
getProposalOperations mphash mperiodIdx mOffset mLimit = do
  case mphash of
    Just proposalHash -> do
      proposalOperationsWithInfo <-
        Ds.getProposalOperationsForProposal proposalHash mperiodIdx mOffset mLimit
      pure $ makeProposalOperation <$> proposalOperationsWithInfo
    Nothing -> case mperiodIdx of
      Just periodIdx -> do
        getProposalOperationsForPeriod periodIdx mOffset mLimit
      Nothing        -> throwRE "Proposal hash or period index required"

-- | Gets proposal operations for a period.
getProposalOperationsForPeriod
  :: Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> ApiM [AT.ProposalOperation]
getProposalOperationsForPeriod periodIdx mOffset mLimit =  do
    proposalOperationsWithInfo <- Ds.getProposalOperationsForPeriod
      periodIdx mOffset mLimit
    pure $ makeProposalOperation <$> proposalOperationsWithInfo

-- | Glues together various data from the database query and makes a
-- proposal operation object.
makeProposalOperation
  :: ( ProposalOperations.ProposalOperation, Node.PkHash, UTCTime
     , Node.Level, Node.ProposalHash, Node.VotingPower)
  -> AT.ProposalOperation
makeProposalOperation
  (proposalOperation, proposer, timestamp , level, pHash, votingPower) =
    AT.ProposalOperation
      (fromIntegralSafe $ unDbId proposalOperation.id)
      proposalOperation.operationHash
      (AT.SimpleProposal Nothing pHash)
      timestamp
      votingPower
      (AT.Delegate proposer Nothing Nothing)
      level

-- | Endpoint handler that fetches protocol info
getProtocols
  :: Maybe Int
  -> Maybe Int
  -> ApiM [AT.Protocol]
getProtocols mOffset mLimit = do
  protocolDataRaw <- Ds.getProtocols mOffset mLimit
  pure $ flip fmap protocolDataRaw $
    \(hash, level, meLevel, consts) ->
        AT.Protocol hash level meLevel
          (convertConstants $ Node.rawConstantsToConstants consts)

-- | Convert constants to the format as expected by the response.
convertConstants
  :: Node.Constants
  -> AT.Constants
convertConstants c@(Node.Constants {..}) = AT.Constants
  { blocksPerCycle = blocksPerCycle
  , blocksPerVoting = Just $ Node.unLevel $ getBlocksPerVotingPeriod c
  , timeBetweenBlocks = Node.unMinimalBlockDelay <$>
      (minimalBlockDelay <|> timeBetweenBlocks)
  }

-- | Gets blocks per voting period value from the protocol constants.
getBlocksPerVotingPeriod
  :: Node.Constants
  -> Node.Level
getBlocksPerVotingPeriod c = case c.blocksPerVotingPeriod of
  Just x -> Node.Level x
  Nothing -> fromMaybe
    (error "Constants does not have info to infer blocks per voting period") $ do
      cpv <- c.cyclesPerVotingPeriod
      bpc <- c.blocksPerCycle
      pure $ Node.Level $ cpv * bpc

-- | Computes block delay from protocol constants.
getMinBlockDelayInSecs
  :: Node.Constants
  -> Node.NumberType
getMinBlockDelayInSecs c = case c.minimalBlockDelay of
  Just x -> Node.unMinimalBlockDelay x
  Nothing -> case c.timeBetweenBlocks of
    Just x -> Node.unMinimalBlockDelay x
    Nothing -> error "Constants does not have info to infer minimal block delay."

-- | Endpoint handler that assembles the period info from the database.
getPeriod
  :: Node.PeriodIdx
  -> ApiM AT.PeriodInfo
getPeriod periodIdx = do
  (ns, startLevel) <- Ds.getNetworkStateForPeriod periodIdx
  startBlock <- Ds.getBlock (Node.LevelSpec startLevel) >>= \case
    Just b -> pure b
    Nothing -> throwRE "Block at the start of the period not found"
  periodProtocol <- Ds.getProtocolForBlock startBlock
  let constants = Node.rawConstantsToConstants periodProtocol.constants
  let levelsPerPeriod = getBlocksPerVotingPeriod constants
  let endLevel = startLevel + levelsPerPeriod - 1
  status <- Ds.getPeriodStatus periodIdx
  ballots <- Ds.getBallotsForProposal (Just periodIdx) Nothing Nothing Nothing
  let
    (yayBallots, nayBallots, passBallots) =
      DL.foldr (\b (y, n, p) -> case b.ballot.ballot of
        Node.Yay -> (b:y, n, p)
        Node.Nay -> (y, b:n, p)
        Node.Pass -> (y, n, b:p)) ([], [], []) ballots

  let yayBallotsCount = DL.length yayBallots
  let nayBallotsCount = DL.length nayBallots
  let passBallotsCount = DL.length passBallots

  endTime <- Ds.getBlock (Node.LevelSpec endLevel) >>= \case
    Just b  -> pure b.blockTime
    Nothing -> do
      -- End level not yet in database. We try to compute the period
      -- end time from the last block from this period we have seen and
      -- the block delay.
      Ds.getLastCommitedBlockInLevelRange startBlock.level endLevel >>= \case
        Just lastCommitedBlockInPeriod -> do
          let levelsToGo = endLevel - lastCommitedBlockInPeriod.level
          pure $ addUTCTime
            (secondsToNominalDiffTime $
              realToFrac $ Node.unLevel levelsToGo * getMinBlockDelayInSecs constants)
            lastCommitedBlockInPeriod.blockTime
        Nothing -> throwRE "Blocks belonging to period not found unexpectedly"

  voters <- Ds.getVotersForPeriodWithVotingPower periodIdx Nothing Nothing

  pure $ AT.PeriodInfo
    (convertPeriodType ns.periodKind)
    startLevel
    endLevel
    startBlock.blockTime
    endTime
    (Just $ convertPeriodStatus status)
    ns.epoch
    (Just $ fromIntegralSafe yayBallotsCount)
    (Just $ Prelude.sum (Ds.voterVotingPower <$> yayBallots))

    (Just $ fromIntegralSafe nayBallotsCount)
    (Just $ Prelude.sum (Ds.voterVotingPower <$> nayBallots))

    (Just $ fromIntegralSafe passBallotsCount)
    (Just $ Prelude.sum (Ds.voterVotingPower <$> passBallots))

    ((\q -> realToFrac (Node.unQuorum q) / 100) <$> ns.quorum)
    (Just 80)
    (Just $ fromIntegralSafe $ DL.length voters)
    (Just $ Prelude.sum $ snd <$> voters)
    periodIdx

convertPeriodType
  :: Node.VotingPeriodKind
  -> AT.PeriodType
convertPeriodType = \case
  Node.Proposal    -> AT.Proposing
  Node.Exploration -> AT.Exploration
  Node.Testing     -> AT.Testing
  Node.Promotion   -> AT.Promotion
  Node.Adoption    -> AT.Adoption

convertPeriodStatus
  :: Node.PeriodStatus
  -> AT.PeriodStatus
convertPeriodStatus = \case
  Node.ActivePeriod    -> AT.PActive
  Node.NoProposals     -> AT.PNoProposals
  Node.NoQuorum        -> AT.PNoQuorum
  Node.NoSupermajority -> AT.PNoSupermajority
  Node.Success         -> AT.PSuccess
  Node.NoSingleWinner  -> AT.PNoSingleWinner

-- | Fetches all the info for an epoch.
getEpoch
  :: Node.EpochIdx
  -> ApiM AT.EpochInfo
getEpoch epochIdx = do
  periods <- Ds.getPeriodsInEpoch epochIdx
  periodInfos <- mapM getPeriod periods
  pure $ AT.EpochInfo epochIdx periodInfos

-- | Endpoint handler that fetches all the info for all the available epochs.
getEpochs :: ApiM [AT.EpochInfo]
getEpochs = do
  epochIdxs <- Ds.getAllEpochs
  mapM getEpoch epochIdxs
