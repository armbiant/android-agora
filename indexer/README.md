# Agora Tezos Indexer

An indexer that indexes data required for Agora backend to run.

How to run:

1. Create a postgresql database for the indexer, the schema will be initialized by
   the sync component on startup.
3. Start the indexer by running `cabal exec indexerSync`, providing database
   connection string, the list of Tezos nodes to source the blocks from. These
   can be either passed via environment variables or as command line arguments.
   Sample invocation are shown below.
4. Start the API sever by running `cabal exec indexerApi`.


### Starting the indexer

```
export TZ_INDEXER_DATABASE=postgresql://user@localhost/dbname &&
export TZ_INDEXER_NODES="https://node1.com,https://node2.com" &&
cabal exec -- indexerSync
```

or


```
cabal exec -- indexerSync \
    --database=postgresql://user@localhost/dbname \
    --node=https://node1.net \
    --node=https://node2.net
```

### Starting the API server

```
export TZ_INDEXER_DATABASE=postgresql://user@localhost/dbname &&
export TZ_INDEXER_LISTEN_PORT=4040 &&
cabal exec -- indexerApi
```

Or

```
cabal exec -- indexerApi \
    --database=postgresql://user@localhost/dbname \
    --port=4040
```

If you are using `stack` just replace `cabal` with `stack`.

### Building a docker image

Just change directory to `docker` folder and use `docker build .` command there
to build a docker image. By default, the bundled Dockerfile will clone the
agora repo's `master` branch. To use a different branch, you can use a
argument `repobranch` as shown below to specify it.

`docker build . --build-arg repobranch=mybranch`

While starting a container using the image the
behavior of the indexer can be configured using the environment variables.

### How to run tests:

Tests use `ephemeralpg` to create temporary databases. Here is a sample
command to run the tests, after you have installed it and have `pg_tmp`
in your path.

```
export TZ_INDEXER_DATABASE=$(pg_tmp) && cabal test
```

To run tests that have "my-tests" in its description, use the following
command.

```
cabal test --test-options='--match ''my-tests'''
```

If you want to watch the logs during tests, here is how you do it.

```
cabal test --test-show-details=streaming
```
