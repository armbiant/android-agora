-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE DuplicateRecordFields #-}

{-|

Description: This module mostly handles the implementation of the Agora
endpoints using the data fetched from the Indexer via the Indexer capability.

|-}

module Agora.Indexer.Handlers
  ( getBallots
  , getNonVoters
  , getPeriodInfo
  , getProposal
  , getProposalVotes
  , getProposals
  , getSpecificProposalVotes
  ) where

import Data.List ((!!))
import qualified Data.List as DL
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as Map
import Data.Set as S
import Data.Aeson
import Fmt (build, fmt)

import Agora.Config.Definition
import Agora.Indexer.Capability
import Agora.Indexer.Types
import Agora.Mode
import Agora.Refine
import qualified Agora.Types as AT
import qualified Agora.Web.Types as AT

instance AgoraWorkMode m => Predicate m "PeriodExists" AT.PeriodId where
  validate pid = do
    mbPeriods <- nonEmpty <$> idxPeriods
    let periods = fromMaybe (error "Period list is empty") mbPeriods
    pure (pid >= 0 && pid <= maximum (periodIndex <$> periods))

getHistoricValuesForLevel :: forall m. (HasCallStack, AgoraWorkMode m) => AT.Level -> m AT.HistoricalConstants
getHistoricValuesForLevel (AT.Level level) = do
  history <- fromAgoraConfig $ option #network_history
  -- Use JSON instance to decode into `HistoricalConstants`
  let historyMapForLevel = toJSON (mapFn <$> history)
  case fromJSON historyMapForLevel of
    Success a -> pure a
    Error e -> error ("Error in decoding historic values:" <> show e)
  where
    mapFn :: HistoricValue -> Int
    mapFn hv = case DL.find findFn (_values hv) of
      Nothing -> _default hv
      Just v -> _value v

    findFn lrv = _start lrv <= level && _end lrv >= level

getPeriodInfo
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => Maybe AT.PeriodId
  -> m AT.PeriodInfo
getPeriodInfo mpId = do
  mbPeriods <- nonEmpty <$> idxPeriods
  let periods = fromMaybe (error "Period list is empty") mbPeriods
  let currentPeriod = maximum (periodIndex <$> periods)
  let pId = fromMaybe currentPeriod mpId
  let pIdInt = fromIntegral pId
  let periodInfos = NE.toList $ getPeriodItemInfo <$> periods
  case (periods NE.!! pIdInt, periodInfos !! pIdInt) of
    (PeriodInfo {..}, AT.PeriodItemInfo {..}) -> do
      constants <- getConstantsForLevel firstLevel
      let oneCycle = fromIntegral $ fromMaybe (error "Blocks-per-cycle is not available in constants") $ AT.coBlocksPerCycle constants
      chainHead <- idxHead
      discourseHost <- askDiscourseHost
      let hl = headLevel chainHead

      -- This is the last available level that is also
      -- in this period.
      let currentPeriodLevel = min hl lastLevel

      hv <- getHistoricValuesForLevel firstLevel

      let period :: AT.Period = AT.Period
            pId
            firstLevel
            currentPeriodLevel
            lastLevel
            _piiStartTime
            _piiEndTime
            (fromIntegral $ (currentPeriodLevel - firstLevel + 1) `div` oneCycle)
            (fromIntegral $ (lastLevel - firstLevel + 1) `div` oneCycle)
            constants
            hv
      case kind of
        Proposing -> do
          mProposal <- getWonProposalForPeriod_ pId currentPeriod epoch >>= traverse (convertProposal $ Just pId)
          votesStats <- getProposalVotesStatForProposalPeriod pId
          pure $ AT.ProposalInfo period (fromIntegral $ length periods)
            periodInfos
            votesStats
            mProposal
            discourseHost

        Exploration -> do
          proposal <- let pId' = pId - 1 in getWonProposalForPeriod pId' currentPeriod epoch >>= convertProposal (Just pId')
          votesStats <- getVotesStat pId
          ballots <- fromMaybe (error "Didn't find ballots for period") <$> getBallotStatsForPeriod epoch pId
          pure $ AT.ExplorationInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            ((== PSuccess) <$> status)
            votesStats
            ballots
            discourseHost

        Testing -> do
          proposal <- let pId' = pId - 2 in getWonProposalForPeriod pId' currentPeriod epoch >>= convertProposal (Just pId')
          pure $ AT.TestingInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            ((== PSuccess) <$> status)
            discourseHost

        Promotion -> do
          proposal <- let pId' = pId - 3 in getWonProposalForPeriod pId' currentPeriod epoch >>= convertProposal (Just pId')
          votesStats <- getVotesStat pId
          ballots <- fromMaybe (error "Didn't find ballots for period") <$> getBallotStatsForPeriod epoch pId
          pure $ AT.PromotionInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            ((== PSuccess) <$> status)
            votesStats
            ballots
            discourseHost

        Adoption -> do
          proposal <- let pId' = pId - 4 in getWonProposalForPeriod pId' currentPeriod epoch >>= convertProposal (Just pId')
          pure $ AT.AdoptionInfo period (fromIntegral $ length periods)
            periodInfos
            proposal
            discourseHost

getPeriodItemInfo
  :: HasCallStack
  => PeriodInfo
  -> AT.PeriodItemInfo
getPeriodItemInfo PeriodInfo {..} =
  AT.PeriodItemInfo startTime endTime (toAgoraPeriodType kind)

getBallots
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.Ballot]
getBallots periodId = do
  bakers <- idxVoters periodId
  fmap (convertBallot bakers) <$> idxBallotsForPeriod periodId

filterBallotsForPeriod :: AT.PeriodId -> [Ballot] -> [Ballot]
filterBallotsForPeriod periodId = DL.filter (\b -> periodId == simplePeriodInfoPeriod (ballotPeriod b))

getSpecificProposalVotes
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.ProposalHash
  -> AT.PeriodId
  -> m [AT.ProposalVote]
getSpecificProposalVotes proposalHash_ pId = do
  periodsForProposal <- idxGetPeriodsForProposal proposalHash_
  -- This contains all the periods where a proposal operation with hash matching that of `proposalHash_` was found.
  proposalMeta <- idxGetProposalMeta proposalHash_
  bakers <- getVotersForPeriods [pId]
  case find (== pId) periodsForProposal of
    Nothing -> do
      -- This (pId) is not a proposal period, or is a proposal period where propsal with proposalHash_ was not injected.
      -- So get all the ballots for this period, where propsoal hash matches with `proposalHash_`. The the latter in the
      -- previous sentance is true, this should result in empty list.
      proposalBallots <- idxBallotsForProposal proposalHash_
      pure $ convertProposalVote (Map.singleton proposalHash_ proposalMeta) bakers <$> filterBallotsForPeriod pId proposalBallots
    Just _ -> do
      -- This is a proposal period, where proposal operation with proposalHash_ was found. So get all the proposal operations with matching hash
      -- from this period, and count them as upvotes.
      pr <- idxGetProposal proposalHash_
      proposalOps <- idxProposalOperationsForPeriod pId
      -- Filter out proposal operations that are not this proposal
      let filteredProposalOps = DL.filter ((== proposalHash_) . simpleProposalHash . proposalOperationProposal) proposalOps
      pure (convertProposalVoteFromOperation (Map.singleton (proposalHash pr) proposalMeta) bakers <$> filteredProposalOps)

getProposalMetas
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => Proposal
  -> m (AT.ProposalHash, Maybe ProposalMeta)
getProposalMetas (proposalHash -> h) = do
  m <- idxGetProposalMeta h
  pure (h, m)

getVotersForEpoch
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.EpochId
  -> m [PeriodVoter]
getVotersForEpoch epochId = do
  epochInfo <- idxEpoch epochId
  getVotersForPeriods (periodIndex <$> periods epochInfo)

getVotersForPeriods
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => [AT.PeriodId] -> m [PeriodVoter]
getVotersForPeriods periods =
  S.toList <$> foldM (\s pid -> do
    v <- idxVoters pid
    pure $ S.union s (S.fromList v)) S.empty periods

getProposalVotes
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.ProposalVote]
getProposalVotes periodId = do
  refine @"PeriodExists" periodId >>= \case
    Nothing -> error "Period does not exist"
    Just refinedPeriodId -> do
      period <- idxPeriod refinedPeriodId
      bakers <- getVotersForEpoch (periodInfoEpoch period)
      prs <- Map.fromList <$> (idxGetProposals (periodInfoEpoch period) >>= mapM getProposalMetas)
      case kind period of
        Proposing ->
          -- In proposal period, we count the number of proposal operations to get the proposal
          -- vote count for the period.
          fmap (convertProposalVoteFromOperation prs bakers) <$> idxProposalOperationsForPeriod periodId
        _ ->
          -- In other periods, we use the ballots that were submitted during the period.
          fmap (convertProposalVote prs bakers) <$> idxBallotsForPeriod periodId

getProposals
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.Proposal]
getProposals pId = do
  refine @"PeriodExists" pId >>= \case
    Nothing -> error "Period does not exist"
    Just refinedPeriod -> do
      vp <- idxPeriod refinedPeriod
      prs <- idxGetProposals (periodInfoEpoch vp)
      mapM (convertProposal $ Just pId) (sortBy (\a b -> compare (proposalVotingPower b, proposalHash b) (proposalVotingPower a, proposalHash a)) prs)

getProposal
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.ProposalHash
  -> Maybe AT.PeriodId
  -> m AT.Proposal
getProposal proposalHash_ mPeriodId = do
  p <- idxGetProposal proposalHash_
  convertProposal mPeriodId p

getNonVoters
  :: forall m. (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m [AT.Baker]
getNonVoters periodId = do
  periodVoters <- idxVoters periodId
  voters <- S.fromList . fmap (address . ballotDelegate) <$> idxBallotsForPeriod periodId
  let nonVoters = Prelude.filter (\pv -> S.notMember (address $ periodVoterDelegate pv) voters) periodVoters
  pure $ convertPeriodVoter <$> nonVoters

convertPeriodVoter
  :: PeriodVoter
  -> AT.Baker
convertPeriodVoter pv = AT.Baker
  { _bkPkh = address $ periodVoterDelegate pv
  , _bkVotingPower = periodVoterVotingPower pv
  , _bkName = let
      d = periodVoterDelegate pv
      in fromMaybe "" (delegateAlias d)
  , _bkLogoUrl = logoUrl $ periodVoterDelegate pv
  , _bkProfileUrl = Just $ "https://tzkt.io/" <> AT.hashToText (address $ periodVoterDelegate pv) <>"/operations/voting"
  }

convertBallot
  :: [PeriodVoter]
  -> Ballot
  -> AT.Ballot
convertBallot bakers b@Ballot {vote, hash, timestamp, id=id_} = let
  sender = address $ ballotDelegate b
  _bId = fromIntegral id_
  _bAuthor = case find (\PeriodVoter {..} -> address delegate == sender) bakers of
    Just pr -> convertPeriodVoter pr
    Nothing -> error  "Didn't find period voter"
  _bOperation = hash
  _bTimestamp = timestamp
  _bDecision = vote
  in AT.Ballot {..}

convertProposalVoteFromOperation
  :: Map AT.ProposalHash (Maybe ProposalMeta)
  -> [PeriodVoter]
  -> ProposalOperation
  -> AT.ProposalVote
convertProposalVoteFromOperation metas bakers o@ProposalOperation {proposal, hash, timestamp, id=id_} = let
  sender = address $ proposalOperationDelegate o
  _pvId = fromIntegral id_
  _pvProposal = simpleProposalHash proposal
  _pvProposalTitle = do
    meta <- join $ Map.lookup (simpleProposalHash proposal) metas
    pure $ pmtDiscourseTitle meta
  _pvAuthor = case find (\PeriodVoter {delegate} -> address delegate == sender) bakers of
    Just pr -> convertPeriodVoter pr
    Nothing -> error "Proposal operation sender not found"
  _pvOperation = hash
  _pvTimestamp = timestamp
  in AT.ProposalVote {..}

convertProposalVote
  :: HasCallStack
  => Map AT.ProposalHash (Maybe ProposalMeta)
  -> [PeriodVoter]
  -> Ballot
  -> AT.ProposalVote
convertProposalVote metas bakers b@Ballot {proposal, hash, timestamp, id=id_} = let
  sender = address $ ballotDelegate b
  _pvId = fromIntegral id_
  _pvProposal = simpleProposalHash proposal
  _pvProposalTitle = do
    meta <- join $ Map.lookup (simpleProposalHash proposal) metas
    pure $ pmtDiscourseTitle meta
  _pvAuthor = case find (\PeriodVoter {delegate} -> address delegate == sender) bakers of
    Just pr -> convertPeriodVoter pr
    Nothing -> error ("No proposal voter found for address:" <> show sender <> " and ballot " <> AT.hashToText hash)
  _pvOperation = hash
  _pvTimestamp = timestamp
  in AT.ProposalVote {..}

getWonProposalForPeriod_
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> AT.PeriodId
  -> AT.EpochId
  -> m (Maybe Proposal)
getWonProposalForPeriod_ pId currentPeriod epoch = do
  if currentPeriod > pId
    then do
      ps <- getProposalsForPeriod_ pId epoch
      -- In the bunch of proposals, we consider the proposal
      -- with max value of lastPeriod to have won.
      -- We cannot use the "accepted" field because, if a proposal
      -- get rejected in a further period, it will have a non-accepted
      -- status, even though it has survived this period.
      pure . safeHead $ sortBy (\a b -> compare (lastPeriod b) (lastPeriod a)) ps
    else pure Nothing

getProposalsForPeriod_
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> AT.EpochId
  -> m [Proposal]
getProposalsForPeriod_ pId epoch = do
  ps <- idxGetProposals epoch
  pure (Prelude.filter (\Proposal {firstPeriod} -> firstPeriod == pId) ps)

getWonProposalForPeriod
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> AT.PeriodId
  -> AT.EpochId
  -> m Proposal
getWonProposalForPeriod pId currentPeriod epoch = do
  getWonProposalForPeriod_ pId currentPeriod epoch >>= \case
    Just p -> pure p
    _      -> error "Did not find accepted proposal in period"

askDiscourseHost :: AgoraWorkMode m => m Text
askDiscourseHost = fmt . build <$> fromAgoraConfig (sub #discourse . option #host)

getFirstPeriodForProposal :: (HasCallStack, AgoraWorkMode m) => Proposal -> m AT.PeriodId
getFirstPeriodForProposal p = do
  periods <- idxGetPeriodsForProposal (proposalHash p)
  case safeHead $ sort periods of
    Just a -> pure a
    Nothing -> error "Did not find period(s) for proposal"

convertProposal
  :: (HasCallStack, AgoraWorkMode m)
  => Maybe AT.PeriodId -- This has an explicit period argument because a proposal can be re-injected in a different epoch.
  -> Proposal
  -> m AT.Proposal
convertProposal mPeriodId p = do

  pId <- case mPeriodId of
    -- If a period was provided as context, try to use it,
    -- or else use the first period this proposal appeared.
    Just periodId -> do
      periods <- idxGetPeriodsForProposal (proposalHash p)
      case find (== periodId) periods of
        Just _ -> pure periodId
        Nothing -> getFirstPeriodForProposal p
    Nothing -> getFirstPeriodForProposal p

  refine @"PeriodExists" pId >>= \case
    Nothing -> error "Period does not exist"
    Just refinedPeriod -> do
      pinfo <- idxPeriod refinedPeriod
      quorumThreshold <- case ballotsQuorum pinfo of
        -- We have to look in nearby periods because `ballotsQuorum` field
        -- is only defined for Exploration and Promotion periods.
        Nothing -> case kind pinfo of
          Proposing -> refine @"PeriodExists" (pId + 1) >>= \case
            Nothing -> pure Nothing
            Just refinedNextPeriod -> ballotsQuorum <$> idxPeriod refinedNextPeriod
          Exploration -> pure Nothing
          Promotion -> pure Nothing
          Testing -> refine @"PeriodExists" (pId - 1) >>= \case
            Nothing -> pure Nothing
            Just refinedPrevPeriod -> ballotsQuorum <$> idxPeriod refinedPrevPeriod
          Adoption -> refine @"PeriodExists" (pId - 2) >>= \case
            Nothing -> pure Nothing
            Just refinedPrevPeriod -> ballotsQuorum <$> idxPeriod refinedPrevPeriod
        Just x -> pure $ Just x
      bakersForEpoch <- getVotersForEpoch (periodInfoEpoch pinfo)
      bakersForPeriod <- idxVoters pId

      let proposer = case find (\pv -> address (periodVoterDelegate pv) == address (initiator p)) bakersForEpoch of
            Just pr -> convertPeriodVoter pr
            Nothing -> error $
              "Proposer not found in period voters:" <> show bakersForEpoch <> "| " <> show (initiator p)
      idxProposalOperation (proposalHash p) >>= \case
        [] -> error "Proposal info couldn't be looked up"
        (proposalOperation:_) -> do
          mMetadata <- idxGetProposalMeta (proposalHash p)
          discourseHost <- askDiscourseHost
          firstPeriodOfAppearance <- getFirstPeriodForProposal p
          pure AT.Proposal
                { _prPeriod = pId
                , _prFirstPeriod = firstPeriodOfAppearance
                , _prHash = proposalHash p
                , _prTitle = pmtDiscourseTitle <$> mMetadata
                , _prShortDescription = pmtDiscourseShortDesc <$> mMetadata
                , _prLongDescription = pmtDiscourseLongDesc <$> mMetadata
                , _prTimeCreated = operationTimestamp proposalOperation
                , _prProposalFile = pmtDiscourseFileLink =<< mMetadata
                , _prDiscourseLink = sl discourseHost <$> mMetadata
                , _prVotesCasted = fromIntegral $ proposalVotingPower p
                , _prVotersNum = fromIntegral $ length bakersForPeriod
                , _prMinQuorum = floor $ 100 * fromMaybe 0 quorumThreshold
                , _prProposer = proposer
                }
    where
      sl h ProposalMeta {..} = h <> "/t/" <> show (fromIntegral pmtDiscourseTopicId :: Integer)

getConstantsForLevel
  :: (HasCallStack, AgoraWorkMode m)
  => AT.Level
  -> m AT.Constants
getConstantsForLevel level
  | level < 2 = constants <$> getProtocolForLevel 2 -- There are no meaningful constants below level 2
  | otherwise = constants <$> getProtocolForLevel level

getProtocolForLevel
  :: (HasCallStack, AgoraWorkMode m)
  => AT.Level
  -> m Protocol
getProtocolForLevel level = do
  ps <- idxProtocols
  case find (\Protocol {..} -> (firstLevel <= level) && not (isLevelPast lastLevel)) ps of
    Just p  -> pure p
    Nothing -> error $ "Protocol not found for level" <> show (level, ps)
  where
    isLevelPast Nothing   = False
    isLevelPast (Just ll) = level > ll

getBallotStatsForPeriod
  :: (HasCallStack, AgoraWorkMode m)
  => AT.EpochId
  -> AT.PeriodId
  -> m (Maybe AT.Ballots)
getBallotStatsForPeriod epochIdx periodId = do
  epochInfo <- idxEpoch epochIdx
  pure $ do
    PeriodInfo {..} <- find (\PeriodInfo {..} -> index == periodId) (periods epochInfo)
    _bYay <- fromIntegral <$> yayVotingPower
    _bNay <- fromIntegral <$> nayVotingPower
    _bPass <- fromIntegral <$> passVotingPower
    _bSupermajority <- supermajority
    _bQuorum <- ballotsQuorum
    pure AT.Ballots {..}

getProposalVotesStatForProposalPeriod
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m AT.VoteStats
getProposalVotesStatForProposalPeriod pId = do
  voters <- idxVoters pId
  proposalOperations <- idxProposalOperationsForPeriod pId

  -- A delegate can vote multiple times. But their rolls for each vote remains
  -- the same in the period. Here we count the proposal operations sent by the delegate
  -- in the period. If they have sent multiple operations, we only count their vote and rolls
  -- only once.
  let delegatesRollsSet =
        S.fromList
          $ (\p -> (proposalOperationDelegate p, proposalOperationVotingPower p)) <$> proposalOperations

  let votesAvailable = sum (periodVoterVotingPower <$> voters)
  let votesCast = sum (snd <$> S.toList delegatesRollsSet)

  let numVoters = fromIntegral $ S.size delegatesRollsSet
  let numVotersTotals = fromIntegral $ length voters

  pure $ AT.VoteStats (fromIntegral votesCast) (fromIntegral votesAvailable) numVoters numVotersTotals

getVotesStat
  :: (HasCallStack, AgoraWorkMode m)
  => AT.PeriodId
  -> m AT.VoteStats
getVotesStat pId = do
  voters <- idxVoters pId
  ballots <- idxBallotsForPeriod pId

  let ballotDelegatesSet = S.fromList $ ballotDelegate <$> ballots

  let votesAvailable = sum (periodVoterVotingPower <$> voters)

  -- To be precise, it appears that we should count multiple votes
  -- from the same delegate for the same proposal only once. But the current
  -- behavior (With built in indexer) appears not to do so.
  --
  -- For period 17, the current implementation shows 57,818 votes. But
  -- with duplicates elimination it only shows under 50K votes.
  let votesCast = sum (ballotVotingPower <$> ballots)

  let numVoters = fromIntegral $ S.size ballotDelegatesSet
  let numVotersTotals = fromIntegral $ length voters

  pure $ AT.VoteStats (fromIntegral votesCast) (fromIntegral votesAvailable) numVoters numVotersTotals

toAgoraPeriodType :: PeriodType -> AT.PeriodType
toAgoraPeriodType Testing     = AT.Testing
toAgoraPeriodType Proposing   = AT.Proposing
toAgoraPeriodType Exploration = AT.Exploration
toAgoraPeriodType Promotion   = AT.Promotion
toAgoraPeriodType Adoption    = AT.Adoption
