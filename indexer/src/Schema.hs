-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | Module that embeds the SQL schema definition.
module Schema
  ( schemaSQL
  ) where

import Data.FileEmbed
import Data.Text
import Data.Text.Encoding

schemaSQL :: Text
schemaSQL = decodeUtf8 $(embedFile "schema/schema.sql")
