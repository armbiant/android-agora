# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }:

{ config, pkgs, lib, ... }:
let
  cfg = config.services.agora.frontend;
  packages = self.packages.${pkgs.system};
in {
  options.services.agora.frontend = {
    enable = lib.mkEnableOption "agora frontend serving via nginx";
    package = lib.mkOption {
      description = "Path to the frontend package";
      type = lib.types.path;
      default = packages.agora-frontend;
    };
    api_addr = lib.mkOption {
      description = "Address of API";
      type = lib.types.str;
    };
    fqdn = lib.mkOption {
      description = "FQDN for the app to use";
      type = lib.types.str;
      default = "${config.networking.hostName}.${config.networking.domain}";
    };
  };
  config = lib.mkIf cfg.enable {
    services.nginx = {
      enable = true;
      virtualHosts.agora = {
        default = true;
        serverName = cfg.fqdn;
        forceSSL = true;
        enableACME = true;
        locations = {
          "/api/".proxyPass = cfg.api_addr;
          "/static/".alias = "${cfg.package}/";
          "/" = {
            root = cfg.package;
            tryFiles = "/index.html =404";
            extraConfig = "add_header Cache-Control no-cache;";
          };
        };
      };
    };
  };
}
