-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.VotingPower
  ( getTotalVotingPower
  , getVotingPowerForProposal
  ) where

import Data.Text (Text)
import Prelude hiding (max, min)

import Common
import Database.Models.PeriodVoters
import Database.Models.ProposalOperations
import Database.Models.Proposals
import DataStore.Common
import qualified Node

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getVotingPowerForProposal
  :: (MonadIO m, HasCap DbCap caps)
  => ProposalId
  -> CapsT caps m Node.VotingPower
getVotingPowerForProposal proposalId = let
  query = aggregate sumInt8 $ do
    proposalOperation <- selectTable proposalOperationsTable
    periodVoter <- selectTable periodVotersTable
    where_ (proposalOperation.proposalId .== toFields proposalId .&&
      periodVoter.id .== proposalOperation.periodVoterId .&&
      periodVoter.periodIdx .== proposalOperation.periodIdx)
    pure periodVoter.votingPower
  in withConnection $ \conn -> liftIO $
    Node.VotingPower . scientificToInt64 . fromMaybe 0 . listToMaybe <$> runSelect conn query

getTotalVotingPower
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m Node.VotingPower
getTotalVotingPower periodIdx = Node.VotingPower . scientificToInt64 <$> let
  query = aggregate sumInt8 $ do
    periodVoter <- selectTable periodVotersTable
    where_ (periodVoter.periodIdx .== toFields periodIdx)
    pure periodVoter.votingPower
  in withConnection $ \conn ->
    listToOne $ runSelect conn query
