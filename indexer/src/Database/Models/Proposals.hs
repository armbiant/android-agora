-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.Proposals
  ( Proposal
  , ProposalPoly(..)
  , ProposalId
  , proposalsTable
  ) where

import Database.Mapping
import Node qualified

type ProposalId = DbId "proposal"

data ProposalPoly id proposalHash periodIdx sourceBlockId = Proposal
  { id            :: id
  , proposalHash  :: proposalHash
  , periodIdx     :: periodIdx
  , sourceBlockId :: sourceBlockId
  }

$(makeAdaptorAndInstance "pDbProposal" ''ProposalPoly)

type Proposal'' rm = ProposalPoly
  (ComputeFieldTypeAuto rm ProposalId)
  (ComputeFieldType rm Node.ProposalHash)
  (ComputeFieldType rm Node.PeriodIdx)
  (ComputeFieldType rm Int)

type Proposal = Proposal'' 'HaskellRead

proposalsTable
  :: Table (Proposal'' 'DbWrite) (Proposal'' 'DbRead)
proposalsTable =
  table "proposals" (pDbProposal
    (Proposal
      (tableField "id")
      (tableField "proposal_hash")
      (tableField "period_idx")
      (tableField "source_block_id")))
