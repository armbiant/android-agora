// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/controls/AlertButton.scss";
import BellIcon from "../../assets/svg/BellIcon";

interface AlertButtonTypes {
  className?: string;
}

const AlertButton: FunctionComponent<AlertButtonTypes> = ({
  className,
}): ReactElement => {
  const alertLink = "https://t.me/TezosNotifierBot";
  return (
    <>
      <a
        className={cx(className, styles.button, styles.button_desktop)}
        href={alertLink}
        rel="noopener noreferrer"
        target="_blank"
      >
        <BellIcon />
      </a>
      <a
        className={cx(className, styles.button, styles.button_mobile)}
        href={alertLink}
        rel="noopener noreferrer"
        target="_blank"
      >
        <BellIcon />
      </a>
    </>
  );
};

export default AlertButton;
