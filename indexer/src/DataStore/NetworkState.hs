-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.NetworkState
  ( getNetworkState
  , getNetworkStateForPeriod
  , insertNetworkState
  ) where

import Data.Text (Text)

import Common
import Database.Models.Blocks
import Database.Models.NetworkState
import DataStore.Common
import qualified Node

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getNetworkState
  :: (MonadIO m, HasCap DbCap caps)
  => CapsT caps m (Maybe NetworkState)
getNetworkState = let
  query = do
    (ns, _) <- limit 1 $ orderBy (desc (blockTime . snd)) $ do
      ns <- selectTable networkStateTable
      bs <- selectTable blocksTable
      where_ (ns.sourceBlockId .== bs.id)
      pure (ns, bs)
    pure ns
  in withConnection $ \conn -> liftIO $ listToMaybe <$> runSelect conn query

getNetworkStateForPeriod
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m (NetworkState, Node.Level)
getNetworkStateForPeriod periodIdx = let
  query = fst <$> limit 1 (orderBy (desc snd) $ do
    ns <- selectTable networkStateTable
    blk <- selectTable blocksTable
    where_ (ns.periodIdx .== toFields periodIdx)
    where_ (blk.id .== ns.sourceBlockId)
    pure ((ns, blk.level), blk.blockTime))
  in withConnection $ \conn -> listToOne $ runSelect conn query

insertNetworkState
  :: (MonadIO m, HasCap DbCap caps)
  => NetworkStateWrite
  -> CapsT caps m NetworkState
insertNetworkState ns = withConnection $ \conn -> listToOne $
  runInsert conn $ Insert networkStateTable [toFields ns] (rReturning Prelude.id) Nothing
