-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- Common stuff that is used by DataStore functions.
module DataStore.Common
  ( module Monad.Capabilities
  , module Opaleye
  , module Data.Profunctor.Product
  , Int64
  , UTCTime
  , DbCap
  , MonadIO
  , applyOffsetLimit
  , encode
  , listToMaybe
  , listToOne
  , withConnection
  , scientificToInt64
  , fromMaybe
  ) where

import Control.Monad.IO.Class
import Data.Int
import Data.List qualified as DL
import Data.Maybe
import Data.Aeson
import Data.Profunctor.Product
import Data.Scientific
import Data.Time
import Opaleye

import Common
import Database.Caps
import GHC.Stack
import Monad.Capabilities

listToOne
  :: (MonadIO m, HasCallStack)
  => IO [a]
  -> m a
listToOne act = liftIO $ act >>= \case
  [a] -> pure a
  l -> throwRE $ "Unexpected item count in list: " <> showt (DL.length l)

applyOffsetLimit
  :: Maybe Int
  -> Maybe Int
  -> Order a
  -> Select a
  -> Select a
applyOffsetLimit mOffset mLimit orderFn (orderBy orderFn -> query) = let
  oQuery = case mOffset of
    Just off -> offset off query
    Nothing  -> query
  in case mLimit of
    Just lim -> limit lim oQuery
    Nothing  -> oQuery

scientificToInt64
  :: Scientific
  -> Int64
scientificToInt64 s = case toBoundedInteger s of
  Just x  -> x
  Nothing -> error "Number does not fit in 64 bits"
