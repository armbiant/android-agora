// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

module.exports = {
  "extends": "stylelint-config-sass-guidelines",
  "plugins": [
    "stylelint-scss",
  ],
  "rules": {
    "string-quotes": "double",
    "property-no-unknown": [
      true,
      {
        ignoreProperties: ["/^lost-/"],
      },
    ],
    "scss/at-rule-no-unknown": [
      true, { ignoreAtRules: ["lost"] },
    ],
    "selector-pseudo-class-no-unknown": [
      true, { ignorePseudoClasses: ["global"] },
    ],
  },
};
