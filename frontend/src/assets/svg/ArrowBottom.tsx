// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";
import cx from "classnames";

interface ArrowBottomProps {
  className?: string;
}

const ArrowBottomSvg: FunctionComponent<ArrowBottomProps> = ({
  className,
}): ReactElement => (
  <svg width={12} height={6} className={cx(className, styles.invertible_svg)}>
    <path fill="#123262" fillRule="evenodd" d="M0 0h12L6 6z" opacity={0.4} />
  </svg>
);

export default ArrowBottomSvg;
