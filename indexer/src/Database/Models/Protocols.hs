-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.Protocols
  ( Protocol
  , ProtocolPoly(..)
  , ProtocolId
  , protocolsTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Node qualified
import System.Log.FastLogger

type ProtocolId = DbId "protocol"

data ProtocolPoly id protocolHash constants blockId = Protocol
  { id            :: id
  , protocolHash  :: protocolHash
  , constants     :: constants
  , sourceBlockId :: blockId
  } deriving stock Show

instance (Show id, Show protocolHash, Show constants, Show sourceBlockId) =>
  ToLogStr (ProtocolPoly id protocolHash constants sourceBlockId) where
  toLogStr = toLogStr . show

$(makeAdaptorAndInstance "pDbProtocol" ''ProtocolPoly)

type Protocol'' rm = ProtocolPoly
  (ComputeFieldTypeAuto rm ProtocolId)
  (ComputeFieldType rm Node.ProtocolHash)
  (ComputeFieldType rm Node.RawConstants)
  (ComputeFieldType rm Blocks.BlockId)

type Protocol = Protocol'' 'HaskellRead

protocolsTable
  :: Table (Protocol'' 'DbWrite) (Protocol'' 'DbRead)
protocolsTable =
  table "protocols" (pDbProtocol
    (Protocol
      (tableField "id")
      (tableField "protocol_hash")
      (tableField "constants")
      (tableField "source_block_id")))
