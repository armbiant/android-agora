// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import styles from "~/styles/components/controls/LearnMoreButton.scss";
import { useTranslation } from "react-i18next";
import NaviLink from "../common/NaviLink";

interface LearnMoreButtonTypes {
  className?: string;
  href: string;
}

const LearnMoreButton: FunctionComponent<LearnMoreButtonTypes> = ({
  className,
  href,
}): ReactElement => {
  const { t } = useTranslation();
  return (
    <NaviLink className={cx(className, styles.button)} href={href}>
      {t("common.learnMoreButton")}
    </NaviLink>
  );
};

export default LearnMoreButton;
