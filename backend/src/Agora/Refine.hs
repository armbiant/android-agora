-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Refine
  ( Predicate(..)
  , Refined
  , refine
  , unrefine
  ) where

import GHC.TypeLits (Symbol)

newtype Refined (predicate :: Symbol) a = Refined a

class Monad m => Predicate m (p :: Symbol) a where
  validate :: a -> m Bool

unrefine :: Refined n a -> a
unrefine (Refined a) = a

refine :: forall p a m. Predicate m p a => a -> m (Maybe (Refined p a))
refine a = validate @m @p @a a >>= \case
  True -> pure $ Just $ Refined a
  False -> pure Nothing

instance (Monad m, Ord n, Num n) => Predicate m "GTEZ" n where
  validate x = pure (x >= 0)
