// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";

const SelectedItem: FunctionComponent = (): ReactElement => (
  <svg width={14} height={11} className={styles.invertible_svg}>
    <path fill="none" stroke="#0E151A" strokeWidth={2} d="M1 5l4 4 8-8" />
  </svg>
);

export default SelectedItem;
