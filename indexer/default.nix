# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ haskell-nix }:

let
  project = haskell-nix.stackProject {
    src = with haskell-nix.haskellLib; cleanSourceWith {
      name = "agora-indexer";
      src = cleanGit {
        src = ./.;
      };
    };
    modules = [
      ({ pkgs, ... }: {
        packages = {
          agora-indexer = {
            components.tests.indexer-test = {
              # These are runtime deps, but there is nowhere else to put them
              build-tools = with pkgs; [
                ephemeralpg
                python310
                cacert # required for octez-node
                iana-etc # required for octez-client
                octez-client
                octez-node
              ];
              preCheck = ''
                export TZ_INDEXER_DATABASE=$(pg_tmp -w 600)
              '';
              # we need the temporary directory from pg_tmp
              # so extract it out of $TZ_INDEXER_DATABASE
              postCheck = ''
                pg_tmp stop -d $(echo ''${TZ_INDEXER_DATABASE#*=} | sed 's:%2F:/:g') || :
              '';
            };
          };
        };
      })
    ];
  };
in haskell-nix.haskellLib.selectLocalPackages project
