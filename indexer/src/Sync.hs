-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | The module that actually does the indexing.
module Sync
  ( dbInitialize
  , runSync
  , sync
  , fetchBlocksAsync
  ) where

import Universum (whenJust)

import Control.Exception (AsyncException(UserInterrupt))
import Control.Monad
import Control.Monad.Catch
import Control.Monad.IO.Class
import Data.List as DL
import Data.Maybe
import Data.String
import Data.Text as T hiding (null)
import Database.PostgreSQL.Simple (execute_)
import Database.PostgreSQL.Simple.Transaction
import Monad.Capabilities
import System.Log.FastLogger
import UnliftIO.Async

import Caps
import Common
import qualified Database.Caps as Db
import qualified Database.Models.BallotOperations as BallotOperations
import qualified Database.Models.Blocks as Blocks
import qualified Database.Models.NetworkState as NetworkState
import qualified Database.Models.Proposals as Proposals
import qualified Database.Models.Protocols as Protocols
import qualified Database.Models.Voters as Voters
import qualified Database.Models.PeriodVoters as PeriodVoters
import qualified DataStore as Ds
import Logging
import qualified Node
import Schema

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

sync :: SyncM ()
sync =
  catchAnyErrorLogAndExit $
    forever $
      Ds.getLastCommitedBlock >>= \case
        Just ((.level) -> lastCommitedBlockLevel) -> do
          logDebug ("Found last commited block:" <> toLogStr lastCommitedBlockLevel)
          -- Fire a bunch of requests to fetch next few blocks...
          requests <- fetchBlocksAsync
            [ lastCommitedBlockLevel + 1 ..
              lastCommitedBlockLevel + fromIntegral blockFetchConcurrency]
          processResponses requests
        Nothing ->
          -- We start from level 2 because level 1 metadata is weird.
          Node.getBlock (Node.LevelSpec 2) >>= syncBlock
  where
    processResponses :: [Async (Node.Block, FetchSrc)] -> SyncM ()
    processResponses [] = pass
    processResponses (request : rst) = do
      (nextBlock, fetchedFrom) <- wait request
      Ds.getLastCommitedBlock >>= \case
        Nothing -> throwRE "Last commited block not found..."
        Just lastCommitedBlock -> do
          logDebug ("Found new block:" <> toLogStr nextBlock)
          if predecessorMatch lastCommitedBlock nextBlock
            then do
              syncBlock nextBlock
              logDebug ("Caching block:" <> toLogStr nextBlock)
              case fetchedFrom of
                FromNode -> void $ Ds.cacheBlock nextBlock
                FromCache -> pass
              logDebug "Done caching block"
              processResponses rst
            else do
              logInfo $ "Predecessor mismatch, undoing block: last commited block is "
                <> toLogStr lastCommitedBlock
                <> ", and predecessor for new block is "
                <> toLogStr (show nextBlock.header.predecessor)
              undoBlock lastCommitedBlock
              -- skip/cancel the rest of the responses since those won't be
              -- applied for want of this just undo'ed block, even worse, more
              -- blocks would be undoed unnecessarly for each new block that
              -- the rest of the requests in this batch brings.
              mapM_ cancel rst
              logInfo "Cancelled remaining pending requests..."

syncBlock
  :: Node.Block
  -> SyncM ()
syncBlock newBlock = do
  logInfo "--------Syncing block--------"
  logInfo $ toLogStr newBlock
  Db.withConnection $ \conn ->
    flip onException (do logInfo "Rolling back changes..."; liftIO $ rollback conn;) $ do
      liftIO $ begin conn
      logDebug ("Inserting block:" <> toLogStr newBlock)
      newDbBlock <- Ds.insertBlock newBlock
      (networkState, mperiodTransistion) <- updateAndGetNetworkState newBlock newDbBlock
      logDebug ("New network state:" <> toLogStr networkState)
      logDebug "Done..."
      whenJust mperiodTransistion $ \(PeriodTransistion mpastNetworkState) -> do
        -- If we are transistioning to a new period do some initializations
        -- for the new period.
        logDebug "Period transistion, syncing protocol and voters..."
        syncProtocol newDbBlock newBlock.protocol
        logDebug "Synced protocol..."
        syncVoters newDbBlock.id networkState
        logDebug "Synced voters..."
        -- If we have a past period, then update the proposal statuses
        -- for that period.
        whenJust mpastNetworkState $ \pastNetworkState -> do
          periodStatus <- do
            logDebug "Updating proposal status for period..."
            updateProposalStatusesForPeriod newDbBlock.id pastNetworkState networkState
          logDebug "Done..."
          void $ Ds.insertPeriodStatus newDbBlock.id pastNetworkState.periodIdx periodStatus
      logDebug "Syncing operations..."
      syncOperations networkState newDbBlock.id newBlock.operations
      liftIO $ commit conn
      logInfo "--------- Done --------------"

syncOperations
  :: NetworkState.NetworkState
  -> Blocks.BlockId
  -> [Node.Operation]
  -> SyncM ()
syncOperations networkState sourceBlockId operations = do
  forM_ operations $ \case
    Node.Operation operationHash (Node.ProposalOperation pdata) -> do
      logDebug ("Inserting proposal operations " <> toLogStr (show pdata.proposals))
      insertProposalOperation
        sourceBlockId networkState.periodIdx operationHash pdata
    Node.Operation operationHash (Node.BallotOperation bdata) -> do
      logDebug ("Inserting ballot operation "
        <> toLogStr (show (bdata.proposal, bdata.ballot, bdata.source, bdata.period)))
      void $ insertBallotOperation
        sourceBlockId networkState operationHash bdata

predecessorMatch
  :: Blocks.Block
  -> Node.Block
  -> Bool
predecessorMatch predecessor block =
  predecessor.hash == block.header.predecessor

syncVoters
  :: Blocks.BlockId
  -> NetworkState.NetworkState
  -> SyncM ()
syncVoters sourceBlockId networkState = do
  let
    votersAvailable  = case networkState.periodKind of
      Node.Proposal    -> True
      Node.Exploration -> True
      Node.Promotion   -> True
      _                -> False
  if votersAvailable
    then do
      logDebug "Syncing voters..."
      block <- Ds.getBlockWithId sourceBlockId
      voters <- Node.getVotersForBlock block.hash
      mapM_ saveOneVoter voters
      logDebug $ "Saved " <> toLogStr (DL.length voters) <> " voters..."
      logDebug $ toLogStr $ showt voters
    else do
      logDebug "Voters not available for period type..."
  where
    saveOneVoter :: Node.Voter -> SyncM ()
    saveOneVoter voter = do
      voterId <- Ds.getVoterWithAddress voter.pkHash >>= \case
        Just v  -> pure v.id
        Nothing -> Ds.insertVoter sourceBlockId voter.pkHash
      void $ Ds.insertPeriodVoter sourceBlockId voterId
        networkState.periodIdx voter.votingPower

syncProtocol
  :: Blocks.Block
  -> Node.ProtocolHash
  ->  SyncM ()
syncProtocol block protocolHash = do
  logDebug $ "Syncing protocol" <> toLogStr protocolHash
  protocol <- Ds.getProtocol protocolHash >>= \case
    Just p -> do
      logDebug $ "Protocol found in db with id: " <> (toLogStr p.id)
      pure p
    Nothing -> do
      constants <- Node.getConstantsForBlock block.hash
      p <- Ds.insertProtocol protocolHash constants block.id
      logDebug $ "Inserted protocol: " <> toLogStr p
      pure p
  _ <- Ds.insertBlocksToProtocol block.id protocol.id
  logDebug $ "Synced protocol" <> toLogStr protocolHash

hasMetQuorum :: Node.Quorum -> Node.VotingPower -> Node.VotingPower -> Bool
hasMetQuorum quorum (Node.unVotingPower -> totalVotingPower) (Node.unVotingPower -> votingPower) =
  div (votingPower * 10000) totalVotingPower >= fromIntegralSafe (Node.unQuorum quorum)
  -- Multiply by 10000 instead of 100 because on the network a quorum of 80% is represented as 8000.

filterAndSumVotingPowerForBallots
  :: (BallotOperations.Ballot -> Bool)
  -> [(BallotOperations.Ballot, Node.VotingPower)]
  -> Node.VotingPower
filterAndSumVotingPowerForBallots filterFn ballots =
  sum $ snd <$> DL.filter (filterFn . fst) ballots

hasSuperMajority
  :: [(BallotOperations.Ballot, Node.VotingPower)]
  -> Bool
hasSuperMajority ballots =
  let
    yayBallots = Node.unVotingPower $
      filterAndSumVotingPowerForBallots (\b -> b.ballot == Node.Yay) ballots
    nayBallots = Node.unVotingPower $
      filterAndSumVotingPowerForBallots (\b -> b.ballot == Node.Nay) ballots
  in (div (yayBallots * 100) (yayBallots + nayBallots) >= 80)

data VoteResults
  = Winner Proposals.ProposalId [Proposals.ProposalId]
  | Tied [Proposals.ProposalId]
  | NoPropos
  deriving stock Show

updateProposalStatusesForPeriod
  :: Blocks.BlockId
  -> NetworkState.NetworkState
  -> NetworkState.NetworkState
  -> SyncM Node.PeriodStatus
updateProposalStatusesForPeriod sourceBlockId pastNetworkState networkState = do
  logDebug $ "Updating proposal statuses for period: " <> (toLogStr pastNetworkState.periodIdx)
  case pastNetworkState.periodKind of
    Node.Proposal -> do
      logDebug "Past period is a proposal period..."
      proposalsWithUpvotes <- Ds.getProposalsForPeriodWithUpvotes pastNetworkState.periodIdx
      logDebug "Found these proposals with upvotes for the period..."
      logDebug $ toLogStr $ show proposalsWithUpvotes
      let
        sortedProposals = case sortBy (\t1 t2 -> compare (snd t2) (snd t1)) proposalsWithUpvotes of
          [] -> NoPropos
          ((winner, vp1): rest@((_, vp2) : _)) -> do
            if vp1 == vp2 then Tied (winner:(fst <$> rest)) else Winner winner (fst <$> rest)
          [(winner, _)] -> Winner winner []

      logDebug "Evaluated proposal status as:"
      logDebug $ toLogStr $ show sortedProposals
      case sortedProposals of
        Winner winner rest ->  do
          -- Since quorum requirements can change in protocols, the only
          -- relible way to check if a proposal period was successful is to check the
          -- next period and see if it is a proposal kind. So check the next period..
          if networkState.periodKind == Node.Proposal
            then do
              logDebug
                "The next period is a proposal period, so assumming\
                \ this proposal period to have failed..."
              logDebug "Marking all proposals in this period to be skipped..."
              mapM_ (Ds.insertProposalStatus sourceBlockId Node.Skipped) (winner : rest)
              -- We could check if period met a required 5% quorum, but this was not required
              -- for older periods, so now we ll have to change logic based on which period we are in,
              -- and that will complicate things, so we just assume quorum was not met.
              -- TODO: See if recent protocols allow us to query for this information and use it here.
              pure Node.NoQuorum
            else do
              logDebug $ "Found winner proposal: " <> toLogStr winner
              logDebug "Marking rest of proposals in this period to be skipped..."
              mapM_ (Ds.insertProposalStatus sourceBlockId Node.Skipped) rest
              pure Node.Success

        Tied proposals -> do
          logDebug "Proposals tie for a winner, marking all proposals as skipped."
          mapM_ (Ds.insertProposalStatus sourceBlockId Node.Skipped) proposals
          pure Node.NoSingleWinner
        NoPropos -> do
          logDebug "No proposals for period"
          pure Node.NoProposals
    Node.Exploration -> updateProposalStatusForExplorationPromotion
    Node.Testing -> do
      logDebug "Marking testing period as a success automatically..."
      pure Node.Success
    Node.Promotion -> updateProposalStatusForExplorationPromotion
    Node.Adoption -> do
      let proposalPeriod = getCorrespondingProposalPeriod pastNetworkState
      getActiveProposalFor proposalPeriod >>= \case
        Nothing -> throwRE $ "Did not find active proposal for period:"
          <> (showt networkState.periodIdx)
        Just activeProposal -> do
          logDebug $ "Marking status of " <>
            toLogStr (show (activeProposal.id, activeProposal.proposalHash)) <> " as a accepted..."
          void $ Ds.insertProposalStatus sourceBlockId Node.Accepted activeProposal.id
          logDebug "Marking adoption period as a success automatically..."
          pure Node.Success

  where

    getActiveProposalFor :: Node.PeriodIdx -> SyncM (Maybe Proposals.Proposal)
    getActiveProposalFor periodIdx = do
      (DL.filter ((== Node.Active) . snd) <$> Ds.getProposalsWithStatusesFor periodIdx) >>= \case
        [(p, Node.Active)] -> pure $ Just p
        s -> throwRE $ "Unexpected number of active proposals for period: "
          <> showt periodIdx <> " : " <> showt (DL.length s) <> " proposals..."

    updateProposalStatusForExplorationPromotion :: SyncM Node.PeriodStatus
    updateProposalStatusForExplorationPromotion = do
      logDebug "Updating proposal status for Exploration/Promotion periods..."
      let proposalPeriod = getCorrespondingProposalPeriod pastNetworkState
      logDebug $ "Corresponding proposal period is: " <> toLogStr proposalPeriod
      getActiveProposalFor proposalPeriod  >>= \case
        Just activeProposal -> do
          logDebug ("Found accepted proposal for period:" <> (toLogStr activeProposal.proposalHash))
          case networkState.periodKind of
              Node.Proposal -> do
                -- Current network state is a proposal, which means, unless this epoch does not have an adoption
                -- period, the past period exploration/promotion period was not successful.
                -- Let us find out why.
                ballotsWithVp <- Ds.getBallotsForProposalWithVotingPower activeProposal.id pastNetworkState.periodIdx
                let votingPowerForProposal = sum $ snd <$> ballotsWithVp
                totalVotingPowerForPeriod <- Ds.getTotalVotingPower pastNetworkState.periodIdx
                case (hasSuperMajority ballotsWithVp, hasMetQuorum (fromMaybe (error "Quorum not available for period") $
                  pastNetworkState.quorum) totalVotingPowerForPeriod votingPowerForProposal) of
                  (_, False) -> do
                    logDebug "Found that the active proposal have supermajority,\
                      \ but didn't meet quorum, marking proposal as skipped..."
                    void $ Ds.insertProposalStatus sourceBlockId Node.Skipped activeProposal.id
                    pure Node.NoQuorum
                  (False, True) -> do
                    logDebug "Found that the active proposal didn't have super majorty,\
                      \ marking proposal as rejected..."
                    void $ Ds.insertProposalStatus sourceBlockId Node.Rejected activeProposal.id
                    pure Node.NoSupermajority
                  (True, True) ->
                    if pastNetworkState.periodKind == Node.Promotion
                      then do
                        -- Everything has passed, and the proposal should have
                        -- proceeded to adoption, then why is next period a
                        -- proposal period? Because this is probably a legacy epoch
                        -- without adoption period. Mark this period as successful and
                        -- proposal as being accepted.
                        logDebug "Found that the active proposal did get more Yay votes\
                          \ than Nay votes, but and met super majorty and quorum, marking as success,\
                          \ assuming this to be last period in epoch..."
                        void $ Ds.insertProposalStatus sourceBlockId Node.Accepted activeProposal.id
                        pure Node.Success
                      else
                        throwRE "Period status ambigious!"
              _ -> do
                logDebug "Next period is not a proposal period, so this period is a success..."
                pure Node.Success
        Nothing -> throwRE $
          "Accepted proposal not found for exploration/promotion periods:" <> showt proposalPeriod

-- A type to represent if a block switches the network to a new period and if
-- so, wrap the old period.
data PeriodTransistion = PeriodTransistion
  { _oldNetworkState :: Maybe NetworkState.NetworkState
  }

-- Compute the new network state caused by a new block. Returns the new network state and an old
-- network state, if available.
updateAndGetNetworkState
  :: Node.Block
  -> Blocks.Block
  -> SyncM (NetworkState.NetworkState, Maybe PeriodTransistion)
updateAndGetNetworkState newBlock newDbBlock =
  Ds.getNetworkState >>= \mNs -> do
    case mNs of
      Nothing -> do
        logDebug "No past network state found, initializing..."
        logInfo $ "Initializing chain id:" <> toLogStr (Node.unChainId newBlock.chainId)
        Ds.insertChainId newDbBlock.id newBlock.chainId
        logDebug "Getting quorum threshold for the block..."
        quorum <- Node.getQuorumForBlock newBlock.hash
        logDebug $ "Got: " <> toLogStr (show quorum)
        networkState <- do
          let zeroEpoch = 0
          ns <- Ds.insertNetworkState $
            NetworkState.NetworkState Nothing newBlock.metadata.period
              newBlock.metadata.periodKind zeroEpoch quorum newDbBlock.id
          logDebug $ "Initializing empty network state:" <> toLogStr ns
          newPeriodStatus <- Ds.insertPeriodStatus newDbBlock.id
            newBlock.metadata.period Node.ActivePeriod
          logDebug $ "Inserted period status:" <> toLogStr newPeriodStatus
          pure ns
        pure (networkState, Just $ PeriodTransistion Nothing)
      Just networkState -> do
        logDebug ("Current network state: " <> toLogStr networkState)
        if networkState.periodIdx /= newBlock.metadata.period
          then do
            logDebug ("Entering new period: " <> (toLogStr newBlock.metadata.period))
            let
              -- Now this is a new period, and if that is a proposal period, increment the epoch index.
              newEpoch = if newBlock.metadata.periodKind == Node.Proposal
                then networkState.epoch + 1
                else networkState.epoch

            -- Update quorum for the period if available.
            logDebug "Getting quorum threshold for the block..."
            quorum <- Node.getQuorumForBlock newBlock.hash
            logDebug $ "Got: " <> toLogStr (show quorum)
            ns <- Ds.insertNetworkState $
              NetworkState.NetworkState Nothing newBlock.metadata.period
                newBlock.metadata.periodKind newEpoch quorum newDbBlock.id
            logDebug $ "Inserted new network state:" <> toLogStr ns
            newPeriodStatus <- Ds.insertPeriodStatus newDbBlock.id
              newBlock.metadata.period Node.ActivePeriod
            logDebug $ "Inserted period status:" <> toLogStr newPeriodStatus
            pure (ns, Just $ PeriodTransistion (Just networkState))
          else pure (networkState, Nothing)

insertProposalOperation
  :: Blocks.BlockId
  -> Node.PeriodIdx
  -> Node.OperationHash
  -> Node.ProposalOperationData
  -> SyncM ()
insertProposalOperation sourceBlockId periodIdx operationHash proposalOperationData = do
  forM_ proposalOperationData.proposals $ \proposalHash -> do
    logDebug $ "Found proposal operation for" <> toLogStr proposalHash <>
      " look for existing proposal..."
    proposalId <- Ds.getProposal periodIdx proposalHash >>= \case
      Just p  -> do
        logDebug $ "Found existing proposal with id: " <> (toLogStr p.id)
        pure p.id
      Nothing -> do
        logDebug "Existing proposal not found, inserting proposal..."
        proposal <- Ds.insertProposal sourceBlockId periodIdx proposalHash
        logDebug $ "Inserted proposal:" <> toLogStr (proposal.id)
        proposalStatus <- Ds.insertProposalStatus sourceBlockId Node.Active proposal.id
        logDebug $ "Inserted proposal status:" <> toLogStr proposalStatus
        pure proposal.id
    logDebug $ "Proposal id:" <> toLogStr proposalId
    logDebug "Getting period voter..."
    periodVoter <- Ds.getPeriodVoter proposalOperationData.source periodIdx >>= \case
      Just periodVoter -> pure periodVoter
      Nothing -> throwRE $ "Period voter not found for: " <> (showt proposalOperationData.source) <> " and period " <> showt periodIdx
    logDebug $ "Got period voter: " <> toLogStr (periodVoter.id)
    proposalOperation <- Ds.insertProposalOperation sourceBlockId periodIdx
      proposalId operationHash periodVoter
    logDebug $ "Inserted proposal operation: " <> toLogStr proposalOperation

getCorrespondingProposalPeriod
  :: NetworkState.NetworkState
  -> Node.PeriodIdx
getCorrespondingProposalPeriod networkState = case networkState.periodKind of
  Node.Proposal    -> networkState.periodIdx
  Node.Exploration -> networkState.periodIdx - 1
  Node.Testing     -> networkState.periodIdx - 2
  Node.Promotion   -> networkState.periodIdx - 3
  Node.Adoption    -> networkState.periodIdx - 4

insertBallotOperation
  :: Blocks.BlockId
  -> NetworkState.NetworkState
  -> Node.OperationHash
  -> Node.BallotOperationData
  -> SyncM ()
insertBallotOperation sourceBlockId networkState operationHash ballotOperationData = do
    logDebug $ "Found ballot operation: " <> toLogStr ballotOperationData
    let
      correspondingProposalPeriod = getCorrespondingProposalPeriod networkState
    proposalId <- Ds.getProposal correspondingProposalPeriod ballotOperationData.proposal >>= \case
      Just p  -> pure p.id
      Nothing -> throwRE "Proposal operation for ballot not found"
    periodVoter <- Ds.getPeriodVoter ballotOperationData.source networkState.periodIdx >>= \case
      Just pv -> pure pv
      Nothing -> throwRE $ "Period voter not found for: "
        <> (showt ballotOperationData.source) <> " and period " <> (showt networkState.periodIdx)
    ballotOperation <-
      Ds.insertBallotOperation sourceBlockId proposalId
        ballotOperationData.period operationHash periodVoter.id ballotOperationData.ballot
    logDebug $ "Inserted ballot operation: " <> toLogStr ballotOperation

catchAnyErrorLogAndExit ::  SyncM () -> SyncM ()
catchAnyErrorLogAndExit act =
  catch act $ \(e :: SomeException) -> do
    case fromException e of
      Just UserInterrupt -> pass
      _ -> do
        logInfo $ "\n!!!!!!!! UNCAUGHT EXCEPTION  !!!!!!!: \n\n " <>
          toLogStr (displayException e) <>
            "\n\n!!!!!!!! ------------------  !!!!!!!: \n\n "
        throwM e

runSync :: IO ()
runSync = runSyncM $ do
  dbInitialize
  sync

blockFetchConcurrency :: Int
blockFetchConcurrency = 20

data FetchSrc
  = FromNode
  | FromCache

fetchBlocksAsync :: [Node.Level] -> SyncM [Async (Node.Block, FetchSrc)]
fetchBlocksAsync levels = forM levels $ async . getBlock . Node.LevelSpec
  where
    getBlock ls =
      Ds.fetchBlockFromCache ls >>= \case
        Nothing -> do
          block <- Node.getBlock ls
          pure (block, FromNode)
          -- We do not cache the block here since if we do it before validating
          -- the predecessor, then we might end up caching an outdated block,
          -- which would cause a loop. This is because, say we cache a block at
          -- level `x`, which ended up being outdated. Then, when processing
          -- level `(x - 1)`, that block will never pass validation
          -- because the level `x`, which will be always fetched from cache,
          -- will have a different predecessor. Note that this happens because
          -- this function (`fetchBlocksAsync`) is called concurrently, without
          -- waiting for the processing of fetched block to be done.
        Just x -> do
          logDebug $ "Found block " <> toLogStr (showt ls) <> " in cache..."
          pure (x, FromCache)

dbInitialize :: HasCaps ApiCaps caps => CapsT caps IO ()
dbInitialize = handle (\(e::SomeException) -> logDebug $ toLogStr $ show e) $ do
  logDebug "Initializing db..."
  Db.withConnection $ \conn -> flip onException
    (do logInfo "Rolling back changes..."; liftIO $ rollback conn;) $ do
      liftIO $ do
        begin conn
        execute_ conn $ fromString $ T.unpack schemaSQL
        commit conn

undoBlock :: Blocks.Block -> SyncM ()
undoBlock blk = do
  logInfo "------- Undoing block ---------"
  Db.withConnection $ \conn -> do
    liftIO $ begin conn
    void $ Ds.deleteBlock blk
    -- Due to the database constraints, all the database rows derived from this
    -- block (except the cache) will be cascade dropped automatically.

    void $ Ds.removeBlockFromCache blk.hash
    -- Then we clear the cached block manually. This is not covered in the above
    -- mechanism because we will want to retain the cache, even after clearing
    -- all the rows from the`blocks` table. So that we can re-populate the
    -- database using data from the cache.
    liftIO $ commit conn
    logInfo "------- Done ------------------"
