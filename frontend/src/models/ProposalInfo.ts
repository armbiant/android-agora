// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

function humanize(n: number): string {
  if (n > 1000000) {
    return (n / 1000000).toFixed(2).toString() + " M";
  } else if (n > 10000) {
    return (n / 1000).toFixed(2).toString() + " K";
  } else {
    return n.toFixed(2).toString();
  }
}

export function formatVotingPower(mtz: number): string {
  let tz = mtz / 1000000;
  return humanize(tz);
}

export interface Proposer {
  pkh: string;
  votingPower: number;
  name: string;
  logoUrl: string | null;
  profileUrl: string | null;
}

export interface Proposal {
  id: number;
  hash: string;
  title: string;
  shortDescription: string;
  longDescription: string;
  timeCreated: string;
  proposalFile: string | null;
  discourseLink: string;
  proposer: Proposer;
  period: number;
  votesCasted: number;
  votersNum: number;
}
