<!--
SPDX-FileCopyrightText: 2019-2021 Tocqueville Group

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Agora backend

Agora backend is a middleware exploiting Tezos node RPC to provide an API for
Agora frontend.

